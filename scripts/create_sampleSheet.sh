#!/bin/bash

## [Goal] Create sample sheet for all fastq files in a folder
## [Author] Carlus Deneke, Holger Brendebach
version=0.5.3

## Shell Behaviour --------------------------------------------------
set -Eeuo pipefail # to exit immediately, # ADD e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name --mode {mode} --fastxDir {path/to/fastxDir} [Options]

${bold}Description:${normal}
Create sample sheet for all fastX files in a specified folder (version: $version).

${bold}Required Named Arguments:${normal}
-m, --mode {mode}               Choose mode from {illumina, ncbi, flex, dot, assembly, fna}  (default=illumina)
-f, --fastxDir {path}           Path to existing directory containing the fastq or fasta files (default=$(pwd -P))

${bold}Optional Named Arguments:${normal}
-o, --outDir {path}             Path to existing outDir (default={fastxDir})
--nocheck                       Do not check consistency of sample sheet (default=false)
-F, --force                     Overwrite existing samples.tsv files in OutDir (default=false)
-n, --dryrun                    Perform a dryrun to review the commands (default=false)
-i, --interactive               Ask before starting the program (default=false)

${bold}Details on {mode} parameters:${normal}
* illumina   ${blue}{sample_name}${yellow}_S*_R{1,2}_001.fastq*${normal}
* ncbi       ${blue}{sample_name}${yellow}_{1,2}.fastq.gz${normal}
* flex       ${blue}{sample_name}${yellow}*_R{1,2}*.fastq*${normal}   Note: The sample name is cut before "_R1"
* dot        ${blue}{sample_name}${yellow}.{1,2}.fastq.gz${normal}
* assembly   ${blue}{sample_name}${yellow}.fasta${normal}
* fna        ${blue}{sample_name}${yellow}.fna[.gz]${normal}
* name       ${blue}{sample_name}${yellow}.fastq.gz${normal}

${bold}Example:${normal}
bash $script_name --mode assembly --fastxDir ./Analysis/Assembly/assembly --outDir ./Analysis/ --force
> This saves a sample sheet from all fasta files in the folder ./Analysis/Assembly/assembly in the folder ./Analysis/

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  mode="illumina"
  fastxDir=$(pwd -P)

  ## default values of switches
  dryrun=false
  interactive=false
  force=false
  check=true
  keep_undetermined=false
  status="PASS"

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      --nocheck)
        check=false
        shift
        ;;
      -m | --mode)
        mode=${2,,} # convert to lower case
        shift 2
        ;;
      -f | --fastxDir)
        fastxDir="$2"
        shift 2
        ;;
      -o | --outDir)
        outDir="$2"
        shift 2
        ;;
      -F | --force)
        force=true
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -i | --interactive)
        interactive=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. Try ${blue}bash $script_real --help${normal}"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  #  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed, disable in BfR-ABC repositories

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths() use realpaths for proper awk splitting

  outDir=${outDir:-${fastxDir}}
  outfile="${outDir}/samples.tsv"

  ## checks
  [[ -d $fastxDir ]] || die "The fastxDir directory does NOT exist: $fastxDir"
  [[ -d $outDir ]] || die "The outDir directory does NOT exist: $outDir"
  [[ -f ${outfile} && ${force} != true ]] && die "The sample sheet already exists. Use --force option to replace it: $outfile"
  [[ ${dryrun} == true ]] && outfile="/dev/null"

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="${outDir}/${logfile_name}"
  [[ $dryrun == false ]] && logfile_global=/dev/null # ADD

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
mode:|$mode
search pattern:|$pattern
input fastxDir:|$fastxDir
ouput outfile:|$outfile
check:|$check
force:|$force
dryrun:|$dryrun
interactive:|$interactive
" | column -t -s "|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  echo
}

## Modules --------------------------------------------------

check_mode() {
  ## [Rationale] check_mode() set find and parsing functions accordingly

  # check mode parameter by its first three letters
  case ${mode} in
    *ill*)
      mode="illumina"
      header="sample\tfq1\tfq2"
      pattern="*_S*_R1_001.fastq*"
      function get_sample_name() { awk -F '_S[0-9]' '{print $1}'; }
      function reverse_read() { sed 's/_R1_001.fastq/_R2_001.fastq/'; }
      ;;
    *ncb*)
      mode="ncbi"
      header="sample\tfq1\tfq2"
      pattern="*_1.fastq*"
      function get_sample_name() { cut -f 1 -d '_'; }
      function reverse_read() { sed 's/_1.fastq/_2.fastq/'; }
      ;;
    *fle*)
      mode="flex"
      header="sample\tfq1\tfq2"
      pattern="*_R1*.fastq*"
      function get_sample_name() { awk -F '_R1' '{print $1}'; }
      function reverse_read() { sed 's/_R1/_R2/'; }
      ;;
    *dot*)
      mode="dotted"
      header="sample\tfq1\tfq2"
      pattern="*_1.fastq*"
      function get_sample_name() { cut -f 1 -d '.'; }
      function reverse_read() { sed 's/\.1.fastq/.2.fastq/'; }
      ;;
    *ass*) # ;-)
      mode="assembly"
      header="sample\tassembly"
      pattern="*.fasta"
      function get_sample_name() { sed 's/\.fasta//'; }
      function reverse_read() { :; }
      ;;
    *fna*)
      mode="fna"
      header="sample\tassembly"
      pattern="${fastxDir}/*.fasta"
      function get_sample_name() { sed 's/\.fna.gz//' | sed 's/\.fna//'; }
      function reverse_read() { :; }
      ;;
    *nam*)
      mode="names"
      header="sample"
      pattern="*.fastq.gz"
      function get_sample_name() { xargs -n1 basename | cut -d '_' -f 1 | sort | uniq; }
      ;;
    *) die "The {mode} was not specified or recognized. Check ${blue}bash $script_real --help${normal} for available parameters" ;;
  esac

  return 0
}

create_sample_sheet() {
  ## [Rationale] create_sample_sheet() find and parse samples based on mode

  ## find files
  files=$(find ${fastxDir}/* -maxdepth 0 -type f,l -name "${pattern}" -print)
  [[ $(grep -c -v '^$' <<< $files) -eq 0 ]] && die "No samples found. Check if your samples are formatted according to the selected mode"

  ## write sample sheet header
  [[ $dryrun == true ]] && logheader "Sample Sheet:" && echo -e "$header"
  echo -e "$header" > ${outfile}

  ## write sample sheet content
  if [[ $mode != "names" ]]; then
    for file in $files; do
      unset sample R2 sample_line
      sample=$(basename ${file} | get_sample_name)
      R2=$(echo ${file} | reverse_read)
      [[ $header != *assembly* ]] && ([[ -f ${R2} ]] || die "Reverse read does NOT exist: $R2")
      [[ -z $R2 ]] && sample_line=$(echo -e "$sample\t${file//\/\//\/}")
      [[ -n $R2 ]] && sample_line=$(echo -e "$sample\t${file//\/\//\/}\t${R2//\/\//\/}")
      [[ $dryrun == true ]] && echo "$sample_line"
      echo "$sample_line" >> ${outfile}
    done
  fi

  # write simple sample list
  if [[ ${mode} == "names" ]]; then
    [[ $dryrun == true ]] && echo $files | get_sample_name # verbose for debug
    echo $files | get_sample_name > ${outfile}             # NOTE: overwrite the header
  fi

  # remove undetermined
  [[ $dryrun != true && $keep_undetermined != true ]] && sed -i '/Undetermined_/d' ${outfile}
  [[ $dryrun == true ]] && echo

  # final sample count
  [[ $mode != "names" ]] && samples_found=$(tail -n +2 ${outfile} | wc -l)
  [[ $mode == "names" ]] && samples_found=$(tail -n +1 ${outfile} | wc -l)

  return 0
}

validate_sample_sheet() {
  # check if sample name is contained in the read files
  if [[ $mode != "names" ]]; then
    while read -r sample read1 read2; do
      if ! [[ ${read1} =~ "${sample}_" || ${read1} =~ "${sample}.1.fastq" || ${read1} =~ "${sample}.fasta" || ${read1} =~ "${sample}.fna" ]]; then
        logerror "$sample not contained in $read1"
        status="FAIL"
      fi
    done < <(tail -n +2 ${outfile})
  fi

  # check if same sample name more than once
  dups=$(tail -n +2 ${outfile} | cut -f 1 | sort | uniq -c | sed -E 's/^[ ]+//' | cut -f 1 -d ' ' | awk '$1 != 1' | wc -l)
  [[ $dups -gt 0 ]] && status="FAIL" && logerror "Duplicated sample names. Check if your samples are formatted according to the selected mode"

  # report
  [[ $status == "FAIL" ]] && logerror "Check NOT successful. Removing sample sheet: ${outfile}" && [[ $dryrun == false ]] && rm --verbose ${outfile}

  return 0
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
check_mode
show_info
[[ $interactive == true ]] && interactive_query # from helper_functions.sh

## Workflow
create_sample_sheet
[[ ${check} == true ]] && validate_sample_sheet

## Summary
if [[ $samples_found -gt 0 ]]; then
  [[ $status == PASS ]] && logsuccess "Found $samples_found samples"
else
  [[ $dryrun == false ]] && logwarn "No specific samples found. Removing sample sheet: ${outfile}" && rm --verbose ${outfile}
fi

logglobal "[STOP] $script_real"
exit 0
