# get reference loci of cgmlst scheme

schemedir="/cephfs/abteilung4/NGS/ReferenceDB/MLST/active_schemes/Salmonella/enterobase_cgmlst/enterobase_senterica_cgmlst/short"

outfile="/cephfs/abteilung4/deneke/Projects/snippysnake_dev/vcf_annotation/cgmlst_loci.fasta"

for fasta in $schemedir/*.fasta; do
    echo "Processing $fasta"
    #grep '>' $fasta | head -n 1 >> $outfile
    bioawk -c fastx '{if(NR == 1) print ">"$name" "$comment"\n"$seq}' $fasta >> $outfile
done


# rename

# make abricate db
