#!/bin/bash
set -e
set -u
set -o pipefail



## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type SubSet_SampleSheet.sh --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


## search for "--help" flag
if [[ $@ =~ '--help' || $@ =~ '-h' ]]; then
    echo "Help file ----------------------"
    echo "Basic usage: RefDownloader species path/2/data [--complete]"
    echo
    echo "species: species or genus"
    echo "dbDir: target base directory"
    echo "--complete optional Whether to select complete genomes only"
    echo "--rep optional whether to select representative genomes only"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi   




download_assembly_summary=false #true
use_wget=true


#species=$1
dbDir="$2"

#species='Listeria monocytogenes'
#species='Bacillus'
#species='Brucella canis'
species="$1"
#species='Salmonella enterica'
#species="Francisella tularensis"
#dbDir="/cephfs/abteilung4/NGS/ReferenceDB/NCBI"
#dbDir="/home/carlus/BfR/ReferenceDB/NCBI/Refseq"


#filter="Complete Genome"
#filter="$3" #false
#filter=true

if [[ $@ =~ '--complete' ]]; then
	complete=true
else 
	complete=false
fi


if [[ $@ =~ '--rep' ]]; then
	representative=true
else 
	representative=false
fi



speciesFolder=${species//\"/}
speciesFolder=${speciesFolder// /_}
speciesFolder=${speciesFolder//./}


if [[ ! -d $dbDir ]];then
	echo "Warning: Dir $dbDir does not exist."
	[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


if [[ -d $dbDir/$speciesFolder ]];then
echo "Warning: Dir $dbDir/$speciesFolder already exists"
fi

interactive=true
# echo all parameters:

echo "species: $species"
echo "targetDir: $dbDir"
echo "speciesFolder: $dbDir/$speciesFolder"
echo "complete: $complete"
echo "representative: $representative"
echo "download_assembly_summary: $download_assembly_summary"
echo "use_wget: $use_wget"

# ask if in interactive mode
if [[ $interactive == true ]]
then
	read -p "Do you want to continue? " -n 1 -r
	echo
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
	fi
fi



# RefDownloader ---------------------------------------




#if [[ $download_assembly_summary == true ]]; then

if [[ ! -f $dbDir/assembly_summary.txt || $download_assembly_summary == true ]]; then

#cd $dbDir

 #    Download the /refseq/bacteria/assembly_summary.txt file
#rsync rsync://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt $dbDir

#cd $dbDir
echo "wget -P $dbDir https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt"
wget -P $dbDir https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt

fi


echo "Creating folder $dbDir/$speciesFolder"
mkdir -p $dbDir/$speciesFolder

# List the FTP path (column 20) for the assemblies of interest, in this case those that have "Complete Genome" assembly_level (column 12) and "latest" version_status (column 11). One way to do this would be using the following awk command:



if [[ $complete == false && $representative == false ]]; then
	echo "your choice complete=$complete or representative=$representative"
	grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$11=="latest"{print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
elif [[ $complete == true && $representative == false ]]; then
	echo "your choice complete=$complete or representative=$representative"
	grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
elif [[ $complete == false && $representative == true ]]; then
	echo "your choice complete=$complete or representative=$representative"
	grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$11=="latest" && $5 != "na" {print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
elif [[ $complete == true && $representative == true ]]; then
	echo "your choice complete=$complete or representative=$representative"
	grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$12=="Complete Genome" && $11=="latest" && $5 != "na" {print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
else
	echo "either complete=$complete or representative=$representative not correctly specified"
fi

#    awk -v filter=$filter -F "\t" '$12=="$filter" && $11=="latest"{print $20}' assembly_summary.txt  > ftpdirpaths

# extracting ftp path:
echo "cut -f 20 $dbDir/$speciesFolder/assembly_summary.txt > $dbDir/$speciesFolder/ftpdirpaths"
cut -f 20 $dbDir/$speciesFolder/assembly_summary.txt > $dbDir/$speciesFolder/ftpdirpaths


#if [[ $filter == false ]]; then
#echo "Not filtering for complete genomes"
#grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$11=="latest"{print $20}' > $dbDir/$speciesFolder/ftpdirpaths
#grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$11=="latest"{print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
#else
#echo "Filtering for complete genomes"
#grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $20}' > $dbDir/$speciesFolder/ftpdirpaths
#grep "$species" $dbDir/assembly_summary.txt | awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $0}' > $dbDir/$speciesFolder/assembly_summary.txt
#fi

# Append the filename of interest, in this case "*_genomic.gbff.gz" to the FTP directory names. One way to do this would be using the following awk command:

awk 'BEGIN{FS=OFS="/";filesuffix="genomic.fna.gz"}{ftpdir=$0;asm=$10;file=asm"_"filesuffix;print ftpdir,file}' $dbDir/$speciesFolder/ftpdirpaths > $dbDir/$speciesFolder/ftpfilepaths

cat $dbDir/$speciesFolder/ftpfilepaths | sed 's/ftp:/rsync:/g' > $dbDir/$speciesFolder/rsyncfilepaths


count=`wc -l $dbDir/$speciesFolder/ftpfilepaths | cut -f 1 -d ' '`

if [ $count == 0 ];then
echo "No genomes found"
[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
else 
echo "Found $count genomes "
fi


if [[ $interactive == true ]]
then
	read -p "Do you want to continue to download $count genomes? " -n 1 -r
	echo
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		[[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
	fi
fi



#files_present=`ls $dbDir/$speciesFolder/*.fna.gz | wc -l | cut -f 1 -d ' '`
#echo "Found $count complete genome for $species on refseq. In $dbDir/$speciesFolder are $files_present files already available"


    
#    awk 'BEGIN{FS=OFS="/";filesuffix="genomic.gbff.gz"}{ftpdir=$0;asm=$10;file=asm"_"filesuffix;print ftpdir,file}' ftpdirpaths > ftpfilepaths
#    Use a script to download the data file for each FTP path in the list


while read url; do
	echo "Downloading $url"
	echo "wget -P $speciesFolder $url"
	wget -P $dbDir/$speciesFolder $url
done < $dbDir/$speciesFolder/ftpfilepaths



# if rsync
#while read url; do
	#echo "Downloading $url"
	#echo "rsync --copy-links --times --verbose $url $dbDir/$speciesFolder"
	#rsync --copy-links --times --verbose $url $dbDir/$speciesFolder
#done < $dbDir/$speciesFolder/rsyncfilepaths
#done < <(tail -n 219 $dbDir/$speciesFolder/rsyncfilepaths)
#done < <(head $dbDir/$speciesFolder/rsyncfilepaths) # for testing



