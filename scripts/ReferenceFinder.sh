#!/bin/bash
set -e
set -u
set -o pipefail


# [Goal] Given a set of reads, find the closest reference using mash
# [Author] Carlus Deneke, Carlus.Deneke@bfr.bund.de
version=0.2


# requires mash
check_mash=`which mash`
if [[ -f $check_mash ]];then 
    mash_version=`mash --version`
    echo "mash found in $check_mash. Version $mash_version"
else 
    echo "mash not found in PATH" 
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi



# Read in parameters --------------------------------


OPTIONS=hs:o:in:
LONGOPTIONS=help,referencePath:,sample_sheet:,outdir:,species:,assembly_stats:,interactive,download_reference,copy_reference,custom_db,num_hits:


# Default settings
download_reference=false
copy_reference=false
db_type="default" # "custom"
interactive=false #true
num_hits=1
help=false


#outdir="/cephfs/abteilung4/Projects_NGS/Listeria_outbreaks/reffinder/mash"
#outdir="/home/carlus/BfR/Data/TestData/mash/Salmonella"
#outdir="/home/carlus/BfR/Data/TestData/mash"

#sample_sheet="/cephfs/abteilung4/Projects_NGS/Listeria_outbreaks/trimmedData/samples_cluster1.tsv"
#sample_sheet="/home/carlus/BfR/Data/TestData/GMI/samples.tsv"

#species="Listeria_monocytogenes"
species="Salmonella"


# define reference location:

#referencePath="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/ref/mash/Refseq_completeBacteria_s1000_k21.msh"
#referenceInfo="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/ref/mash/Refseq_completeBacteria_s1000_k21.info"
# note: uses only single chromosome as reference, need to re-group all complete refs without plasmids

# dbs are build with script mash_buildDB.sh

referencePath="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/plasmids_removed/mash/Refseq_completeBacteria_chromosomes_s1000_k21.msh"
referenceInfo="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/plasmids_removed/mash/Refseq_completeBacteria_chromosomes_s1000_k21.info"



#referencePath="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/Refseq_completeBacteriawPlasmids_s1000_k21.msh" # complete genomes, but still containing plasmids
#referenceInfo="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/Refseq_completeBacteriawPlasmids_s1000_k21.info"

#referencePath="/cephfs/abteilung4/NGS/ReferenceDB/mash/refseq.genomes.k21s1000.msh" # official release, no plasmids

#referencePath="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/Current/Refseq_completeBacteria.msh" # custom db on complete bacteria

assembly_stats="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/assembly_summary.txt"



## Helpfile and escape --------------------------------------------------------------
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type ReferenceFinder.sh --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi




PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -s|--sample_sheet)
            sample_sheet="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        --species)
            species="$2"
            shift 2
            ;;
        --assembly_stats)
            assembly_stats="$2"
            shift 2
            ;;
        --num_hits)
            num_hits="$2"
            shift 2
            ;;
        -i|--interactive)
            interactive=true
            shift
            ;;
        #~ -F|--force)
            #~ force=true
            #~ shift
            #~ ;;
        --download_reference)
            download_reference=true
            shift
            ;;
        --copy_reference)
            copy_reference=true
            shift
            ;;
        --custom_db)
            db_type="custom"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done




# call help --------------------------------------
if [[ $help == true ]]
then
    echo "You called the script ReferenceFinder.sh (version $version). Purpose: Get best matching reference for a set of sequence files"
    echo
    echo "============================="
    echo "Call: ReferenceFinder.sh --referencePath path/2/mashref.msh--sample_sheet path/2/samples.tsv --outDir path/out/dir --species species_name --assembly_stats path/2/assembly_summary.txt [Options]"
    
    echo "referencePath: Path to mash reference database"
    echo "sample_sheet: Path to sample sheet. The sample sheet is a tsv with the columns sample_name, path2read_1, path2read2"
    echo "outdir: File output directory"
    echo "species: Filter for species name"
    echo "assembly_stats: Path to NCBI's assembly stats (required for download)"

    echo 
    echo "Options:"
    echo "interactive: Prompt before execution"
    echo "download_reference: Download the best reference"
    echo "custom_db: mash db is a custom databade"

    echo "Output files:"
    echo "mash_distances_raw.tsv: raw mash results"
    echo "mash_distances.tsv: results filtered by name and sorted by similarity score"
    echo "mash_distances_aggregate.tsv: summed score per target sequence"
    echo "refdir/: directory where references are saved"

    echo "============================="
    exit 1

fi


# define folders =================================


if [[ -d $outdir ]];then
echo "Warning: Output folder does not exist"
fi
mkdir -p $outdir


if [[ ! -f $sample_sheet ]];then
    echo "Error: Sample sheet $sample_sheet does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


if [[ ! -f $referencePath ]];then
    echo "Error: Reference DB path $referencePath does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi



if [[ ! -f $assembly_stats ]];then
    echo "Error: Assembly stats file $assembly_stats does not exist."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


refdir="$outdir/ref"




# echo all parameters:


echo "referencePath: $referencePath"
echo "sample_sheet: $sample_sheet"
echo "outdir: $outdir"
echo "species: $species"
echo "assembly_stats: $assembly_stats"
echo "interactive: $interactive"
echo "download_reference: $download_reference"
echo "refdir: $refdir"



# ask if in interactive mode
if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi


# Run program --------------------------------------------------------------------------------------

# create sketches ===========================================================    
#Sketch the reads, using -m 2 to improve results by ignoring single-copy k-mers, which are more likely to be erroneous:

while read sample read1 read2; do
  echo "Sketching sample $sample"
  echo "mash sketch -k 21 -s 1000 -m 2 -o $outdir/$sample <(zcat $read1 $read2)"
  mash sketch -k 21 -s 1000 -m 2 -o $outdir/$sample <(zcat $read1 $read2)
done < <(tail -n +2 $sample_sheet)

# ===========================================================
#Run mash dist with the RefSeq archive as the reference and the read sketch as the query:

#echo "mash dist $referencePath $outdir/*.msh > $outdir/mash_distances_raw.tsv"
#mash dist $referencePath $outdir/*.msh > $outdir/mash_distances_raw.tsv

for file in $outdir/*.msh; do
    outfile=`echo $file | sed 's/msh$/dist/'`
    sample=`basename -s .msh $file`
    echo "mash dist $referencePath $file | sed \"s/\/dev\/fd\/63/$sample/\" > $outfile"
    mash dist $referencePath $file | sed "s/\/dev\/fd\/63/$sample/" > $outfile
done


cat $outdir/*.dist | sed 's/\/1000/\t1000/g' > $outdir/mash_distances_raw.tsv


#sort -gk3 mash_distances_raw.tsv | head -n 10
#mash dist $referencePath $outdir/*.msh | sed 's/\/1000/\t1000/g' | grep $species > $outdir/mash_distances.tsv #species info not present

# filter
cat $outdir/mash_distances_raw.tsv | sed 's/\/1000/\t1000/g' | awk -F'\t' '$5 > 500' > $outdir/mash_distances.tsv

# print top hits
#cat $outdir/mash_distances.tsv | sort -nr -k 5 | head

# compute aggeragated scores (summed score per target)
awk -F'\t' 'BEGIN{OFS="\t"};{summedscore[$1]+=$5;l[$1]+=1}; END{for(value in summedscore) print value, summedscore[value], l[value],summedscore[value]/l[value]}' $outdir/mash_distances.tsv | sort -nr -k 4 > $outdir/mash_distances_aggregate.tsv


#complement with mash info: best hit
#paste <(head -n 1 $outdir/mash_distances_aggregate.tsv) <(grep -f <(head -n 1 $outdir/mash_distances_aggregate.tsv | cut -f 1) $referenceInfo | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{2,}/\t/g' | cut -f 4)


#complement with mash info: all hits
echo -e "ref\ttotal\tcount\tavg\tinfo" > $outdir/mash_distances_aggregate_info.tsv
while read ref total count avg; do
    info=`grep $ref $referenceInfo | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{2,}/\t/g' | cut -f 4`
    echo -e "$ref\t$total\t$count\t$avg\t$info" >> $outdir/mash_distances_aggregate_info.tsv
done < $outdir/mash_distances_aggregate.tsv



# takes long (make optional?)
echo -e "ref\tquery\tdistance\tpvalue\tshared_hashes\ttotal_hashes\tinfo" > $outdir/mash_distances_info.tsv
while read ref query distance pvalue shared_hashes total_hashes; do
    info=`grep $ref $referenceInfo | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{2,}/\t/g' | cut -f 4`
    echo -e "$ref\t$query\t$distance\t$pvalue\t$shared_hashes\t$total_hashes\t$info" >> $outdir/mash_distances_info.tsv
done < $outdir/mash_distances.tsv



#"paste <(head -n 1 {input.distance}) <(grep -f <(head -n 1 {input.distance} | cut -f 1) {params.mashinfo} | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{{2,}}/\t/g' | cut -f 4) > {output}"



if [[ $db_type == "custom" ]]; then
# for custom:
BestGCF=`cat $outdir/mash_distances_aggregate.tsv | head -n 1 | cut -f 1 | cut -f 5 -d'-'`
BestHit=`cat $outdir/mash_distances_aggregate.tsv | head -n 1 | cut -f 1`
# for default DB:
else
echo "Default DB"
#BestGCF=`cat $outdir/mash_distances_aggregate.tsv | head -n 1 | cut -f 1 | cut -f 1-2 -d'_'`
BestGCF=`cat $outdir/mash_distances_aggregate.tsv | head -n 1 | cut -f 1 | awk -F'/' '{print $NF}' | cut -f 1-2 -d'_'`
BestHit=`cat $outdir/mash_distances_aggregate.tsv | head -n 1 | cut -f 1`
fi



if [[ $copy_reference == true ]];then
    echo "Copying best reference"
    echo "cp $BestHit $refdir/reference.fasta"
    mkdir -p $refdir
    cp $BestHit $refdir/reference.fasta.gz
    gunzip $refdir/reference.fasta.gz
fi




#TODO more than one hit
#cat $outdir/mash_distances_aggregate.tsv | head -n $num_hits | cut -f 1 | cut -f 1-2 -d'_'



# Best Hit

#~ db_type == "default"
#~ if [[ $db_type == "custom" ]]; then
#~ # for custom:
#~ BestGCF=`cat $outdir/mash_distances.tsv | sort -nr -k 5 | head -n 1 | cut -f 1 | cut -f 5 -d'-'`
#~ # for default DB:
#~ else
#~ echo "Default DB"
#~ BestGCF=`cat $outdir/mash_distances.tsv | sort -nr -k 5 | head -n 1 | cut -f 1 | cut -f 1-2 -d'_'`
#~ fi



BestURL=`grep --color=never $BestGCF $assembly_stats | cut -f 20`
Best_Accession=`basename $BestURL`
BestURL2fasta="$BestURL/${Best_Accession}_genomic.fna.gz"
BestURL2gbff="$BestURL/${Best_Accession}_genomic.gbff.gz"

echo "Best Hit:"
echo -e "$BestGCF\t$BestURL\t$BestURL2fasta\t$BestHit"


# Download reference

if [[ $download_reference == true ]];then
    mkdir -p $refdir
    echo "wget -P $refdir $BestURL2fasta"
    wget -P $refdir $BestURL2fasta
    echo "wget -P $refdir $BestURL2gbff"
    wget -P $refdir $BestURL2gbff
fi


# unzip references
for file in $refdir/*.gz; do
    gunzip $file
done


