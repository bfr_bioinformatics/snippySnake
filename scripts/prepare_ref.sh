#!/bin/bash
#set -e
#set -u
#set -o pipefail

# [Goal] Prepare annotated reference from fasta OR fastq

# TODO
# manage input
# assembly
# annotate
# add cgmlst coordinates?

VERSION="0.1"

OPTIONS=h,i,n,o,r,a,f,r
LONGOPTIONS=help,file1:,file2:,interactive,dryrun,reads,assembly,outdir:,cgmlst-scheme:,threads:,force,referencename:,phages

# initialize arguments
help=false;file1="";file2="";interactive=false;dryrun=false;outdir="";reads=false;assembly=false;cgmlstscheme="";threads=10;force=false;referencename="";do_phispy=false


#conda_env="/home/DenekeC/anaconda3/envs/seeman_tools"
conda_env="/home/DenekeC/miniconda3/envs/snippyextra"

echo "Please make sure that the conda environment $conda_env has been activated"


## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type prepare_ref.sh --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

# ---
PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"




while true; do
    case "$1" in
        -h|--help)
            help=true
            shift
            ;;
        -a|--assembly)
            assembly=true
            shift
            ;;
        -r|--reads)
            reads=true
            shift
            ;;
        -n|--reference-name)
            referencename="$2"
            shift 2
            ;;
        --file1)
            file1="$2"
            shift 2
            ;;
        --file2)
            file2="$2"
            shift 2
            ;;
        -o|--outdir)
            outdir="$2"
            shift 2
            ;;
        --cgmlst-scheme)
            cgmlstscheme="$2"
            shift 2
            ;;
        -t|--threads)
            threads="$2"
            shift 2
            ;;
        --dryrun)
            dryrun=true
            shift
            ;;
        --phages)
            do_phispy=true
            shift
            ;;
        -i|--interactive)
            interactive=true
            shift
            ;;
        -f|--force)
            force=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done




echo "help: $help"

# ---
if [[ $help == true ]]
then
    echo "Help file ----------------------"
    echo "Basic usage: prepare_ref.sh --file1 --outdir [--file2 --assembly --reads --cgmlst-scheme --interactive]"
    echo
    echo
    echo "Requires tools shovill, prokka"
    echo "Make sure you have prokka and shovill available. E.g. do conda active $conda_env"
    echo
    echo "--file1 path to first input file - either fastq.gz or fasta file"
    echo "--outdir path to results"
    echo "--file2 path to second input file, required for --reads [OPTIONAL] "
    echo "--assembly Input is assembly"
    echo "--reads Input are reads and requires assembly (supply --file1 and --file2)" 
    echo "--cgmlst-scheme [OPTIONAL] Path to a preprered fasta file with all cgmlST loci as peptide sequences. See /home/NGSAdmin/HPC_referencedb/cgmlst/prokka_references"
    echo "--interactive [OPTIONAL] "
    echo "--force [OPTIONAL] "
    echo "--phages [OPTIONAL] Also predict phages using phispy"
    #echo "--dryrun [OPTIONAL] "

    echo
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    exit 1
fi


# define data


# input

#echo "file1: $file1"
#echo "file2: $file2"
#echo "outdir: $outdir"
#echo "type: $type"


# checks

echo "Checking ..."

if [[ $assembly == true && $reads == true ]]; then
    type="NA"
    echo "ERROR: Specify only single input type: Either --reads OR --assembly"
    exit 1
elif [[ $assembly == true ]]; then
    type="assembly"
elif [[ $reads == true ]]; then
    type="reads"
else
    type="NA"
    echo "ERROR: Specify a single input type: Either --reads OR --assembly"
    exit 1
fi

# file input
if [[ ! -f $file1 ]]; then
    echo "ERROR: file1 $file1 does NOT exists"
    exit 1
fi


if [[ ! -f $file2 && $reads == true ]]; then
    echo "ERROR: file2 $file2 does NOT exists but mode --reads specified"
    exit 1
fi


# outdir


# handle not existing outdir
echo $outdir
if [[ $outdir == "" ]]; then
    outdir=`dirname $file1`
    echo "Choosing assembly dir $outdir as output"
fi


if [[ -d $outdir && $force == false ]]; then
    echo "ERROR: outdir $outdir already exists and --force NOT specified"
    exit 1
fi




# make it a full path
outdir=`realpath $outdir`
summaryfile="$outdir/summary_prepareref.txt"


# outdir
if [[ $cgmlstscheme != "" ]]; then
    echo "USING a cgmlst scheme for annotation"
    cgmlst=true
    if [[ ! -f $cgmlstscheme ]]; then
        echo "ERROR: path 2 cgmlst scheme is not correct: $cgmlstscheme"
        exit 1
    fi
else
    cgmlst=false
fi



# check depdenecies

if ! command -v prokka &> /dev/null
then
    echo "prokka could not be found"
    echo "Make sure you have prokka and shovill available. E.g. do conda active $conda_env"
    exit 1
else
    echo "prokka OK"
fi

if ! command -v shovill &> /dev/null
then
    echo "shovill could not be found"
    echo "Make sure you have prokka and shovill available. E.g. do conda active $conda_env"
    exit 1
else
    echo "shovill OK"
fi


if [[ $do_phispy == true ]]; then
    if ! command -v PhiSpy.py &> /dev/null
    then
        echo "PhiSpy.py could not be found"
        echo "Make sure you have PhiSpy.py, prokka and shovill available. E.g. do conda active $conda_env"
        exit 1
    else
        echo "PhiSpy.py OK"
    fi
fi

#TODO check that file1 is reads

#if file $file1 | grep -q compressed  ; then
#     echo "$file1 is compressed"
    

if [[ $type == "reads" ]]; then
     if zcat $file1 | head -n 1 | grep -q '^@'; then
        echo "confirmed: is fastq file"
     else
        echo "ERROR: NOT a fastq file"
        exit 1
     fi
elif [[ $type == "assembly" ]]; then
     if cat $file1 | head -n 1 | grep -q '^>'; then
        echo "confirmed: is fasta file"
     else
        echo "ERROR: NOT a fasta file"
        exit 1
     fi
fi



# check that cgmlstscheme is a set a single fasta files with all loci (properly formated)

if [[ $cgmlst == true ]]; then
    if cat $cgmlstscheme | head -n 1 | grep -q '^>'; then
        echo "Confirmed. cgmlstscheme is a fasta file" 
    else
        echo "Error: cgmlstscheme $cgmlstscheme is NOT a fasta file"
        exit 1

    fi
fi



# phispy


phispy_hmmdb="/cephfs/abteilung4/deneke/Projects/PhageCharac/databases/pVOGs/pVOGs.hmm"


if [[ $do_phispy == true ]]; then
    if [[ ! -f $phispy_hmmdb ]]; then
        echo "error: phispy_hmmdb $phispy_hmmdb is missing"
        exit 1
    fi
fi




echo "DONE Checking."
echo
echo "---------------------------------" | tee $summaryfile
echo "Parameters" | tee -a $summaryfile
echo | tee -a $summaryfile
echo "file1: $file1" | tee -a $summaryfile
echo "file2: $file2" | tee -a $summaryfile
echo "outdir: $outdir" | tee -a $summaryfile
echo "type: $type" | tee -a $summaryfile
echo "Do cgmlst: $cgmlst" | tee -a $summaryfile
echo "cgmlstscheme: $cgmlstscheme" | tee -a $summaryfile
echo "threads: $threads" | tee -a $summaryfile
echo "do_phispy: $do_phispy" | tee -a $summaryfile
echo "phispy_hmmdb: $phispy_hmmdb" | tee -a $summaryfile
echo "---------------------------------" | tee -a $summaryfile
echo


# interactive
# ask if in interactive mode
if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi




# EXECUTION ---------------------------------------------------------------


echo "Starting prepare_ref.sh (version $VERSION) on `date`" | tee -a $summaryfile

# assembly
if [[ $type == "reads" ]]; then
    reffasta=$outdir/shovill/contigs.fa
    
    if [[ -f $reffasta ]]; then
        echo "Assembly already exists: $reffasta. Skipping shovill" | tee -a $summaryfile
    else
        echo "Performing assembly from reads using shovill..."
        
        echo "shovill --force --noreadcorr --assembler spades --cpus $threads --outdir $outdir/shovill --R1 $file1 --R2 $file2" | tee -a $summaryfile
        shovill --force --noreadcorr --assembler spades --cpus $threads --outdir $outdir/shovill --R1 $file1 --R2 $file2

        if [[ ! -f $reffasta ]]; then
            echo "assembly not availabe: File $reffasta does NOT exist" | tee -a $summaryfile
            # exit 1
        fi
    fi # end if shovill
    
else 
    reffasta=$file1
    echo "Not performing assembly since you provided $reffasta"
fi


# annotation -----------------

# annotation parameters
if [[ $cgmlst == true ]]; then
    echo "add and check cgmlst"
    prokka_extra_parameters="--proteins $cgmlstscheme"
else
    echo "Not checking cgmlst" 
    prokka_extra_parameters=""
fi


# run prokka 
prokkadir=$outdir/prokka


# TODO change to sample name in reference
sample=`basename -s ".fasta" $file1`
echo "sample name_ $sample"
reference_genbank="$prokkadir/$sample.gbk"


if [[ -f $reference_genbank ]]; then
    echo "Prokka already exists. Skipping prokka." | tee -a $summaryfile
else

    echo "Run prokka ..."
    mkdir -p $prokkadir

    echo "prokka --force --kingdom Bacteria --cpus $threads --outdir $prokkadir --prefix $sample $prokka_extra_parameters $reffasta" | tee -a $summaryfile
    prokka --force --kingdom Bacteria --cpus $threads --outdir $prokkadir --prefix $sample $prokka_extra_parameters $reffasta

# --outdir $prokkadir --prefix reference --locustag {wildcards.sample} {input}
#prokka  --force --dbdir {params.db} --cpus {threads} {params.extra} --genus {params.genus} --evalue {params.evalue} --coverage {params.coverage} --kingdom Bacteria --outdir {params.outdir} --prefix {wildcards.sample} --locustag {wildcards.sample} {input}

    if [[ ! -f $reference_genbank ]]; then
        echo "genbank reference not availabe: File $reference_genbank does NOT exist" | tee -a $summaryfile
        exit 1
    else 
        echo "Reference is $reference_genbank"
    fi


fi # end prokka


# prokka summary: genes, loci etc

# genome length
genome_length=`grep 'bases' $prokkadir/reference.txt | cut -f 2 -d ' '`
# contigs
contig_count=`grep 'contigs' $prokkadir/reference.txt | cut -f 2 -d ' '`
# CDS
cds_count=`grep 'CDS' $prokkadir/reference.txt | cut -f 2 -d ' '`
#`wc -l $prokkadir/reference.tsv | cut -f 1 -d ' '`
# loci
if [[ $cgmlst == true ]]; then
    locus_count=`grep -c 'cgMLSTlocus' $prokkadir/reference.tsv`
else
    locus_count="NA"
fi




# TODO predict phages using phispy

do_phispy=false # remove as long as not finished (need parsing, need problems with dependencies fixed)
if [[ $do_phispy == true ]]; then
    phispydir="$outdir/phispy"
    if [[ -d $phispydir ]]; then
        echo "phispydir $phispydir already exists. Skipping phispy"
    else
        mkdir -p $phispydir

        phage_information="$phispydir/reference_prophage_information.tsv"
        phage_coordinates="$phispydir/reference_prophage_coordinates.tsv"
        phage_fasta="$phispydir/reference_phage.fasta"
        phispylog="$phispydir/phispy.log.txt"

        echo "PhiSpy.py -o $phispydir --phmms $phispy_hmmdb --output_choice 15 -p "reference" --keep --threads $threads --log $phispylog $reference_genbank"
        PhiSpy.py -o $phispydir --phmms $phispy_hmmdb --output_choice 15 -p "reference" --keep --threads $threads --log $phispylog $reference_genbank

        if [[ ! -f $phage_information ]]; then
            echo "ERROR: Phispy failed: File $phage_information is missing".
            exit 1
        else
            echo "Phispy ok: Results in $phispydir"
        fi

        # TODO create bed file

    fi

fi # if phispy

# report

# print summary
echo | tee -a $summaryfile
echo "--------------------------------------------------------------------" | tee -a $summaryfile
echo "Summary:" | tee -a $summaryfile
echo "genome_length: $genome_length" | tee -a $summaryfile
echo "contig_count: $contig_count" | tee -a $summaryfile
echo "cds_count: $cds_count" | tee -a $summaryfile
echo "cgmlst_locus_count: $locus_count" | tee -a $summaryfile
echo "reference genbank: $reference_genbank" | tee -a $summaryfile
echo "assembly fasta: $reffasta" | tee -a $summaryfile
echo "--------------------------------------------------------------------" | tee -a $summaryfile
echo  | tee -a $summaryfile

echo "Done on `date`. Results in $outdir. Please specify reference $reference_genbank"  | tee -a $summaryfile

echo "$reference_genbank"


#echo "END TEST"
#exit 0


#input=$1 # comma separated

#input="/cephfs/abteilung4/NGS/Data/MiSeq/170714_M03380_0071_000000000-B67K7/Analysis/trimmed/BfR-CA-15082_R1.fastq.gz,/cephfs/abteilung4/NGS/Data/MiSeq/170714_M03380_0071_000000000-B67K7/Analysis/trimmed/BfR-CA-15082_R2.fastq.gz"


#sample=""
#reference_name="$3"

# split by , or ;


#BfR-CA-15082    /cephfs/abteilung4/NGS/Data/MiSeq/170714_M03380_0071_000000000-B67K7/Analysis/trimmed/BfR-CA-15082_R1.fastq.gz  /cephfs/abteilung4/NGS/Data/MiSeq/170714_M03380_0071_000000000-B67K7/Analysis/trimmed/BfR-CA-15082_R2.fastq.gz




# outdir


#outdir="/cephfs/abteilung4/NRL_Campylobacter/Clusteranalysen/2021-01474_Anfrage_Hiller/reference_autogeneration"

# options

# load conda env

# try presence of tools
