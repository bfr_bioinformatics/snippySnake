#!/usr/bin/env Rscript

# [Goal] Filter high density SNPs

message("[START] filter_highdensity_snps.R")

suppressPackageStartupMessages(library(dplyr, quietly = T, warn.conflicts = F))
suppressPackageStartupMessages(library(Biostrings, quietly = T, warn.conflicts = F))
#library(seqinr)

# input core.vcf or core.tab
# output core.filtered and filtered alifgnment
# maybe SNP distance matrix
# methods: density region or adjacent SNPs

# Also filter other variants? not in core
# Also filter annotated variant table?
# define data ------

if (exists("snakemake")) {
  core.tab_file           <- snakemake@input[["core_tab"]]
  filtered_alignment_file <- snakemake@output[["filtered_alignment"]]
  annotated_core_tab_file <- snakemake@output[["annotated_core_tab"]]
  snp_threshold           <- snakemake@params[["snp_threshold"]]
  window_size             <- snakemake@params[["window_size"]]
  filter_type             <- snakemake@params[["filter_type"]]
  functions_file          <- file.path(snakemake@params[["workflow_dir"]], "scripts/functions.R")
} else {
  warning("In debug mode")
  core.tab_file           <- "/cephfs/abteilung4/NRL_Campylobacter/Clusteranalysen/20220504_RKI/snp_calling/results/core.tab"
  outdir                  <- "/cephfs/abteilung4/NRL_Campylobacter/Clusteranalysen/20220504_RKI/snp_calling/results"
  filtered_alignment_file <- file.path(outdir, "core.filtered.aln")
  annotated_core_tab_file <- file.path(outdir, "core.filtered.tab")

  snp_threshold <- 3 # number of snps in window
  window_size   <- 1000
  filter_type   <- "window"
  # filter_type   <- "consecutive"

  functions_file <- "/home/DenekeC/Snakefiles/snippySnake/scripts/functions.R"
  #functions_file <- "scripts/functions.R"
}

# read data ------

stopifnot(file.exists(core.tab_file))
core_tab <- read.delim(core.tab_file, stringsAsFactors = F, check.names = F)

# functions -----
stopifnot(file.exists(functions_file))
source(functions_file)

# Method1 ----

if (filter_type == "window") {
  message("Using SNP filtering with window of size ", window_size, " and SNP threshold of ", snp_threshold)
} else if (filter_type == "consecutive") {
  warning("Consecutive mode, setting window_size to snp_threshold ", snp_threshold)
  window_size <- snp_threshold
} else {
  stop("filter_type ", filter_type, " not recognized")
}

core_tab_annotated <- get_density_information(core_tab, distance_filter = window_size, min_cluster_size = snp_threshold, verbose = F)
core_tab_filtered  <- core_tab_annotated %>% filter(in_high_density_site == F)


# compute distance matrix ------

# create new core alignment
core_tab_basepairs <- core_tab_filtered %>% select(-c(CHR, POS, REF, n, high_density_site, in_high_density_site))

core_alignment <- lapply(1:ncol(core_tab_basepairs), function(i) {
  seq        <- paste(core_tab_basepairs[, i], collapse = "")
  #names(seq) <- paste0(">",colnames(core_tab_basepairs)[i])
  names(seq) <- colnames(core_tab_basepairs)[i]
  return(seq)
})
#names(core_alignment) <- colnames(core_tab_basepairs)

core_alignment_cat <- do.call(c, core_alignment)
# core_alignment_cat <- do.call(cbind,core_alignment)
# core_alignment_cat
# write(core_alignment_cat,file = file.path(outdir,"core.filtered.aln"), sep="\n")
# readr::write_lines(core_alignment_cat,file = file.path(outdir,"core.filtered.aln"), sep="\n")
# cat(core_alignment_cat)
# cat(core_alignment_cat,file = file.path(outdir,"core.filtered.aln"), labels = names(core_alignment_cat))
#seqinr::write.fasta(sequences = core_alignment,names(core_alignment),file.out = file.path(outdir,"core.filtered.aln"))

# TODO
# export using Biostrings ------

core_alignment_biostrings <- DNAStringSet(x = core_alignment_cat)
writeXStringSet(core_alignment_biostrings, filepath = filtered_alignment_file)

# TODO: exception handling for empty string?

# export table

write.table(core_tab_annotated, annotated_core_tab_file, sep = "\t", quote = F, row.names = F)

# report
table(core_tab_annotated$in_high_density_site)
count_removed_snps <- sum(core_tab_annotated$in_high_density_site == T)

message("Removed ", count_removed_snps, " SNPs in high density regions")
message("[STOP] filter_highdensity_snps.R")
