#!/bin/bash

## [Goal] Install mamba, conda env, complement conda env, download databases, download test data for BakCharak
## [Author] Holger Brendebach
pipeline="snippySnake"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_real [OPTIONS]

${bold}Description:${normal}
This script completes the installation of the $pipeline pipeline. The openssl library is required for hashing the downloaded files.
For $pipeline installations from Gitlab use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/${pipeline,,}

${bold}Options:${normal}
  -s, --status               Show installation status
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default: ${pipeline,,})
  -m, --mamba                Install the latest version of mamba to the conda base environment and
                             create the $pipeline environment from the git repository recipe
  -t, --test_data            Download test data to <$pipeline>/download and extract them in <$pipeline>/test_data
  --sra_toolkit              Download test data from NCBI SRA. Requires the installation of the SRA toolkit to a separate conda environment. Please read the Note section below.
  -f, --force                Force overwrite for downloads in <$pipeline>/download
  -k, --keep_downloads       Do not remove downloads after extraction
  -a, --auto                 Skip interactive confirmation dialogue (default=false), e.g. for tmux-wrapped script calls
  -h, --help                 Show this help

${bold}Example:${normal}
Downloading test data from NCBI SRA requires the installation of the SRA toolkit to the snippySnake conda environment.
The SRA toolkit is not included in the snippySnake conda recipe. Hence, please create a new env and install the SRA toolkit with this command:
  mamba create -n sra-toolkit sra-tools
It is advisable to install the SRA toolkit in a separate environment to avoid conflicts with other tools and to use the --keep_downloads until the test data is downloaded.

${bold}Example:${normal}
bash $script_real --env_name ${pipeline,,} --mamba --test_data --keep_downloads --dryrun

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname $script_path)
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  arg_dryrun=""
  arg_force=""

  ## default values of switches
  conda_status=false
  mamba_status=false
  bioconda_status=false

  arg_mamba=false
  arg_test_data=false
  arg_sra_toolkit=false
  arg_status=false
  arg_keep_dl=false
  force=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -m | --mamba)
        arg_mamba=true
        shift
        ;;
      -t | --test_data)
        arg_test_data=true
        shift
        ;;
      --sra_toolkit)
        arg_sra_toolkit=true
        shift
        ;;
      -s | --status)
        arg_status=true
        shift
        ;;
      -f | --force)
        force=true
        arg_force="--force"
        shift
        ;;
      -k | --keep_downloads)
        arg_keep_dl=true
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        arg_dryrun=" --dryrun"
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. For the manual, type ${blue}bash $script_real --help${normal}"

  ## checks
  [[ $(basename $script_path) != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution. Aborting..."
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  ## avoid deletion of pre-existing downloads
  [[ -d "$repo_path/download" && $arg_keep_dl == false ]] && logwarn "Skipping default clean-up because download directory already exists: $repo_path/download" && arg_keep_dl=true

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Clean-up on unexpected behaviour

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  [[ -d "$repo_path/download" ]] && [[ -n ${arg_keep_dl+x} ]] && logheader "Clean Up:" && rm -v -R $repo_path/download
}

[[ $(declare -F cleanup) == cleanup && "$arg_keep_dl" == false ]] && logecho "Clean up of download directory is enabled" && trap cleanup SIGINT SIGTERM EXIT ERR

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths()

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Use SRA toolkit:|$arg_sra_toolkit
Keep Downloads:|$arg_keep_dl
Logfile:|$logfile
" | column -t -s="|" | tee -a $logfile $logfile_global
}

## Modules --------------------------------------------------

download_file() {

  # init local variables
  local remote_archive local_archive hashfile extraction_directory
  remote_archive=$1
  local_archive=$2
  hashfile=$3
  extraction_directory=${4:-}

  # set global variable
  download_success=false

  # Download
  if ( [[ ! -f $local_archive ]] || [[ $force == true ]] ); then
    logecho "Downloading $remote_archive to $local_archive"
    logexec "wget --output-document $local_archive $remote_archive" && download_success=true

    # Verify Integrity of Download
    [[ "$download_success" == true  ]] && [[ -s $local_archive ]] && logexec "openssl dgst -r -md5 $local_archive >> $hashfile"
    [[ "$download_success" == false ]] && logwarn "A download error occurred"
  else
    logwarn "The file $local_archive already exists. Skipping download"
  fi

  if [[ -f $local_archive ]] && [[ -n $extraction_directory ]] && ( [[ "$download_success" == true  ]] || [[ $force == true ]] ); then
    # Unpack Downloads
    logecho "Extracting archive $(basename $local_archive) to $extraction_directory"
    [[ ! -d "$extraction_directory" ]] && logexec mkdir -p $extraction_directory
    logexec gzip --keep --force --decompress $local_archive
    logexec mv ${local_archive%.gz} $extraction_directory
  fi

  return 0
}

setup_mamba() {
  # checks
  [[ "$bioconda_status" == false ]] && logecho "Installing the latest version of \"mamba\" to the Conda base environment"
  [[ "$bioconda_status" == true  ]] && die "This is a BioConda installation. There is no need to setup Mamba or an environment for $pipeline"

  source $conda_base/etc/profile.d/conda.sh
  [[ "$mamba_status" == false ]] && logexec conda install --channel conda-forge mamba
  [[ "$mamba_status" == true ]] && logwarn "Skipping Mamba installation. Mamba is already detected"

  # Create Conda Environment for pipeline
  logecho "Creating the $pipeline environment \"${conda_recipe_env_name}\" with the Conda recipe: $conda_recipe"

  set +eu  # workaround: see https://github.com/conda/conda/issues/8186
  [[ -z "${conda_env_path}" ]] && logexec "mamba env create -n ${conda_recipe_env_name} -f $conda_recipe || true"
  [[ -n "${conda_env_path}" && "$force" == true && ! -d "${conda_base}/envs/${conda_env_name}" ]] && logexec "mamba env create -p ${conda_base}/envs/${conda_env_name} -f $conda_recipe || true"  # corner case: there is already a conda env with the same name in another conda_base, e.g. NGSAdmin
  set -eu

  [[ -n "${conda_env_path}" ]] && logwarn "A Conda environment with the name \"$conda_env_name\" is already present. Skipping environment creation" && return 0
  logsuccess "Environment installation completed"
}

download_test_data() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download"                        ]] && logexec mkdir -p $repo_path/download
  [[ ! -d "$repo_path/test_data/reference"             ]] && logexec mkdir -p $repo_path/test_data/reference
  [[ ! -d "$repo_path/test_data/samples"               ]] && logexec mkdir -p $repo_path/test_data/samples
  hashfile="$repo_path/download/test_data.md5"

  # Check for Reference Genome and SRA Accession Lists
  reference_table="$repo_path/test_data/reference_accession.tsv"
  accession_table="$repo_path/test_data/sra_accessions.tsv"
  [[ ! -f "$repo_path/test_data/reference_accession.tsv" ]] && die "The reference genome list is missing: $reference_table"
  [[ ! -f "$repo_path/test_data/sra_accessions.tsv"    ]] && die "The SRA accession list is missing: $accession_table"

  # Download Reference Genome
  remote_archive=$(grep "ftp_link" "$reference_table" | cut -f2)
  local_archive="$repo_path/download/$(basename "$remote_archive")"
  extraction_directory="$repo_path/test_data/reference"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  # Download Sample Genomes
  extraction_directory="$repo_path/test_data/samples"
  if [[ ${arg_sra_toolkit:-false} == true ]]; then
    [[ ! -x "$(command -v "prefetch")" ]] && logerror "The SRA toolkit is not available. Please install the SRA toolkit to download SRA runs by accession IDs" && return 1
    mapfile -t accessionArray < <(grep -vP "^#" "$accession_table" | cut -f1 | sort -u)

    logheader "Downloading test data from NCBI SRA in sra format:"  # Note: the conda run command requires a newer conda version (>=4.12)
    logexec find "$repo_path/download" -type f -name "*.sra.lock" -delete  # remove lock files from interrupted downloads
    [[ $force == true ]] && arg_force="--force no"
    logecho "conda run --no-capture-output -n sra-toolkit prefetch --transport http ${arg_force} --resume yes --verify yes --output-directory $repo_path/download ${accessionArray[*]}"
             conda run --no-capture-output -n sra-toolkit prefetch --transport http ${arg_force} --resume yes --verify yes --output-directory $repo_path/download ${accessionArray[*]} |& tee -a $logfile
    [[ $force == true ]] && arg_force="--force" # reset

    logecho "Checking for missing .fastq files before executing fasterq-dump"
    missing_fastq=()
    for accession in "${accessionArray[@]}"; do
      [[ ! -f "$extraction_directory/${accession}_1.fastq" ||  ! -f "$extraction_directory/${accession}_2.fastq" ]] && missing_fastq+=("$accession")
    done

    if [[ ${#missing_fastq[@]} -gt 0 || $force == true ]]; then
      logheader "Deriving fastq files from sra files:"
      logexec find "$extraction_directory" -type d -name "fasterq.tmp*" -exec rm -R {} +  # remove interrupted extractions
      logecho "conda run --no-capture-output --cwd $repo_path/download --name sra-tools fasterq-dump ${accessionArray[*]} --outdir $extraction_directory --temp $extraction_directory --format fastq --details ${arg_force} --size-check on"  # NOTE: fasterq-dump ignores the --force option and overwrites existing files every time
               conda run --no-capture-output --cwd $repo_path/download --name sra-tools fasterq-dump ${accessionArray[*]} --outdir $extraction_directory --temp $extraction_directory --format fastq --details ${arg_force} --size-check on |& tee -a $logfile
      logexec "openssl dgst -r -md5 $extraction_directory/*.fastq >> $hashfile"
    else
      logecho "All fastq files are already present. Skipping fasterq-dump."
    fi
    download_success=true
  else
    logheader "Download Sample Genomes from hard-coded FTP links:"
    mapfile -t accessionArray < <(grep -vP "^#" "$accession_table" | cut -f2)
    for remote_archive in "${accessionArray[@]}"; do
      local_archive="$extraction_directory/$(basename "$remote_archive")"
      download_file "$remote_archive" "$local_archive" "$hashfile"
    done
  fi

  # Create Sample Sheet for Test Data
  [[ "$download_success" == true ]] && logecho "Creating test data sample sheet $repo_path/test_data/assemblies/samples.tsv" 
  [[ "$download_success" == true ]] && bash ${script_path}/create_sampleSheet.sh --force --mode ncbi --fastxDir $repo_path/test_data/samples --outDir $extraction_directory ${arg_dryrun:-} && echo

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/test_data/$(basename $hashfile)"

  logsuccess "Test data download completed"
}

check_success() {
  [[ -d ${conda_env_path:-} ]] && status_conda_env="${green}OK${normal}"
  logheader "Database Status:"
  echo "
status_conda_env:|${status_conda_env:-"${red}FAIL${normal}"}
" | column -t -s "|"
  echo
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info

## Workflow
check_conda $conda_recipe_env_name
[[ "$interactive" == true ]] && interactive_query
[[ "$arg_mamba" == true ]] && setup_mamba
[[ "$arg_test_data" == true ]] && download_test_data
[[ "$arg_keep_dl" == false ]] && cleanup
[[ "$arg_status" == false ]] && check_conda $conda_recipe_env_name  # check changes to conda
show_info
check_success

logglobal "[STOP] $script_real"
echo "Thank you for installing $pipeline"
