---
title: "SNP Report from snippySnake pipeline"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: html_document
---

<style type="text/css">
.main-container {
  max-width: 3600px;
  margin-left: auto;
  margin-right: auto;
}
</style>

<style>
  tbody tr:hover {
  background: yellow;
} 

</style>


```{r setup, include=FALSE}

# output:
#   html_document:
#     toc: true
#     toc_depth: 2


knitr::opts_chunk$set(out.width = '80%',fig.asp= 0.5,fig.align='center',echo=FALSE, warning=FALSE, message=FALSE, fig.height = 20, fig.width = 20, fig.align = "center")
options(markdown.HTML.header = system.file("misc", "datatables.html", package = "knitr"))

executor <- Sys.info()["user"]

```

---
author: `r paste0(executor)`
---


```{r load_packages, echo=FALSE}

suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(reshape2)) # for converting matrix
suppressPackageStartupMessages(library(ggplot2)) # for plotting (which?)
suppressPackageStartupMessages(library(ape)) # read/== TRUE data
suppressPackageStartupMessages(library(DT)) # datatable 
# suppressPackageStartupMessages(library(ggtree)) # nicer plots 
suppressPackageStartupMessages(library(dendextend)) ## get node attributes
suppressPackageStartupMessages(library(plotly)) # interactive plots/trees

```

```{r define_functions, echo=FALSE}

read_distance_matrix <- function(file, sep = " ", header = FALSE){
  if(!file.exists(file)) {stop(paste("File",file,"does Not exist")); return(NA)}
  distance_matrix <- read.delim(file, sep=sep, stringsAsFactors = F, header = header)
  rownames(distance_matrix) <- sub(".fasta","",distance_matrix[,1])
  distance_matrix <- distance_matrix[,-1]
  colnames(distance_matrix) <- rownames(distance_matrix)
  distance_matrix <- as.matrix(distance_matrix[order(colnames(distance_matrix)),order(colnames(distance_matrix))])
  return(distance_matrix)
}

reorder_cormat <- function(cormat){
  # Use correlation between variables as distance
  #dd <- as.dist((1-cormat)/2)
  dd <- as.dist(cormat)
  hc <- hclust(dd)
  cormat <-cormat[hc$order, hc$order]
}

get_upper_tri <- function(cormat){
  cormat[lower.tri(cormat)]<- NA
  return(cormat)
}


plot_distance_matrix <- function(distance_matrix2, param_color_thr = c(0,1,5,10,20), param_size = 3, param_legendsize = 12, label_color = "black"){
  
  # remove diagonal elements
  diag(distance_matrix2) <- NA
  
  # Reorder the correlation matrix
  distance_matrix2 <- reorder_cormat(distance_matrix2)
  
  # convert to data frame
  
  # melt matrix
  distance_matrix.m <- reshape2::melt(distance_matrix2, na.rm = TRUE, value.name="Distance",varnames=c("Sample1","Sample2"), id.vars = "id")
  

  p1 <- ggplot(distance_matrix.m, aes(Sample1, Sample2)) +
    geom_tile(aes(fill = Distance), colour = "white", show.legend = F) + 
    geom_text(aes(label = round(Distance, 1)), size = param_size) +
    scale_fill_gradientn(colours = c("white", "yellow", "orange", "red"), values = scales::rescale(param_color_thr)) +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, size = param_legendsize, hjust = 1, color = label_color),  axis.text.y = element_text(size = param_legendsize, color = label_color), axis.title.x = element_blank(),
          axis.title.y = element_blank()) + coord_fixed()
  
  
  return(p1)
  
}


currentScript <- function() {
  .getSourcedScriptName <- function() {
    for (i in sys.nframe():1) {
      x <- sys.frame(i)$ofile
      if (!is.null(x)) {return(normalizePath(x))}
    }
  }
  if (is.null(.getSourcedScriptName())) {
    return(normalizePath(rstudioapi::getActiveDocumentContext()$path))
  } else {
    .getSourcedScriptName()
  }
}


```


```{r define_Data, echo=FALSE}

autoref <- F # TODO infer from input, as parameter

# if flag debug_mode == TRUE, specify paths per hand, otherwise via snakamake
#debug_mode <- T # params from file report_parameters
#debug_mode <- F # params from snakemake

if (!exists("snakemake")) {
  warning("DEBUG MODE: LOAD PARAMETERS FROM FILE")
  #source(file = file.path(dirname(currentScript()), gsub("\\.Rmd", "_debug.R", basename(currentScript()))))
  parameter_file <- file.path(dirname(currentScript() ),"report_parameters.R")
  if(!file.exists(parameter_file)) stop("No parameter file report_parameters.R present: ",parameter_file)
  source(parameter_file)

#if(debug_mode){
#  warning("Calling parameters from file report_parameters.R")
# source("report_parameters.R")
} else {
  workdir <- snakemake@params[["workdir"]]  
  
  
  snp_matrix_file <- file.path(workdir, snakemake@input[["snp_matrix"]])
  core.vcf_file <- file.path(workdir, snakemake@input[["core_vcf"]])
  core.txt_file <- file.path(workdir, snakemake@input[["core_txt"]])

  # coregenome analysis
  coregenome_stats.file <- snakemake@input[["coregenome_stats"]]
  coregenome_summary.file  <- snakemake@input[["coregenome_summary"]]
  
  # params
  configfile <- file.path(workdir,"config_snippysnake.yaml")
  exportpath <- file.path(workdir, snakemake@params[["exportpath"]])
  export <- snakemake@params[["export"]]
  #if(grepl("True",export, ignore.case = T)) export <- TRUE else export <- FALSE
  snp_threshold <- snakemake@params[["snp_threshold"]]
  outlier_threshold <- snakemake@params[["outlier_threshold"]]
  alignment_threshold <-snakemake@params[["alignment_threshold"]] # 0.25  
  
  clustering_method <- "single" # "average"
  cut_threshold_range_raw <- c(50,20,10,5,1)#opt$thresholds
  #cut_threshold_range_raw <- trimws(cut_threshold_range_raw, which = c("both"))
  #cut_threshold_range <- as.numeric(strsplit(cut_threshold_range_raw,"[ ,;]+")[[1]])
  cut_threshold_range <- cut_threshold_range_raw

  do_iqtree <- snakemake@params[["do_iqtree"]]
  do_nj <- snakemake@params[["do_nj"]]
  do_raxml <- snakemake@params[["do_raxml"]]
  do_gubbins  <- snakemake@params[["do_gubbins"]]
  gubbins_stats_file  <- snakemake@params[["gubbins_stats"]]

  
  #nwk_file  <- file.path(workdir, snakemake@input[["nwk"]])
  
    
  if (do_iqtree == TRUE){
   # nwk_file <- file.path(workdir, "results/snp_tree.treefile")  
      nwk_file  <- file.path(workdir, snakemake@input[["nwk"]])
    plot_phylo <- T
    phylo_type="maximum likelihood tree with iqtree"
  } else if (do_nj == TRUE) {
    #nwk_file <- file.path(workdir, "results/snp_tree_nj.nwk")  
      nwk_file  <- file.path(workdir, snakemake@input[["nwk"]])
    plot_phylo <- T
    phylo_type="neighbor-joining likelihood tree with fastnj"
  } else if (do_raxml == TRUE) {
    #nwk_file <- file.path(workdir, "results/core.raxml")  
      nwk_file  <- file.path(workdir, snakemake@input[["nwk"]])
    plot_phylo <- T
    phylo_type="maximum likelihood tree with raxml"
  } else {
    nwk_file <- ""
    plot_phylo <- F
    phylo_type="None"
  }
}

```

```{r load_data, echo=FALSE}


snp_matrix <- read.delim(snp_matrix_file, stringsAsFactors = F)

alignment_stats <- read.delim(core.txt_file, stringsAsFactors = F)
rownames(alignment_stats) <- alignment_stats$ID

# load core genome stats
coregenome_stats <- read.delim(coregenome_stats.file, stringsAsFactors = F, check.names = F)
coregenome_summary <- read.delim(coregenome_summary.file, stringsAsFactors = F, check.names = F)

#colnames(coregenome_stats)[1]  <- "sample"



vcf_raw <- scan(core.vcf_file,what = "character", sep = "\n")
vcf_header_lines <- tail(grep("##",vcf_raw),1)
vcf <- read.delim(core.vcf_file, header = T, skip = vcf_header_lines, check.names=FALSE, stringsAsFactors = F)

if(plot_phylo){
  if (file.exists(nwk_file)){
    nwk <- ape::read.tree(nwk_file)    
  } else {
    warning("Not plotting phylogeny since newick file does not exist")
    plot_phylo <- F
  }
}

# gubbins
if(do_gubbins == TRUE){
  gubbins_stats <- read.delim(gubbins_stats_file, sep="\t",header=T, stringsAsFactors = F)
}


#configfile <- 
config <- yaml::read_yaml(configfile)


#Reference: `r config$ref$fasta`

samples <- as.character(alignment_stats[,1])
```

```{r variants, echo = FALSE}
# remove reference from sample list if not present in vcf column headers

if (any(grepl("Reference",samples)) && !any(grepl("Reference",colnames(vcf)) ) ){
  samples_cleaned <- samples[-grep("Reference",samples)]
} else {
  samples_cleaned <- samples
}
    
# Filtered SNPs
SNPs2Ref <- sapply(as.character(samples_cleaned), function(x) sum(vcf[,x])) # only works if reference is present
SNPs_total <- nrow(vcf)
# TODO count snp positions to reference

Variants2Ref <- alignment_stats[samples_cleaned,"VARIANT"]
names(Variants2Ref) <- alignment_stats[samples_cleaned,"ID"]


```

```{r preprocess_data, echo=FALSE}

# re-format snp distance matrix
rownames(snp_matrix) <- snp_matrix[,1]
colnames(snp_matrix) <- sub("^X","",colnames(snp_matrix))
colnames(snp_matrix) <- gsub("[.]","-",colnames(snp_matrix))
snp_matrix <- as.matrix(snp_matrix[,-1])

# re-order matrix
snp_matrix_clustered <- hclust(as.dist(snp_matrix))
snp_matrix <- snp_matrix[snp_matrix_clustered$order, snp_matrix_clustered$order]

# filter matrix: remove outliers



# criterion1: less than 25% of reference bases should be unaligned or low coverage
outliers1 <- alignment_stats[which((alignment_stats$UNALIGNED + alignment_stats$LOWCOV) > alignment_stats$LENGTH*alignment_threshold),"ID"]


# criterion: less than 1000 variants
# PROBLEM: Discards values then reference is too distant (> 1000)
#outliers2 <- alignment_stats[which(alignment_stats$VARIANT > outlier_threshold),"ID"]

# variants <- alignment_stats$VARIANT
# names(variants) <- alignment_stats$ID
# variants <- variants[which(names(variants) != "Reference")]
# variants_remaining <- sapply(names(variants), function(n){
#   variants[n] - SNPs2Ref[n]
# })
# names(variants_remaining) <- names(variants)
# outliers2 <- variants_remaining[which(variants_remaining > outlier_threshold)]

# criterion distance of a sample is on average larger than

samples_cleaned_noref <- snp_matrix[samples_cleaned,samples_cleaned]
outliers2 <- rownames(samples_cleaned_noref)[apply(samples_cleaned_noref,1,median) > outlier_threshold]


# join
outliers <- unique(c(outliers1,outliers2))



#hist(unlist(snp_matrix), breaks = 100)
#outliers <- which(SNPs2Ref > outlier_threshold)

#outliers <- which(rowMeans(snp_matrix) > outlier_threshold) # remove all samples where mean over all samples is above threshold

#outliers <- which(apply(snp_matrix,1,median) > outlier_threshold) # remove all samples where median over all samples is above threshold
#samples_outliers <- names(outliers)
samples_outliers <- outliers
samples_2keep <- setdiff(rownames(snp_matrix),outliers)

if(length(outliers) > 0 ){
  #snp_matrix <- snp_matrix[-outliers,-outliers]
  snp_matrix <- snp_matrix[samples_2keep,samples_2keep]  
}


# melt matrix
snp_matrix.m <- melt(snp_matrix)
colnames(snp_matrix.m) <- c("Sample1","Sample2","Distance")

# SNP report for analysis `r workdir` {.tabset}
# SNP report for analysis {.tabset}
```

# SNP report {.tabset}

## Overview

Workdir: `r workdir`

Last update on `r strsplit(as.character(file.info(snp_matrix_file)$mtime[1]), split = " ")[[1]][1]`.

Overall, `r SNPs_total` core SNP positions were identified after filtering.

```{r Overview, echo=FALSE}


#hist(snp_matrix, breaks = 50, xlab = "SNPs", main = "Histogram of SNP distances")

#SNPs2Ref_outliers <- alignment_stats[outliers,c("VARIANT","UNALIGNED","LOWCOV")]
SNPs2Ref_outliers <- cbind(coreSNPs=SNPs2Ref[samples_outliers],alignment_stats[outliers,c("VARIANT","UNALIGNED","LOWCOV")])
colnames(SNPs2Ref_outliers) <- c("coreSNPs","total variants","unaligned bases","low coverage bases")

#SNPs2Ref_outliers <- data.frame(Sample = names(

#SNPs2Ref_outliers <- data.frame(Sample = names(SNPs2Ref[samples_outliers]), SNPs2Ref = SNPs2Ref[samples_outliers])
#colnames(SNPs2Ref_outliers) <- c("Sample","SNPs to reference genome")

knitr::kable(SNPs2Ref_outliers, caption="Outlier samples excluded from analysis" , row.names = T)

#Note that the SNPs2Ref values are after SNP filtering. The number of variants from the alignment stats yields the unfiltered variants.

```

Definition of outiers:
1. if more han `r alignment_threshold * 100`  % of reference bases are  unaligned or low coverage
2. the median distance of a sample to all samples is larger `r outlier_threshold`


### Reference info
The reference was `r if(autoref){"detected automatically"} else {"manually chosen"} `.

```{r more_reference_info, echo = FALSE}
# TODO
# get information about size and number of contigs of reference
# complete, draft assembly

knitr::kable(coregenome_summary,caption = "core genome analysis summary", row.names = F)

```

### Variants to reference


```{r refererence.snps, echo = FALSE}

#knitr::kable(t(SNPs2Ref), caption = "No of SNPs to reference genome") # only works when reference is present

#hist(SNPs2Ref, breaks = seq(0,max(SNPs2Ref)*2, by = 100), xlim = c(0,1000), xlab = "SNPs to reference genome", main = "")

#Variants2Ref

p_hist <- plot_ly(x = SNPs2Ref[Variants2Ref <= 1000], type = "histogram") %>%
  layout(xaxis = list(title="SNPs to reference genome", range = c(0,1000)))

# filtered SNPs
#p_hist <- plot_ly(x = SNPs2Ref[SNPs2Ref <= 1000], type = "histogram") %>%
#  layout(xaxis = list(title="SNPs to reference genome", range = c(0,1000)))

p_hist

```


## Quality assessment

### Alignment stats


```{r alignment_Stats}

#datatable(alignment_stats, caption = "Overview of alignment stats", filter = 'top',rownames= FALSE, escape = FALSE)

DT::datatable(alignment_stats, caption = "Overview of alignment stats", filter = 'top',rownames= FALSE, escape = FALSE, 
          extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns=1)),
    options = list(
                dom = 'BRrltpi',
                autoWidth=FALSE,
                scrollX = TRUE,
                order = list(list(2, 'asc')),
                fixedColumns = TRUE,
                lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                ColReorder = TRUE,
                buttons =
                  list(
                    'copy',
                    'print',
                    list(
                      extend = 'collection',
                      buttons = c('csv', 'excel', 'pdf'),
                      text = 'Download'
                    ),
                    I('colvis')
                  )
              ))



# fix column names

```

* ID: Sample name
* LENGTH: Reference length
* ALIGNED: Aligned bases
* UNALIGNED: Unaligned bases
* VARIANT: Number of detected variants
* HET: Variants with heterozygous or poor quality genotype
* MASKED: Masked variants
* LOWCOV: Reference sites covered with low coverage


### Core genome analysis

```{r core_genonme, echo=FALSE}

knitr::kable(coregenome_stats,caption = "core genome analysis stats", row.names = F)





```

## Table of variants
Original vcf file is here `r core.vcf_file`

```{r variant_table, echo=FALSE}


vcf_clean <- vcf %>% rename(contig = `#CHROM`) %>%
  select(-c(ID,QUAL,FILTER,INFO,FORMAT) )

# convert all entries to factors
vcf_clean <- vcf_clean %>%
  dplyr::mutate_if(sapply(vcf_clean, is.character), as.factor)  %>% 
  dplyr::mutate_if(sapply(vcf_clean, is.integer), as.factor) %>%
  mutate(POS = as.integer(as.character(POS)) )


# rotate column names by 90 degrees (NOT working yet):need to adatp CCS style?
#names(df) <- sprintf('<div style="transform:rotate(-90deg);margin-top:30px;">%s</div>', names(df))
#names(vcf_clean) <- sprintf('<div style="transform:rotate(-90deg);margin-top:30px;">%s</div>', names(vcf_clean))

#%>% as_tibble()



         
if(nrow(vcf) > 3000 ){
  print("Too many variants. Not showing vcf table for report performance reasons.")
} else{
  


    DT::datatable(vcf_clean, caption = "Table of variants", filter = 'top',rownames= FALSE, escape = FALSE, 
          extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns=1)),
    options = list(
                dom = 'BRrltpi',
                #autoWidth=FALSE,
                scrollX = TRUE,
                #fixedColumns = TRUE,
                lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                #ColReorder = TRUE,
                buttons =
                  list(
                    'copy',
                    'print',
                    list(
                      extend = 'collection',
                      buttons = c('csv', 'excel', 'pdf'),
                      text = 'Download'
                    ),
                    I('colvis')
                  )
              ))

} 

```

## Recombination analysis
Gubbins detects recombination sites and filters SNP core alignments.

You chose `r if(do_gubbins != "True"){"not"}` to filter the SNPs for recombinant sites.




```{r gubbins, echo=FALSE}

print("Gubbins")
print(do_gubbins)
print(gubbins_stats_file)

if(do_gubbins == TRUE){
    rownames(gubbins_stats) <- gubbins_stats$Node
    gubbins_stats_selected <- gubbins_stats[grep("internal",gubbins_stats$Node, invert = T),]
    colnames(gubbins_stats_selected)[1] <- "Sample"
    cols2remove <- which(colnames(gubbins_stats_selected) %in% c("r.m","rho.theta"))
    gubbins_stats_selected <- gubbins_stats_selected[,-cols2remove]
    colnames(gubbins_stats_selected) <- gsub("[.]"," ",colnames(gubbins_stats_selected))
  
    # datatable(gubbins_stats_selected, caption = "Summary of gubbins recombination filtering", filter = 'top',rownames= FALSE, escape = FALSE)
    
    DT::datatable(gubbins_stats_selected, caption = "Summary of gubbins recombination filtering", filter = 'top',rownames= FALSE, escape = FALSE, 
          extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns=1)),
    options = list(
                dom = 'BRrltpi',
                #autoWidth=FALSE,
                scrollX = TRUE,
                #fixedColumns = TRUE,
                lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                #ColReorder = TRUE,
                buttons =
                  list(
                    'copy',
                    'print',
                    list(
                      extend = 'collection',
                      buttons = c('csv', 'excel', 'pdf'),
                      text = 'Download'
                    ),
                    I('colvis')
                  )
              ))


    
}


```

## SNP distance table

```{r distance_table, echo=FALSE}
snp_matrix_noself.m <- snp_matrix.m[snp_matrix.m$Sample1 != snp_matrix.m$Sample2,]

#datatable(snp_matrix_noself.m, filter = 'top',rownames= FALSE, escape = FALSE)


DT::datatable(snp_matrix_noself.m, filter = 'top',rownames= FALSE, escape = FALSE, 
          extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns=1)),
    options = list(
                dom = 'BRrltpi',
                #autoWidth=FALSE,
                scrollX = TRUE,
                order = list(list(2, 'asc')),
                #fixedColumns = TRUE,
                lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                #ColReorder = TRUE,
                buttons =
                  list(
                    'copy',
                    'print',
                    list(
                      extend = 'collection',
                      buttons = c('csv', 'excel', 'pdf'),
                      text = 'Download'
                    ),
                    I('colvis')
                  )
              ))

```


## SNP distance matrix

Pair-wise SNP distances

```{r distance_matrix, echo=FALSE}

#label_color <- ifelse(meta_selected$Source[match(colnames(matrix_reduced),meta_selected$ID)] == "human", "blue", "black")
label_color <- "black"

# dynamic size
if (nrow(snp_matrix) > 30){
  param_size = 1.5
  param_legendsize = 6
} else {
  param_size = 3
  param_legendsize = 12
}


p1b <- plot_distance_matrix(snp_matrix, param_size = param_size, param_legendsize = param_legendsize, label_color = label_color)
p1b

#ggsave(distance_matrix_pdf)

#plotly::ggplotly(p1)

```

### Interactive visualization

The figure can be zommed, exported and on hover displays the distance value (z)

```{r distance_matrix_interactive, echo=FALSE}

a <- list(
  title = "Sample",
  showticklabels = TRUE,
  tickangle = 45
)

p2 <- plot_ly(x=snp_matrix.m$Sample1,y=snp_matrix.m$Sample2,z=snp_matrix.m$Distance, type = "heatmap") %>%
layout(xaxis = a, yaxis = a, showlegend = TRUE)

p2


```

## Clustering

```{r clustering, echo=FALSE}

clust <- snp_matrix %>% as.dist %>% hclust(., method = clustering_method)
dendro <- clust %>% as.dendrogram
phylo <- clust %>% as.phylo

# subclusters
subcluster_counts <- lapply(cut_threshold_range, function(cut_threshold) cut(dendro, h = cut_threshold)) %>% sapply(., function(x) length(x$lower))
names(subcluster_counts) <- cut_threshold_range

# initialize snp_address_matrix
snp_address_matrix <- matrix(data = NA, nrow = nrow(snp_matrix), ncol = length(cut_threshold_range), dimnames = list( rownames(snp_matrix),rev(cut_threshold_range)))


# compute cluster membership for all thresholds
for(cut_threshold in cut_threshold_range) {
  dendro_cut <- cut(dendro, h = cut_threshold)
  for (subcluster in 1:length(dendro_cut$lower)){
    snp_address_matrix[dendro_cut$lower[[subcluster]] %>% get_nodes_attr("label") %>% .[!is.na(.)],as.character(cut_threshold)] <- subcluster
  }
}

# re-order
snp_address_matrix <- snp_address_matrix[,as.character(sort(as.numeric(colnames(snp_address_matrix)),decreasing = T))]


# convert to single address
snp_address <- apply(snp_address_matrix,1,function(x) paste(x, collapse = "."))


# table of cluster membership
snp_address_matrix_augmented <- as.data.frame(snp_address_matrix)
colnames(snp_address_matrix_augmented) <- paste("threshold",colnames(snp_address_matrix_augmented),sep = "_")
snp_address_matrix_augmented$Sample <- rownames(snp_address_matrix)

snp_address_matrix_augmented[] <- lapply( snp_address_matrix_augmented, factor) 

snp_address_matrix_augmented <- snp_address_matrix_augmented[,c(ncol(snp_address_matrix_augmented),1:(ncol(snp_address_matrix_augmented)-1))]

```

The SNP distance matrix was hierarchically clustered using average linkage clustering and the subsequent clustering was divided into different sub-clustered at the chosen thresholds `r paste(cut_threshold_range,collape=",") `. At each threshold, samples are assigned to a cluster - resulting in a cluster member number. Searching the following table provides information which samples cluster together at a choses SNP threshold.

The combined cluster member numbers at all chosen threshold correspond to the SNP address.

Note that the cluster member numbers (SNP address) is subject to change every time the clustering is run again!

The entire tree as well as the individual trees can be exported and visualized - together with relevant metadata - in interactive tools such as grapetree, phandango etc. The files were exported to the directory `r exportpath`


```{r visualize_clustering}

#datatable(snp_address_matrix_augmented, caption = "Cluster membership", filter = 'top',rownames= FALSE, escape = FALSE)

DT::datatable(snp_address_matrix_augmented, caption = "Cluster membership", filter = 'top',rownames= FALSE, escape = FALSE, 
          extensions = list("ColReorder" = NULL, "Buttons" = NULL, "FixedColumns" = list(leftColumns=1)),
    options = list(
                dom = 'BRrltpi',
                #autoWidth=FALSE,
                scrollX = TRUE,
                #fixedColumns = TRUE,
                lengthMenu = list(c(10, 50, -1), c('10', '50', 'All')),
                #ColReorder = TRUE,
                buttons =
                  list(
                    'copy',
                    'print',
                    list(
                      extend = 'collection',
                      buttons = c('csv', 'excel', 'pdf'),
                      text = 'Download'
                    ),
                    I('colvis')
                  )
              ))


knitr::kable(data.frame(cbind("SNP threshold" = names(subcluster_counts), "Cluster count" = subcluster_counts)),row.names = F)

#plot(dendro, horiz = T, xlab = "Clustering threshold", ylab = "Samples", main = "Tree resulting from clustering analysis")
#abline(v= cut_threshold_range, col = rainbow(length(cut_threshold_range)))



```




### Details of clustering analysis using a SNP threshold of `r snp_threshold`

In the following, the clustering analysis is visualized as a `r paste0(clustering_method,"-linkage tree")`. At a cluster threshold of `r snp_threshold`, the tree is divided int sub-clusters (cluster types), indicated by the coloring of the branches.



```{r cluster_viz_details}

#color_value <- subcluster_counts[as.character(snp_threshold)]
color_value <- 3


if (length(phylo$tip.label) > 500) {
    my_cex_tiplabel <- 0.1
    my_cex_edgelabel <- 0.1
  } else if (length(phylo$tip.label) > 150) {
    my_cex_tiplabel <- 0.3
    my_cex_edgelabel <- 0.3
      } else if (length(phylo$tip.label) > 50) {
    my_cex_tiplabel <- 0.5
    my_cex_edgelabel <- 0.5
    } else{
    my_cex_tiplabel <- 0.8
    my_cex_edgelabel <- 0.8
  }



dendro %>% set("labels_cex", my_cex_tiplabel) %>% set("labels_col", value = rainbow(color_value), h=snp_threshold) %>% set("branches_k_color", value = rainbow(color_value), h = snp_threshold) %>% 
  plot(main = paste("Tree resulting from clustering analysis"), sub = paste("Colored by using SNP threshold of",snp_threshold), horiz = T, xlab = "Clustering threshold", ylab = "Samples")
abline(v= snp_threshold, col = 8 , lwd = 3)


```


### `r paste0(clustering_method,"-linkage tree")`

Shown here is the `r paste0(clustering_method,"-linkage tree")` with the branch length defined by the snp distance.

```{r cluster_viz_overview_2}

  edge_color <- ifelse(phylo$edge.length<1,"white","red") 
  tip_color <- "black"
  plot(phylo, main = paste0(clustering_method,"-linkage tree"), cex=my_cex_tiplabel, tip.color = tip_color)
  edgelabels(round(phylo$edge.length, 0), srt = 0, bg = "white", adj = c(0.5,1), col = edge_color, cex = my_cex_edgelabel, frame = "none")


```


### Cluster details

Shown here are the tree structures of the `r paste0(clustering_method,"-linkage tree")` for every sub-cluster defined by a snp distance of `r snp_threshold`. Clusters containg only a single sample are excluded.

Note that the cluster numbers can be looked up in the _cluster membership_ table above - in the field `r paste0("threshold_",snp_threshold)`. Furthermore, the colored tree above visualizes every cluster with a different number - starting with cluster 1 at the bottom.


```{r cluster_viz_details3}

dendro_cut <- cut(dendro, h = snp_threshold)
clusters_notplotted <- c()

par(mar=c(2,2,4.1,6.1)) # more space on the right margin

for (subtree in 1:length(dendro_cut$lower)){  
  dend <- dendro_cut$lower[[subtree]]
  
  cluster_size <- dend %>% get_nodes_attr("label") %>% (function(x) x[!is.na(x)]) %>% length
  if(cluster_size > 1){
    plot(dend, horiz = T, main = paste("Subtree for cluster",subtree))
  } else {
    clusters_notplotted <- c(clusters_notplotted,subtree)
  }
}

# unplotted clusters


```


The following clusters - only containing a single sample - were not plotted: `r paste(clusters_notplotted, collapse = ",")`

```{r export_clustering}

if(export){

  
  dir.create(exportpath, showWarnings = F)


  
# export clustering
  ape::write.tree(as.phylo(dendro), file = file.path(exportpath,"clustering_global.tre"))    

# export cluster memberships
write.table(snp_address_matrix_augmented, file = file.path(exportpath, "snp_addresses.tsv"), sep = "\t", col.names = sub("Sample","ID",colnames(snp_address_matrix_augmented)), row.names = F, quote = F)

# export subtrees

par(mar=c(2,2,4.1,6.1))

for (subtree in 1:length(dendro_cut$lower)){  
  dend <- dendro_cut$lower[[subtree]]
  treefile_sub <- file.path(exportpath,paste0("clustering_subtree_",subtree,".tre"))
  cluster_size <- dend %>% get_nodes_attr("label") %>% (function(x) x[!is.na(x)]) %>% length
  if(cluster_size > 1){
    #phylogram::write.dendrogram(dend, file = treefile_sub, append = FALSE, edges = TRUE)
    #ape::== TRUE.tree(dend.phylo, file = treefile_sub) # not working here

  } 
}


}

```


## Phylogenetic tree

The following phylogenetic tree is based on the method: `r print(phylo_type)`.
It provides a more thorough phylogentic relationship than a clustering based on the distances alone.

Note that you can visualize the tree more interactively and combine it with metadata using tools such as grapetree, iTOL and phandango. 

The branch length is determined by the nucleotide substitutions per site – that is the number of substitutions divided by the length of the sequence.

```{r trees, echo=FALSE}

if(plot_phylo){

  print("Version 1")

  tip_color <- "black"
  edge_color <- "red" #ifelse(nwk$edge.length<1,"white","red") 
  
  if (length(nwk$tip.label) > 500) {
    my_cex_tiplabel <- 0.1
    my_cex_edgelabel <- 0.1
    } else if (length(nwk$tip.label) > 50) {
    my_cex_tiplabel <- 0.5
    my_cex_edgelabel <- 0.4
    } else{
    my_cex_tiplabel <- 0.8
    my_cex_edgelabel <- 0.7
  }
  
  plot(nwk, main = paste(phylo_type), cex=my_cex_tiplabel, tip.color = tip_color, align.tip.label = F)
  edgelabels(round(nwk$edge.length,6), srt = 0, bg = "white", adj = c(0.5,1), col = edge_color, cex = my_cex_edgelabel, frame = "none")

  #print("Version 2")
  
  if (length(nwk$tip.label) > 200) {
    plot_label_size <- 1  
  } else if (length(nwk$tip.label) > 50) {
    plot_label_size <- 2  
  } else {
    plot_label_size <- 6
  }
  
  #ggtree(nwk) + geom_tiplab(size=plot_label_size) + theme_tree2() + labs(caption="nucleotide substitutions per site")
  
}


```



```{r phylocanvas_radial}
# do.phylocanvas <- F
# ### phylocanvas_radial
# if(do.phylocanvas){
# 
# phylocanvas(nwk, textsize = 10, nodesize = 2, treetype = "radial",showscalebar = T ) 
# }
# 
# 
# ### phylocanvas_rectangular
# 
# if(do.phylocanvas){
# phylocanvas(nwk, width = 3000, textsize = 5, nodesize = 2, treetype = "rectangular",showscalebar = T ) 
# }
# 
# 
# ### phylocanvas_circular
# if(do.phylocanvas){
# phylocanvas(nwk, width = 3000, textsize = 10, nodesize = 2, treetype = "circular",showscalebar = T ) 
# }
```

## Parameters

```{r print-config}
str(object = configfile,
    give.attr = FALSE,
    give.head = FALSE,
    comp.str = "",
    indent.str = "  ",
    no.list = TRUE)
```


## Links to files

All data is available at `r workdir`.

The exported clustering files are in `r exportpath`

```{r file_info}



file_timestamps <- data.frame(
  name=c(
"SNP distance matrix",
"Variant-calling file",
"Alignment stats",
"Phylogenetic tree"
  ),
  file=c(
snp_matrix_file,
core.vcf_file,
core.txt_file,
nwk_file
)
)

file_timestamps$Link <- paste0("<a href=",file_timestamps$file,">",file_timestamps$name,"</a>")

knitr::kable(file_timestamps, caption = "Overview of files used for this report and their modification date")
  

#exportpath
 if(export){
   file_timestamps_2 <- data.frame(
  name=list.files(exportpath, full.names = F),
  file= list.files(exportpath, full.names = T))

   file_timestamps_2$Link <- paste0("<a href=",file_timestamps_2$file,">",file_timestamps_2$name,"</a>")

   
      knitr::kable(file_timestamps_2, caption = "Overview of files related to clustering")
 }
  


```
