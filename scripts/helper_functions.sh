#!/bin/bash

## [Goal] Helper Functions for Bash Scripts of https://gitlab.bfr.bund.de/4nsz/scripts.git
## [Author] Holger Brendebach
## [Version] 1.0.15

## Notification Trap --------------------------------------------------

error_handler() {
  ## remove trap that was set in the sourcing parent script
  trap - SIGINT SIGTERM EXIT ERR
  ## message failure
  ## NOTE: notification hierarchy: .bash-env and crontab define "EMAIL" as NGSadmin@bfr.bund.de, accordingly,
  ## NOTE: additional recipients of ERROR notifications should be added as an email2 string;
  ## NOTE: lower-case "email" is used in config files for SUCCESS notifications
  [[ ${dryrun:-"false"} == false && -n ${EMAIL:-${email:-}} ]] && cat << MSG | mail -s "[${script_name:-}] Script Error${email_subject_suffix:-}" "${EMAIL:-${email:-NGSadmin@bfr.bund.de}}" "${EMAIL2:-${email2:-}}"
Date:       $(date +"%F %T")
Script:     ${script_real:-}
Arguments:  ${*:-${args:-"No arguments provided"}}
User/Host:  $(whoami)@$(hostname)
Script ID:  ${child_script_id:-${script_id:-}}
Parent ID:  ${parent_script_id:-}
Error:      A SIGINT/SIGTERM/ERR signal was caught.

Please review the logs of the script execution:
* instance: ${logfile:-}
* global:   ${logfile_global:-}

Logfile tail:
>>>>>>>>>>
$([[ -f ${logfile-} ]] && tail -n5 "$logfile")
<<<<<<<<<<
MSG
  exit 1
}

## Logging --------------------------------------------------

# Resource: messaging template from https://betterdev.blog/minimal-safe-bash-script-template/

## set script_id for this instance

if [ -n "${script_id-}" ]; then
  #  if [[ $script_id =~ ^[0-9a-fA-F]{8}$ ]]; then
  parent_script_id=${script_id-}
  child_script_id=$(date +%s%N | md5sum | cut -c -8)
  script_id="${parent_script_id}][${child_script_id}"
#  fi
else
  script_id=$(date +%s%N | md5sum | cut -c -8)
fi
declare -x script_id

## set logging variables logfile and logfile_global

if [ -d "/tmp" ] && [ -w "/tmp" ]; then # create the log directory in /tmp and file or set it to /dev/null
  logfile="/tmp/${USER}_logs/${child_script_id:-$script_id}.log" # temporary logfile for detailed, parameter-specific logging (may be set in the sourcing script)
  [[ -d "$(dirname "$logfile")" ]] || ( mkdir "$(dirname "$logfile")" 2> /dev/null && chmod 700 "$(dirname "$logfile")" 2> /dev/null ) # create the log directory and set permissions silently
  touch "$logfile" 2> /dev/null || logfile="/dev/null"
else
  logfile="/dev/null"
fi
logfile_global="/dev/null" # logfile for script execution logging (may be set in the sourcing script)

## Check if inside a cronjob ----------------------------------

incron=false
if [ -x "$(command -v "pstree")" ]; then
  incron=$(pstree -s $$ | grep -q cron && echo true || echo false)
fi
#declare -x incron

## An Echo replacement for STDOUT and STDERR ----------------------------------

msg() {
  # this function is meant to be used to print everything that is NOT a script output
  echo >&2 -e "${1-}"
}

## Logging to either Run or Global Logfile ----------------------------------

logglobal() {
  printf -v logdate '%(%F %T)T' -1
  if [ $# -gt 0 ]; then
    echo "[$logdate][$script_id] $*" | tee -a "$logfile" "$logfile_global"
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      echo "[$logdate][$script_id] $line" | tee -a "$logfile" "$logfile_global"
    done
  else
    die "No input provided"
  fi
  return 0
}

logecho() {
  printf -v logdate '%(%F %T)T' -1
  echo "[$logdate][$script_id] $*" | tee -a "$logfile"
}

logpipe() {
  # use this if you want to log the output of a pipe
  if [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      logecho "$line"
    done
  elif [ $# -gt 0 ]; then
    logecho "$*"
  else
    die "No input provided"
  fi
  return 0
}

## Logging with Colored Status ----------------------------------

logsuccess() {
  if [ $# -gt 0 ]; then
    [ "${dryrun:-false}" != true ] && logecho "${green}[SUCCESS]${normal} $*"
    [ "${dryrun:-false}" == true ] && logecho "${blue}[DRYRUN]${normal} $*"
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      [ "${dryrun:-false}" != true ] && logecho "${green}[SUCCESS]${normal} $line"
      [ "${dryrun:-false}" == true ] && logecho "${blue}[DRYRUN]${normal} $line"
    done
  else
    die "No input provided"
  fi
  return 0
}

logwarn() {
  if [ $# -gt 0 ]; then
    logecho "${yellow}[WARNING]${normal} $*"
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      logecho "${yellow}[WARNING]${normal} $line"
    done
  else
    die "No input provided"
  fi
  return 0
}

logerror() {
  if [ $# -gt 0 ]; then
    logecho "${red}[ERROR]${normal} $*"
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      logecho "${red}[ERROR]${normal} $line"
    done
  else
    die "No input provided"
  fi
  return 0
}

logstderr() {
  # use this if you want to explicitly log the STDERR of a command, used in logexec() when commands are scripts
  if [ $# -gt 0 ]; then
    logecho "${blue}[STDERR]${normal} $*"
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      logecho "${blue}[STDERR]${normal} $line"
    done
  else
    die "No input provided"
  fi
  return 0
}

die() {
  local message="${1-}"
  local exit_status=${2-1} # default exit status 1
  [ "${dryrun:-false}" == true ] || logglobal "${red}[DIE]${normal} $message"
  [ "${dryrun:-false}" == true ] && logecho "${blue}[DRYRUN]${normal} Skipping termination cause: $message" && return 0 # default is termination except with dryrun true
  sleep 1                                                                                                         # give the logecho time to be written to the logfile
  [ "$exit_status" -eq 1 ] && kill -s TERM $$                                                                       # send TERM signal to the script to trigger the trap
  exit "$exit_status"
}

## A Section Header in the Logfiles ----------------------------------

logheader() {
  local message="${1-}"
  echo
  echo "${cyan}$message${normal}"
  echo "${cyan}$message${normal}" | decolorize | tee -a "$logfile" > /dev/null # no header in logfile_global
}

## Run Commands and Log them ----------------------------------

logexec() {
  # NOTE: call function payload in "" if several commands are concatenated with && or diverting STDOUT/STDERR
  local message="$*"
  logecho "$message"
  [[ ${dryrun:-true} == true ]] && return 0 # default is dryrun with set $dryrun
  if grep --color=never -q -P "\.py" <<< "$message"; then
    # use no logging at all if the command is a python script, i.e. a snakemake wrapper
    eval "$message"
    local exit_status="$?"
  elif grep --color=never -q -P "\.sh" <<< "$message"; then
    # use STDERR logging if the command is a shell script with its own logging, to avoid double logging
    eval "$message" 2> >(logstderr)
    local exit_status="$?"
  else
    # use full logging if the command is not a script
    eval "$message" |& tee -a "$logfile"
    local exit_status="$?"
  fi
  sleep 0.2                  # give the logecho time to be written to the logfile
  return "${exit_status:-0}" # return the exit status of the eval command, b/c sleep will always return 0
}

run_command_array() {
  # use this when your command has newlines, e.g. from a heredoc
  local single_line_cmd cmd_array exit_status
  mapfile -t cmd_array < <(echo "$@")                                        # convert multiword string to array
  single_line_cmd=$(printf "%s" "${cmd_array[*]}" | tr '\n' ' ' | tr -s ' ') # convert array to single line
  logecho "$single_line_cmd"
  [[ ${norun:-false} == true ]] && return 0 # default is execution with unset $norun, e.g. used in aquamis_wrapper.sh
  if grep --color=never -q -P "\.py" <<< "$single_line_cmd"; then
    # use no logging at all if the command is a python script, i.e. a snakemake wrapper
    eval "$single_line_cmd"
    exit_status="$?"
  elif grep --color=never -q -P "\.sh" <<< "$single_line_cmd"; then
    # use STDERR logging if the command is a shell script with its own logging, to avoid double logging
    eval "$single_line_cmd" 2> >(logstderr)
    exit_status="$?"
  else
    # use full logging if the command is not a script
    eval "$single_line_cmd" |& tee -a "$logfile"
    exit_status="$?"
  fi
  sleep 0.2                  # give the logecho time to be written to the logfile
  return "${exit_status:-0}" # return the exit status of the eval command, b/c sleep will always return 0
}

## ANSI colors --------------------------------------------------

colorize() {
  ## Fancy echo
  bold=$(tput bold)
  normal=$(tput sgr0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  grey=$(tput setaf 7)
  inverted=$(tput rev)
}

# load colors
colorize

# remove preexisting decolorize alias
if alias "decolorize" &>/dev/null; then
  unalias decolorize
fi

decolorize() {
  if [ $# -gt 0 ]; then
    echo "$*" | sed -r 's,\x1B[[(][0-9;]*[a-zA-Z],,g'
  elif [ -p /dev/stdin ]; then
    while IFS= read -r line; do
      echo "$line" | sed -r 's,\x1B[[(][0-9;]*[a-zA-Z],,g'
    done
  fi
}

## miscellaneous script functions  --------------------------------------------------

dev_notes() {
  cat <<- DEV

${red}Development Notes:${normal}

${bold}Goal:${normal}
$(grep --color=never -ioP "(?<=\[Goal\] ).*$" "$1")

${bold}Rationales:${normal}
$(grep --color=never -ionP "(?<=\[Rationale\]).*$" "$1")

${bold}ToDos:${normal}
$(grep --color=never -ionP "(?<=# TODO:).*$" "$1")

${bold}Notes:${normal}
$(grep --color=never -ionP "(?<=# NOTE:).*$" "$1")

DEV
  exit
}

interactive_query() {
  local message=${1:-"Do you want to continue?"}
  read -p "$message [Yy] " -n 1 -r
  echo
  [[ ! $REPLY =~ ^[Yy]$ ]] && exit 1
  echo
}

progress_bar() {
  printf -v logdate '%(%F %T)T' -1
  local current total msg width progress completed left fill empty
  current=$1
  total=$2
  msg=${3:-}
  width=40 # Width of the progress bar
  progress=$(((current * 100) / total))
  completed=$(((progress * width) / 100))
  left=$((width - completed))
  fill=$(printf "%${completed}s")
  empty=$(printf "%${left}s")
  [ -z "$msg" ] && printf "\r[$logdate] Progress: [${fill// /#}${empty// /-}] ${progress}%% ($current of $total) $msg"
  [ -n "$msg" ] && logecho "[${fill// /#}${empty// /-}] ${progress}% ($current of $total) $msg"
  return 0
}

## transform tables --------------------------------------------------

body() {
  IFS= read -r header
  printf '%s\n' "$header"
  "$@"
}

## Conda --------------------------------------------------

check_conda() {
  ## [Rationale] If shell variable CONDA_EXE is set or PATH points to a conda binary, check its version and config/info
  conda_env_name=$(basename "${1:-}")
  script_path=${script_path-} # initialize variable if it is not set

  logheader "Conda Environment:"
  printf "Checking CONDA installation... "
  conda_bin=${CONDA_EXE:-$(which conda)}
  [[ -x "$(command -v "${conda_bin:-conda}")" ]] || (echo "${red}Not Detected${normal}" && return 1)
  [[ -x "$(command -v "${conda_bin:-conda}")" ]] && echo "${green}Detected${normal}" && conda_status=true
  conda_info=$($conda_bin info --json)
  conda_version=$(grep --color=never -Po '(?<="conda_version": ).*$' <<< "$conda_info" | tr -d '",\0')
  conda_base=$(grep --color=never -Po '(?<="root_prefix": ).*$' <<< "$conda_info" | tr -d '",\0')
  conda_env_active=$(grep --color=never -Po '(?<="active_prefix_name": ).*$' <<< "$conda_info" | tr -d '",\0')
  conda_envs=$(grep --color=never -Pzo '(?s)(?<="envs": \[\n).*?(?=])' <<< "$conda_info" | tr -d '",\0' | sed 's/^[[:space:]]*//')
  conda_env_path=$(grep --color=never -e "\/${conda_env_name}$" <<< "$conda_envs") # multiple envs with the same name may exist, e.g. from other users

  printf "Checking MAMBA installation... "
  mamba_bin=${MAMBA_EXE:-$(which mamba)}
  [[ -x "$(command -v "${mamba_bin:-mamba}")" ]] || (echo "${red}Not Detected${normal}" && mamba_status=false)
  [[ -x "$(command -v "${mamba_bin:-mamba}")" ]] && echo "${green}Detected${normal}" && mamba_status=true
  [[ ${mamba_status:-} == true ]] && conda_version=$($mamba_bin --version)

  printf "Checking APK source........... "
  [[ ${script_path##"$conda_base"} != "$script_path" ]] && bioconda_status=true
  [[ ${bioconda_status:-false} == false ]] && echo "${bold}GitLab${normal}"
  [[ ${bioconda_status:-false} == true ]] && echo "${bold}BioConda${normal}"

  logheader "Conda Parameters:"
  echo "
check_conda_arg:|${1:-}
conda_env_name:|${conda_env_name}
conda_base:|${conda_base/$'\n'/' ; '}
conda_env_path:|${conda_env_path/$'\n'/' ; '}
conda_env_active:|$conda_env_active
conda_shell_level:|${CONDA_SHLVL-}
conda_version:|${conda_version/$'\n'/' ; '}
" | column -t -s "|" | tee -a "$logfile"
  echo

  [[ ${CONDA_BUILD_STATE:-} == TEST ]] && return 0
  [[ -z ${conda_env_path-} && ! ${script_name:-} =~ _setup.sh$ ]] && die "The conda environment name was not found in the list of environments."

  if (($(grep --color=never -c . <<< "$conda_env_path") > 1)); then
    NL=$'\n'
    conda_env_path=${conda_env_path%%"$NL"*}
    logwarn "Multiple conda environments with the same name were found in different conda bases. Using the first one: $conda_env_path"
  fi
  return 0
}

source_conda() {
  ## [Rationale] If $PATH or various default installation targets contain a conda binary, query conda for its base directory and source the conda initialisation to allow conda() activate
  conda_bin=${CONDA_EXE:-$(which conda)}
  [[ -x "$(command -v "${conda_bin:-conda}")" ]] && conda_info=$($conda_bin info --json)
  [[ -z ${conda_info-} ]] && conda_info=$("$HOME"/miniconda3/bin/conda info --json)
  [[ -z ${conda_info-} ]] && conda_info=$("$HOME"/anaconda3/bin/conda info --json)
  [[ -z ${conda_info-} ]] || conda_base=$(grep --color=never -Po '(?<="root_prefix": ).*$' <<< "$conda_info" | tr -d '",\0')
  [[ -z ${conda_base-} ]] && die "conda binary not found"
  source "$conda_base"/etc/profile.d/conda.sh # NOTE: this does not activate the conda base but provides the conda activate function
}

## Git --------------------------------------------------

require_clean_working_tree() {
  ## [Rationale] Exit if the tree is not clean.

  ## Update the index
  git update-index -q --ignore-submodules --refresh
  local err=0

  ## Disallow unstaged changes in the working tree
  if ! git diff-files --quiet --ignore-submodules --; then
    logwarn "You have unstaged changes."
    git diff-files --name-status -r --ignore-submodules -- >&2
    err=1
  fi

  ## Disallow uncommitted changes in the index
  if ! git diff-index --cached --quiet HEAD --ignore-submodules --; then
    logwarn "Your index contains uncommitted changes."
    git diff-index --cached --name-status -r --ignore-submodules HEAD -- >&2
    err=1
  fi

  if [ $err = 1 ]; then
    die "You need to commit or stash changes before updating subtrees."
  fi
  return 0
}

exit_on_merge_conflicts() {
  ## [Rationale] Exit if there are merge conflicts.
  local conflicts
  git fetch
  conflicts=$(git ls-files -u | wc -l)
  if [ "$conflicts" -gt 0 ]; then
    git merge --abort
    die "There is a merge conflict. Aborting."
  fi
  return 0
}
