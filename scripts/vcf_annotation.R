#!/usr/bin/env Rscript

# [Goal] annotate vcf

message("[START] vcf_annotation.R")

# method 1: from single vcf files
# method 2: from extra annotation: blast, bed, abricate

abricate.file <- "/cephfs/abteilung4/deneke/Projects/snippysnake_dev/vcf_annotation/snp_mini/abricate2ref/cgmlst_loci.abricate.tsv"

abricate <- read.delim(abricate.file, stringsAsFactors = F)

abricate$START
abricate$END
# check for duplicates
if (any(duplicated(abricate$GENE)){
  warning("Duplicate loci found!")
}

# TODO: check for overlaps and clean up

# create granges object
library(GenomicRanges)
# gr=GRanges(seqnames=c("chr1","chr2","chr2"),
#            ranges=IRanges(start=c(50,150,200),
#                           end=c(100,200,300)),
#            strand=c("+","-","-")
# )
abriacte.gr=GRanges(seqnames=abricate$SEQUENCE,
ranges=IRanges(start=abricate$START,
end=abricate$END),
strand=abricate$STRAND,
locus=abricate$GENE
)



# vcf
vcf_file <- "/cephfs/abteilung4/deneke/Projects/snippysnake_dev/vcf_annotation/snp_mini/results/core.vcf"

header2skip <- 5
vcf <- read.delim(vcf_file, stringsAsFactors = F, skip = header2skip, check.names = F)

# remove all lines where all samples have snps
vcf_all1 <- apply(vcf[, -c(1:9)], 1, function(pos) all(pos == 1))
# find all lines where at least one sample has no SNP
vcf_any0 <- apply(vcf[, -c(1:9)], 1, function(pos) any(pos == 0))

vcf_filtered <- vcf[vcf_any0,]

vcf.gr=GRanges(seqnames=vcf$`#CHROM`,
ranges=IRanges(start=vcf$POS,
end=vcf$POS),
)


myintersection <- GenomicRanges::intersect(vcf.gr, abriacte.gr, ignore.strand = T)
myintersection <- GenomicRanges::intersect(abriacte.gr, vcf.gr, ignore.strand = T)

mysetdiff <- GenomicRanges::setdiff(vcf.gr, abriacte.gr, ignore.strand = T)
unlist(mysetdiff)

GenomicRanges::findOverlaps(abriacte.gr, vcf.gr)
myoverlap <- GenomicRanges::findOverlaps(vcf.gr, abriacte.gr, ignore.strand=T)


myoverlap_df <- data.frame(query=queryHits(myoverlap), subject=subjectHits(myoverlap))


vcf_extended <- do.call(rbind, apply(myoverlap_df, 1, function(ol){
data.frame(vcf[ol[1], 1:9],
LOCUS=abricate[ol[2], "GENE"],
vcf[ol[1], 10:ncol(vcf)])
}))

# use contig and pos!
vcf_nolocus <- setdiff(paste(vcf$`#CHROM`, vcf$POS, sep=":"), paste(vcf$`#CHROM`, vcf_extended$POS, sep=":"))
vcf_nolocus <- as.data.frame(t(sapply(vcf_nolocus, function(x){
mysplit <- strsplit(x, ":")[[1]]
})))
colnames(vcf_nolocus) <- c("Chrom", "Pos")
library(dplyr)
vcf_nolocus <- vcf %>% filter(`#CHROM` %in% vcf_nolocus$Chrom & POS %in% vcf_nolocus$Pos)
vcf_nolocus <- data.frame(vcf_nolocus[, 1:9], LOCUS="NO_LOCUS", vcf_nolocus[, 10:ncol (vcf_nolocus)])


samples <- colnames(vcf_extended[-c(1:10)])



vcf_extended <- rbind (vcf_extended, vcf_nolocus)

vcf_extended <- vcf_extended[order(vcf_extended$X.CHROM, vcf_extended$POS),]

# find all lines where at least one sample has no SNP
vcf_any0 <- apply(vcf_extended[, samples], 1, function(pos) any(pos == 0))

# PROBLEM: ONLY INTERSECTING Positions; need also vcf only!

vcf_extended_filtered <- vcf_extended[which(vcf_any0),]
vcf_extended_filtered


# TODO make a check:
# run snippy against cgmlst reference
vcf2.file <- "/cephfs/abteilung4/deneke/Projects/snippysnake_dev/vcf_annotation/snp_mini_vs_scheme/results/core.vcf"

vcf2.temp <- scan(vcf2.file, what = character(), sep="\n")
header2skip <- length(grep("^##", vcf2.temp))


#header2skip <- 5
vcf2 <- read.delim(vcf2.file, stringsAsFactors = F, skip = header2skip, check.names = F)

# remove all lines where all samples have snps
vcf2_all1 <- apply(vcf2[, -c(1:9)], 1, function(pos) all(pos == 1))
# find all lines where at least one sample has no SNP
vcf2_any0 <- apply(vcf2[, -c(1:9)], 1, function(pos) any(pos == 0))

vcf2[which(vcf2_any0),]

vcf2[vcf2$`#CHROM` == "STMMW_41151_1",]



vcf[vcf$POS >= 4251309,]

which(vcf$POS >= 4251309 & vcf$POS <= 4252067)
# Not the same SNPs found! One extra SNP when calling against cgMLST ref!
# OBERVATION: Many SNPs found for that locus
# Maybe not SNP but MNV?

# TODO Look up sample's vcf!