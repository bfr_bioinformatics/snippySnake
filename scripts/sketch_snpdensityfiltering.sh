# options for snp filtering

# per sample or for all samples ? see discussion https://github.com/tseemann/snippy/issues/354
# remove from core.vcf, core.tab 
# OR
# create bedfile for masking?

# for each snp.vcf find dense regions
# create bedfile per sample
# merge bedfile
# call snippy core with bedfile



# commands
bcftools +prune -w 100 -n 2 core.vcf | grep -v '^#' | wc -l


# TODO convert to bedfile?



# suggestion 1: my_ancient_self (see) ----------------------------------------------
# see https://github.com/tseemann/snippy/issues/122

# 1. Call core snps
snippy-core --noref --prefix core_noref */

# 2. thin neigbouting SNPs
vcftools --vcf core_noref.vcf --out thinned --thin 100 --recode

# 3. Convert to tab format
vcf-to-tab < thinned.recode.vcf > thinned.tab

# 4. convert to alignment (from https://code.google.com/archive/p/vcf-tab-to-fasta/)
perl vcf_tab_to_fasta_alignment.pl -i thinned.tab > thinned.aln

# 5. compute distance matrix
snp-dists $analyis_dir/thinned.aln > $analyis_dir/snp_thinned_matrix.tsv

# suggestion 2: cfsan pipeline ----------------------------------------------
# https://snp-pipeline.readthedocs.io/en/latest/usage.html#snp-filtering
# The SNP Pipeline removes abnormal SNPs from the ends of contigs and from regions where many SNPs are found in close proximity. The pipeline runs both ways, with SNP filtering, and without SNP filtering, generating pairs of output files. You can compare the output files to determine which positions were filtered.
# Dense regions found in any sample are filtered from all the other samples by default. In this mode, if you add or remove a sample from your analysis it may affect the final SNPs detected in all other samples. 
# see /home/DenekeC/software/cfsan-snp-pipeline/snppipeline/filter_regions.py
# exclude an entire region which contained ≥3 SNPs in any 1,000 bp

# suggestion 3: snvphyl ----------------------------------------------
# https://snvphyl.readthedocs.io/en/latest/user/parameters/#step-12-consolidate-vcfs
# The window size is another parameter that can be altered to adjust the sensitivity of the SNV density filter used in SNVPhyl. For a region to be flagged as high density, it must contain at least density threshold SNVs across a region equal to the length of the window size. As a result, increasing the window size generally results in stricter filtering; increasing the proportion of the genome being flagged as high density. The density regions are calculated independently for each isolate genome in a given SNVPhyl run.
# /home/DenekeC/software/snvphyl/snvphyl-galaxy/tools/snvphyl-tools/filter_density/filter-density.xml
# conda activate snvphyl-tools

vcf=core.vcf
filtered_bcf=core_filtered.vcf
out=filtered_region.txt
threshold=2
window_size=100

echo "bcftools plugin filter_snv_density $vcf -O v -o $filtered_bcf -- --filename $vcf --region_file $out --window_size $window_size --threshold $threshold"
bcftools plugin filter_snv_density $vcf -O v -o $filtered_bcf -- --filename $vcf --region_file $out --window_size $window_size --threshold $threshold


# make bedfile from filtered_region.txt

# merge bedfile

# use snippy-core with bedfile



# suggestion 4: simple R script to reduce vcf and re-compute distance matrix? ----------------------------------------------
# See Rscripts/SNP_filteringneighbouringsites.R







#TODOs ---------------------------------------------
# add total distance pairwise
# upgrade to latest version


# Annotations not showing in core.tab
# https://github.com/tseemann/snippy/issues/254
# One could use bedtools intersect with your snps.vcf and references.gff to achieve it, and I am looking to do it that way in the future.