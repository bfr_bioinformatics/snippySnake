#!/bin/bash

## [Goal] Run snippySnake in batches; Wrapper for snippySnake.py, direct Snakemake calls or Docker container
## [Author] Holger Brendebach

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name

${bold}Description:${normal}
Run snippySnake in batch mode based on entries in the snippySnake_runs.tsv table.
Any script arguments that do not match the ones in the next section will be appended to the run command, e.g. "--unlock".
This wrapper may evaluate several run strategies in its run_modules() function: snippySnake.py, direct Snakemake calls or Docker container runs.

${bold}Optional Named Arguments:${normal}
  -e, --env_name             Override conda environment name derived from envs/snippysnake.yaml (default: snippysnake)
  -p, --profile              Set a Snakemake profile, e.g. workstation (default=bfr.hpc)
  -c, --from_config {file}   Deactivate batch mode and use path to BakCharak config file to update the run
  -o, --onerun               Deactivate batch mode and use hardcoded single run parameters (default=false)
  -s, --snakemake            Skip the snippySnake.py wrapper script and use a customized Snakemake command; this also skips config_snippysnake.yaml recreation
  --dryrun                   Perform a dryrun and include a Snakemake dryrun for DAG calculation (default=false)
  -n, --norun                Perform a dryrun without Snakemake commands (default=false), implies --auto
  -a, --auto                 Skip interactive confirmation dialogue (default=false), e.g. for tmux-wrapped script calls

${bold}Notes:${normal}
Check the manual_override() function to tweak script arguments for repetitive use.
Check the tweak_settings() function to review potential system-specific expert settings.
If you like script fail notifications by email, set the environment variable "email" to activate the error_handler() function.

${bold}Example:${normal}
bash $script_real -p bfr.hpc -a -n

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname $script_path)
  profile="bfr.hpc"
  conda_recipe="$repo_path/envs/snippysnake.yaml"
  dag_search="$repo_path/profiles/smk_directives.yaml"

  ## default values of switches
  mode="batch"
  snakemake=false
  norun=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -p | --profile)
        profile="$2"
        shift 2
        ;;
      -c | --from_config)
        mode="config"
        configfile="$2"
        shift 2
        ;;
      -o | --onerun)
        mode="single"
        shift
        ;;
      -s | --snakemake)
        snakemake=true
        shift
        ;;
      -n | --norun)
        norun=true
        dryrun=true
        interactive=false
        user_args+="--dryrun "
        shift
        ;;
      --dryrun)
        dryrun=true
        user_args+="--dryrun "
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -?*)
        user_args+="${1-} "
        shift
        ;;
      *) break ;;
    esac
  done

  ## checks
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
[[ $(declare -F error_handler) == error_handler ]] && [[ -n ${email+x} ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM # ERR # TODO: read HEREDOC causes an unknown error

## Define Paths --------------------------------------------------

manual_override() {
  ## (1) Hardcoded Parameters that will override defaults, comment-in to activate

  ## Conda Environment
#  conda_recipe="snippysnake_dev.yaml"

  ## Code Base
#  repo_path="/cephfs/abteilung4/NGS/Pipelines/snippySnake"  # tagged version: Accreditation

  ## Snakemake Profiles (see config.yaml within)
#  profile="nris.saga.slurm.dev"
#  profile="nris.saga.slurm.normal"
#  profile="bfr.hpc"
#  profile="bfr.hpc.greedy"
#  profile="workstation"

  ## Resources
  [[ "$profile" == "bfr.hpc.greedy"         ]] && threads_sample=6
  [[ "$profile" == "bfr.hpc"                ]] && threads_sample=4
  [[ "$profile" == "nris.saga.slurm.normal" ]] && threads_sample=2
  [[ "$profile" == "nris.saga.slurm.dev"    ]] && threads_sample=2
  [[ "$profile" == "workstation"            ]] && threads_sample=1

  ## DAG Job Prioritization
#  dag_search=$repo_path/profiles/smk_directives_breadthfirst_groups.yaml

  true
}

define_paths() {
  ## [Rationale] Manual Run Parameters Definition

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Resources
  profile_base="$repo_path/profiles"
  profile="$profile_base/$profile"

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  ## Run variables; if in mode "batch" {run_name,dir_samples,dir_data} will be read from run_table
  run_table="$script_path/snippySnake_runs.tsv"
}

set_run_paths() {
  [[ -d $dir_data                  ]] || logexec mkdir -p "$dir_data"
  logecho "cd $dir_data"
  cd $dir_data || die "Cannot change to working directory: $dir_data"

  [[ -f "$dir_samples/samples.tsv" ]] && sample_list="$dir_samples/samples.tsv"
  [[ -z "${configfile:-}"          ]] && configfile="$dir_data/config_snippysnake.yaml"
  [[ -z "${run_name:-}"            ]] && run_name="test_data_$(date +%F)_$(hostname)_$mode"

  # checks
  [[ ! -d "$dir_samples" ]] && die "The sample directory does not exist: $dir_samples"
  [[ ! -f "$sample_list" ]] && die "The sample list does not exist: $sample_list"
  [[ ! -f "$reference"   ]] && die "The reference genome does not exist: $reference"
  true
}

## Info --------------------------------------------------

show_info() {
  logheader "General Settings:"
  echo "
Mode:|$mode
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Threads per Sample:|$threads_sample
Profile:|$profile
DAG Priority:|$dag_search
Snakemake User Args:|${user_args:-}
Logfile:|$logfile
" | column -t -s="|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  true
}

show_info_on_run() {
  ## print current run infos
  cat <<- TXT | tee -a $logfile $logfile_global

${yellow}#################################################################### Processing:${normal}

Run:           ${inverted}$run_name${normal}
SampleList:    ${bold}${sample_list:-"not yet created"}${normal}
WorkDir:       ${bold}$dir_data${normal}
Reference:     ${bold}$reference${normal}

${blue}To monitor the Snakemake log use:${normal}
tail -F -s3 \$(find $dir_data/.snakemake/log -name "*.snakemake.log" -print0 | xargs -r -0 ls -1 -t | head -1)
TXT
}

show_info_on_tmux() {
  ## Prerequisite for tmux wrapping: add `set-option -g default-shell "/bin/bash"` to ~/.tmux.conf
  cat <<- TXT | tee -a $logfile $logfile_global

${blue}Best experience with tmux:${normal}
tmux new -d -s snippysnake_\$USER
tmux send-keys -t snippysnake_\${USER}.0 'bash $script_real --auto' ENTER

All steps were logged to: $logfile and $logfile_global (global)
TXT
}

## Checks --------------------------------------------------

run_checks() {
  [[ ! -d $profile ]] && die "The snakemake profile directory does not exist: $profile"
  [[ $mode == "batch"  && ! -f $run_table      ]] && die "The run table does not exist: $run_table"
  [[ $mode == "config" && ! -f $configfile     ]] && die "The config file does not exist: $configfile"
  # TODO: add more for novices

  return 0
}

## More Settings --------------------------------------------------

tweak_settings() {
  ## more privacy towards NCBI
  declare -x BLAST_USAGE_REPORT=false # see https://github.com/tseemann/mlst/issues/115

  ## append environment libraries to LD path, if dependencies are not found
  [[ $profile =~ "saga" ]] && declare -x LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$conda_env_path/lib/ && logecho "Conda libraries appended to LD_LIBRARY_PATH"
  # NOTE: this may cause a "/bin/bash: <conda-prefix>/envs/aquamis/lib/libtinfo.so.6: no version information available (required by /bin/bash)"
  # NOTE: checkout Issues #13 and #15 at https://gitlab.com/bfr_bioinformatics/AQUAMIS/-/issues/?state=all

  return 0
}

activate_conda() {
  ## activate environment
  if [[ "$conda_env_active" != "$conda_env_name" ]]; then
    first_conda_env=$(head -n1 <<< $conda_env_path)  # use first entry if multiple envs with the same name exist, e.g. from other users
    logecho "Conda environment $conda_env_name has not been activated yet. Activating Conda environment \"$first_conda_env\""
    set -u && source $conda_base/etc/profile.d/conda.sh && set +u
    conda activate $first_conda_env
  else
    logecho "Conda environment $conda_env_name was already activated"
  fi

  ## check
  logecho "Using $(conda info | grep "active env location" | sed -e "s/^\s*//")"

  true
}

## Pre-Run Modules --------------------------------------------------

delete_previousRun() {
  logecho "The script function 'delete_previousRun()' is active. Are you sure?"
  select yn in "Yes, I want to delete previous snippySnake calculations!" "No, I fear consequences."; do
    case $yn in
      "Yes, I want to delete previous snippySnake calculations!" )
        logecho "Starting Cleanup..."
#        [ -d "$dir_data/json"        ] && rm -f -R $dir_data/json
#        [ -d "$dir_data/reports_bak" ] && mv $dir_data/reports_bak $dir_data/reports_old
#        [ -d "$dir_data/reports"     ] && rm -f -R $dir_data/reports
#        [ -d "$dir_data/.snakemake"  ] && rm -f -R $dir_data/.snakemake
#        [ -d "$dir_data/logs"        ] && rm -f -R $dir_data/logs
#        [ -f "$dir_data/config_snippysnake.yaml" ] && mv $dir_data/config.yaml $dir_data/config_9dab9ee.yaml
#        [ -f "$dir_data/config_snippysnake.yaml" ] && rm -f $dir_data/config.yaml
#        [ -f "$dir_data/pipeline_status_snippysnake.txt" ] && rm -f $dir_data/pipeline_status.txt
        echo "Cleanup Finished."
        break
        ;;
      "No, I fear consequences." )
        echo "Cleanup Aborted."
        break
        ;;
    esac
  done < /dev/tty
}

## Modules --------------------------------------------------

run_snippysnake() {
  logheader "Running snippySnake via snippySnake.py:"
  snippysnake_args+="--reference $reference --iqtree "
  read -r -d '' wrapper_cmd <<- EOF
  python3 $repo_path/snippySnake.py
    --profile $profile
    --working_directory $dir_data
    --threads_sample $threads_sample
    --sample_list $sample_list
    ${snippysnake_args:-}
    ${user_args:-}
EOF
#    --rule_directives $dag_search
#    --profile $profile
#    --ephemeral
#    --threads 60
#    --dryrun
#    --force rule

#    --version
#    --conda_frontend
#    --condaprefix $CONDA_BASE/envs
  run_command_array $wrapper_cmd
}

run_snakemake() {
  logheader "Running snippySnake via direct Snakemake command:"
  # NOTE: for CLI options, see https://snakemake.readthedocs.io/en/stable/executing/cli.html
  [[ -f $configfile ]] || die "The config yaml was not created yet in the working directory"

  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $repo_path/profiles/$profile
    --configfile $configfile
    --snakefile $repo_path/snippySnake.smk
    --notemp
    --quiet
    --dryrun
    ${user_args:-}
EOF
#    --quiet progress # TODO: not available in Snakemake 7.5.0
#    --rerun-triggers mtime params input software-env code
#    --forcerun rule
#    --until rule
#    --force rule
#    --summary
#    --keep-incomplete
#    --cleanup-metadata  $(snakemake --configfile $configfile --snakefile $repo_path/Snakefile --list-params-changes)
#    --list-input-changes
#    --list-params-changes
#    --list-code-changes
#    --list-version-changes
#    --list-untracked
#    --cleanup-shadow
#    --conda-prefix $CONDA_BASE/envs
#    --use-conda
  run_command_array $wrapper_cmd
}

run_snakemake_report() {
  logheader "Generating Snakemake Report:"
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --configfile $configfile
    --snakefile $repo_path/Snakefile
    --forceall
    --rulegraph | dot -Tpdf -o $dir_data/rulegraph-all.pdf
EOF
#    --dag | dot -Tpdf -o $dir_data/dag-all.pdf
#    --report $dir_data/reports/smk_report.html
  run_command_array $wrapper_cmd
}

run_modules() {
  ## [Rationale] Define per-run module set - choose between Python wrapper, Snakemake or Docker
  set_run_paths
  show_info_on_run
#  delete_previousRun
  [[ $snakemake == false ]] && run_snippysnake
  [[ $snakemake == true  ]] && run_snakemake
#  run_snakemake_report
}

## Sample Mode --------------------------------------------------

runs_from_run_table() {
  local run_table

  run_table=${1-}

  ## loop through run_table
  mapfile -t runArray <"$run_table"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line) IFS=$' \t\n'
    [[ "${field[0]}" =~ ^#.* ]] && continue # skip commented samples
    run_name=$(eval "echo \"${field[0]}\"")
    dir_samples=$(realpath $(eval "echo \"${field[1]}\""))
    dir_data=$(realpath $(eval "echo \"${field[2]}\""))
    [[ ${#field[@]} -eq 4 ]] && reference=$(eval "echo \"${field[3]}\"")
    run_modules
  done
}

run_from_hardcoded() {
  run_name="test_data_$(date +%F)_$(hostname)"
  dir_samples="$repo_path/test_data"
  dir_data="$repo_path/test_data/analysis_test_data_$(date +%F)"
  reference="$repo_path/test_data/reference/GCF_001047715.1_ASM104771v1_genomic.fna"
  run_modules
}

run_from_configfile() {
  local configfile

  configfile=${1:-}
  snippysnake_args+="--config $configfile "

  ## get values from configfile
  sample_list=$(grep -E "^samples:" "$configfile" | awk -F': ' '{print $2}')
  dir_samples=$(dirname "$sample_list")
  dir_data=$(grep -E "^workdir:" "$configfile" | awk -F': ' '{print $2}')
  threads_sample=$(grep -E "^  threads:" "$configfile" | awk -F': ' '{print $2}')
  reference=$(grep -E "^reference:" "$configfile" | awk -F': ' '{print $2}')
  run_modules
}

## (3) Main

manual_override
define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info
run_checks

## Workflow
check_conda $conda_recipe_env_name
activate_conda
[[ $interactive == true ]] && interactive_query
tweak_settings

## Switch between config, single-run or batch mode
case ${mode} in
  batch)
    logecho "Processing all active runs in ${magenta}run table${normal}: $run_table"
    runs_from_run_table "$run_table"
    ;;
  single)
    logecho "Processing a ${magenta}single run${normal}"
    run_from_hardcoded
    ;;
  config)
    logecho "Processing run defined in snippySnake ${magenta}config file${normal}: $configfile"
    run_from_configfile "$configfile"
    ;;
  *) die "The {mode} was not specified or recognized. Check ${blue}bash $script_real --help${normal} for available parameters" ;;
esac

show_info_on_tmux
logglobal "[STOP] $script_real"
