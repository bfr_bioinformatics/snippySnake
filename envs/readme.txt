conda create -n snippy_pipeline_dependencies snippy=3.2 snp-dists pandas python-dateutil=2.5 fasttree r-rmarkdown
source activate snippy_pipeline_dependencies
conda-env export > /home/DenekeC/Snakefiles/snippy-pipeline/envs/snippy.yaml
