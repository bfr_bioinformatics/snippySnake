snippySnake - A pipeline for snp-calling, snp-filtering and phylogenetic analysis

# News

* New wrapper available:
* Includes option for reference search
* HTML SNP report
* gubbins for

Variant calling pipeline based on the tool snippy.

1. It takes a number bacterial fastq files as input and either a selected reference or finds the optimal reference (based on mash) from a set of complete NCBI refseq references
2. It calls snippy (based on freebayes) for each isolate and the reference
3. It identifies the core SNPs (snippy-core) and the SNP distance matrix (snp-dist)
4. Optional: It removed recombinant sites (using gubbins)
5. Optional: It creates a maximum likelihood tree (using iqtree)
6. Optional: It creates an interactive html report

See also documentation of snippy.

## Authors

* Carlus Deneke (@carlus.deneke@bfr.bund.de)

## Installation

### Step 1: Get the source code by cloning this repisitory

```bash
git clone https://gitlab.com/bfr_bioinformatics/snippySnake.git
```

to a directory of your choice (`path/2/repo`).

### Step 2: Create a conda environment

snippySnake relies on the conda package manager for all dependencies.
Please set up conda on your system as explained [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).
Also make sure that you have the mamba package available (`conda install mamba`).

All requirements are installed via

```bash
mamba env create -n snippysnake -f envs/snippysnake.yaml
```

If this was successfull, you installed all required software packages and reference databases.

_Note_: If resolving the dependencies fails, you may choose to install a mininimum environment containing snakemake and biopython.
Using the option `--use-conda` with `snippySnake.py` will install all depenendencies in a modular way upon the first execution of snippySnake.
See option `--condaprefix` in `snippySnake.py --help` for options where to store the environment files.

## Usage

### Create a sample sheet

All data must be supplied in a sample sheet (samples.tsv). It requires the following entries:

```
sample    fq1   fq2
sample_1    path/2/sample1_R1.fastq.gz  path/2/sample1_R2.fastq.gz
sample_2    path/2/sample2_R1.fastq.gz  path/2/sample2_R2.fastq.gz
```

The sample sheet can be created with the helper script `path/2/repo/scripts/create_sampleSheet.sh`.

```
cd path/2/fastqdata
path/2/repo/scripts/create_sampleSheet.sh --mode flex ## using a flexible scheme where the part before the first '_' denotes the sample name sample_*_R[12]_*.fastq.gz
path/2/repo/scripts/create_sampleSheet.sh --mode illumina ## using the illulima naming scheme sample_S[0-9]_L001_R[12]_001.fastq.gz
path/2/repo/scripts/create_sampleSheet.sh --mode ncbi ## using ncbi style names: sample_[12].fastq.gz
```

See `path/2/repo/scripts/create_sampleSheet.sh --help` for more details.

### Execution

Activate conda environment

```bash
conda activate snippysnake
```

#### Using a pre-defined reference file

```bash
path/2/repo/snippySnake.py --sample_list path/2/samples.tsv --reference path/2/ref --working_directory path/2/results
```

where

* `path/2/repo` is the path to the snippySnake repository
* `path/2/samples.tsv` is the path to the sample sheet specifying all fastq files (see above)
* `path/2/ref` is the path to the reference in fasta or genbank format
* `path/2/results` is the path where all results are to be stored

You can check the correct configuration by appending the `--dryrun` flag to your command.

#### Perform a reference search and execute snippy with the optimal reference file

```bash
path/2/repo/snippySnake.py --sample_list path/2/samples.tsv --working_directory path/2/results
```

I.e. by not specifying a reference with `--reference` or `-r`. You can specify the location of the mash db with `--mash_db` and `--mash_info`. All other optional commands can be used as well.

#### Perform a reference search only

```bash
path/2/repo/snippySnake.py --sample_list path/2/samples.tsv --working_directory path/2/results --nosnp
```

The resulting candidate references and optimal reference can be found in

The snp analysis can be executed by removing the `--nosnp` flag.

### Important optional parameters

* `--report`: Create an interactice html report
* `--iqtree`: Perform a maximum-likelihood. Check related parameters in help file and iqtree software for more information
* `--gubbins`: Perform a recombination analysis and filtering to remove SNPs in recombinant areas (advisable for highly recombinant organsims such as Campylobacter).
  Check related parameters in help file and gubbins for more options.
* `--dryrun`: Do not execute workflow but check if all parameters set correctly

### Further options

Specify `path/2/repo/snippySnake.py --help` for a list of all available options.

<details><summary>help file</summary>
<p>

```
usage: snippySnake.py [-h] [--sample_list SAMPLE_LIST]
                      [--working_directory WORKING_DIRECTORY]
                      [--snakefile SNAKEFILE] [--config CONFIG]
                      [--reference REFERENCE] [--refseq_db REFSEQ_DB]
                      [--mash_db MASH_DB] [--mash_info MASH_INFO]
                      [--mash_minoverlap MASH_MINOVERLAP]
                      [--mash_kmersize MASH_KMERSIZE]
                      [--mash_sketchsize MASH_SKETCHSIZE] [--bedfile BEDFILE]
                      [--mapqual MAPQUAL] [--basequal BASEQUAL]
                      [--mincov MINCOV] [--minfrac MINFRAC]
                      [--minqual MINQUAL] [--maxsoft MAXSOFT]
                      [--iqtree_model IQTREE_MODEL] [--iqtree_bb IQTREE_BB]
                      [--iqtree_alrt IQTREE_ALRT] [--iqtree_nm IQTREE_NM]
                      [--clustering_cutoff CLUSTERING_CUTOFF]
                      [--outlier_threshold OUTLIER_THRESHOLD]
                      [--gubbins_tree_builder GUBBINS_TREE_BUILDER]
                      [--gubbins_iterations GUBBINS_ITERATIONS]
                      [--gubbins_min_snps GUBBINS_MIN_SNPS]
                      [--gubbins_filter_percentage GUBBINS_FILTER_PERCENTAGE]
                      [--gubbins_converge_method GUBBINS_CONVERGE_METHOD]
                      [--gubbins_min_window_size GUBBINS_MIN_WINDOW_SIZE]
                      [--gubbins_max_window_size GUBBINS_MAX_WINDOW_SIZE]
                      [--gubbins_raxml_model GUBBINS_RAXML_MODEL] [--report]
                      [--iqtree] [--nj] [--raxml] [--gubbins] [--nosnp]
                      [--rule_directives RULE_DIRECTIVES] [--use_conda]
                      [--conda_frontend] [--condaprefix CONDAPREFIX]
                      [--threads THREADS] [--threads_sample THREADS_SAMPLE]
                      [--force FORCE] [--forceall FORCEALL]
                      [--profile PROFILE] [--dryrun] [--unlock]
                      [--logdir LOGDIR] [--version]

optional arguments:
  -h, --help            show this help message and exit
  --sample_list SAMPLE_LIST, -l SAMPLE_LIST
                        [REQUIRED] List of samples to analyze, as a two column
                        tsv file with columns sample and assembly paths. Can
                        be generated with provided script
                        create_sampleSheet.sh
  --working_directory WORKING_DIRECTORY, -d WORKING_DIRECTORY
                        [REQUIRED] Working directory where results are saved
  --snakefile SNAKEFILE, -s SNAKEFILE
                        Path to Snakefile of snippySnake; default:
                        <snippySnake>/snippySnake.smk
  --config CONFIG, -c CONFIG
                        Path to pre-defined config file (Other parameters will
                        be ignored)
  --reference REFERENCE, -r REFERENCE
                        Path to reference in fasta or genbank format
                        (unzipped)
  --refseq_db REFSEQ_DB
                        Path to refseq assembly summary (default: <snippySnake>
                        /ref/assembly_summary.txt)
  --mash_db MASH_DB     Path to mash db (default: <snippySnake>/ref/
                        Refseq_completeBacteria_chromosomes_s1000_k21.msh)
  --mash_info MASH_INFO
                        Path to mash db (default: <snippySnake>/ref/
                        Refseq_completeBacteria_chromosomes_s1000_k21.info)
  --mash_minoverlap MASH_MINOVERLAP
                        Minimum number of shared hashes to keep (default
                        "500")
  --mash_kmersize MASH_KMERSIZE
                        kmer size for mash, must match size of database,
                        default 21
  --mash_sketchsize MASH_SKETCHSIZE
                        sketch size for mash, must match size of database,
                        default 1000
  --bedfile BEDFILE     Path to bedfile with regions to be excluded from SNP
                        core analysis
  --mapqual MAPQUAL     Minimum read mapping quality to consider (default
                        "60")
  --basequal BASEQUAL   Minimum base quality to consider (default "13")
  --mincov MINCOV       Minimum site depth to for calling alleles (default
                        "10")
  --minfrac MINFRAC     Minumum proportion for variant evidence (0=AUTO)
                        (default "0")
  --minqual MINQUAL     Minumum QUALITY in VCF column 6 (default "100")
  --maxsoft MAXSOFT     Maximum soft clipping to allow (default "10")
  --iqtree_model IQTREE_MODEL
                        Choose from "GTR+ASC","GTR+G4","MFP" for model finding
                        (default "GTR+ASC")
  --iqtree_bb IQTREE_BB
                        Ultrafast bootstrap (default: 1000)
  --iqtree_alrt IQTREE_ALRT
                        SH-like approximate likelihood ratio test (default:
                        1000)
  --iqtree_nm IQTREE_NM
                        Maximum number of iterations (default: 10000)
  --clustering_cutoff CLUSTERING_CUTOFF
                        Threshold for SNP clustering
  --outlier_threshold OUTLIER_THRESHOLD
                        Number of SNPs to reference to be considered as
                        outlier
  --gubbins_tree_builder GUBBINS_TREE_BUILDER
                        {raxml,fasttree,hybrid} Application to use for tree
                        building (default: raxml)
  --gubbins_iterations GUBBINS_ITERATIONS
                        Maximum No. of iterations (default: 5)
  --gubbins_min_snps GUBBINS_MIN_SNPS
                        Min SNPs to identify a recombination block (default:
                        3)
  --gubbins_filter_percentage GUBBINS_FILTER_PERCENTAGE
                        Filter out taxa with more than this percentage of gaps
                        (default: 25)
  --gubbins_converge_method GUBBINS_CONVERGE_METHOD
                        {weighted_robinson_foulds,robinson_foulds,recombinatio
                        n} Criteria to use to know when to halt iterations
                        (default: weighted_robinson_foulds)
  --gubbins_min_window_size GUBBINS_MIN_WINDOW_SIZE
                        Minimum window size (default: 100)
  --gubbins_max_window_size GUBBINS_MAX_WINDOW_SIZE
                        Maximum window size (default: 10000)
  --gubbins_raxml_model GUBBINS_RAXML_MODEL
                        {GTRGAMMA,GTRCAT}, RAxML model (default: GTRCAT)
  --report              Create html report
  --iqtree              Create Maximum likelihood tree with iqtree
  --nj                  Create neighbor-joining tree with fastnj
  --raxml               Create Maximum likelihood tree with raxml
  --gubbins             Filter SNPs due to recombination using gubbins
  --nosnp               Only reffinder
  --rule_directives RULE_DIRECTIVES
                        Process DAG with rule grouping and/or rule
                        prioritization via Snakemake rule directives YAML;
                        default: "<snippySnake>/profiles/smk_directives.yaml"
                        equals no directives
  --use_conda           Utilize the Snakemake "--useconda" option, i.e. Smk
                        rules require execution with a specific conda env
  --conda_frontend      Do not use mamba but conda as frontend to create
                        individual conda environments
  --condaprefix CONDAPREFIX
                        Path of default conda environment, enables recycling
                        of built environments; default:
                        <snippySnake>/conda_env
  --threads THREADS, -t THREADS
                        Number of Threads/Cores to use. This overrides the
                        "<snippySnake>/profiles" settings
  --threads_sample THREADS_SAMPLE, -p THREADS_SAMPLE
                        Number of Threads to use per sample in multi-threaded
                        rules; default: 5
  --force FORCE, -f FORCE
                        Snakemake force. Force recalculation of output (rule
                        or file) specified here
  --forceall FORCEALL   Snakemake force. Force recalculation of all steps
  --profile PROFILE     (A) Full path or (B) directory name under
                        "<snippySnake>/profiles" of a Snakemake config_snippysnake.yaml
                        with Snakemake parameters, e.g. available CPUs and
                        RAM. Default: "bfr.hpc"
  --dryrun, -n          Snakemake dryrun. Only calculate graph without
                        executing anything
  --unlock              Unlock a Snakemake execution folder if it had been
                        interrupted
  --logdir LOGDIR       Path to directory where .snakemake output is to be
                        saved
  --version, -V         Print program version
```

</p>
</details>

#### Explicit call of the snakemake workflow (skipping the wrapper)

```
conda activate snippysnake
snakemake -p -s /path/to/repo/snippySnake.smk --cores 20 -f all --keep-going --configfile /path/to/config_snippysnake.yaml
```

All parameters are specified in the configfile `config_snippysnake.yaml`.
This file can be created initially using the wrapper `snippySnake.py`.
See the [Snakemake documentation](https://snakemake.readthedocs.io) for further details.

### Output

* config_snippysnake.yaml: All parameters
* pipeline_status:
* The directory `log/` contains numerous files with logging information for each of the workflow's steps

#### Results

Results are provided in subdirecory `results` of your working directory.

* _snp_distancematrix_unfiltered.tsv_: Describes the main output, the pairwise distance matrix between all samples (and the reference).
  Unfiltered denotes that no recombination filtering (gubbins) has been applied
* _report.html_: An interactive HTML report summarizing all findings

An example report can be found [here](https://gitlab.com/bfr_bioinformatics/snippySnake/-/blob/master/test_data/example_report/report.html).

### Troubleshooting

* If you need help, please read help file (`snippySnake.py --help`) and gitlab README
* You may need to set up a ftp proxy for the automatic reference download
* Is is recommended to use snippySnake with trimmed reads
* References must be unzipped!
* If you have unconventional read names, the wrapper `create_sampleSheet.sh` might not work for you.
* Single-ended (short) reads (also IonTorrent) are supported - please specify the second read (column fq) as NA;
* long-reads not supported (yet)
* In case of problems, please file an issue on gitlab or contact the developer

### Test data

For testing purposes, test data and expected results are provided in [test_data](https://gitlab.com/bfr_bioinformatics/snippySnake/-/tree/master/test_data) and may be retrieved with [snippySnake_setup.sh](scripts/snippySnake_setup.sh).

The selected data (see file [sra_accessions.tsv](https://gitlab.com/bfr_bioinformatics/snippySnake/-/blob/master/test_data/sra_accessions.tsv)) is publically available from NCBI SRA.
The data set is described [here](https://github.com/WGS-standards-and-analysis/datasets/tree/master/datasets).
For this testing purpose, a subset of the data described [here](https://github.com/WGS-standards-and-analysis/datasets/blob/master/datasets/Listeria_monocytogenes_1408MLGX6-3WGS.tsv) was chosen.
For convenience, the read data is provided in a [zip file](https://seafile.bfr.berlin/f/aec79e3102e04288a66e/?dl=1).
Note however, that the original data including checksums are availabe at NCBI.

The suggested reference described in the dataset is provided [here](https://seafile.bfr.berlin/f/cfed77a4dcd9445aadb2/?dl=1).
More information about the refererence can be found [here](https://gitlab.com/bfr_bioinformatics/snippySnake/-/blob/master/test_data/reference_accession.tsv).

In order to run the test, please process as described in section _Execution_:

1. Activate conda env
2. Create sample sheet of test data
3. Run snippy Snake with the path to the sample sheet and the reference

For comparison, snippySnake results using the suggested references are [example_report](https://gitlab.com/bfr_bioinformatics/snippySnake/-/blob/master/test_data/example_report/report.html)
and [example_distance_matrix](https://gitlab.com/bfr_bioinformatics/snippySnake/-/blob/master/test_data/example_report/snp_distancematrix_unfiltered.tsv).


****

# References

* snippy
* mash
* gubbins
* iqtree
