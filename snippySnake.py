#!/usr/bin/env python3

# Python Standard Packages
import argparse
import subprocess
import os
import sys

# Other Packages
import pandas as pd
from yaml import dump as yaml_dump, safe_load as yaml_load


# %% Functions

def argument_parser(pipeline: str = "snippySnake", repo_path: str = os.path.dirname(os.path.realpath(__file__))) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    # region # parse arguments
    # path arguments
    parser.add_argument('-l', '--sample_list', default = "/", type = os.path.abspath, help = '[REQUIRED] List of samples to analyze, as a two column tsv file with columns sample and assembly paths. Can be generated with provided script create_sampleSheet.sh')
    parser.add_argument('-d', '--working_directory', default = "/", type = os.path.abspath, help = '[REQUIRED] Working directory where results are saved')
    parser.add_argument('-s', '--snakefile', default = os.path.join(repo_path, "snippySnake.smk"), help = f'Path to Snakefile of {pipeline}; default: {os.path.join(repo_path, "snippySnake.smk")}')
    parser.add_argument('-c', '--config', default = "/", help = 'Path to pre-defined config file (other parameters will be ignored)')
    parser.add_argument('-r', '--reference', default = "", type = os.path.abspath, help = 'Path to reference in unzipped fasta or genbank format')
    # mash parameters
    parser.add_argument('--refseq_db', default = os.path.join(repo_path, "ref/assembly_summary.txt"), type = os.path.abspath, help = f'Path to refseq assembly summary; default: {os.path.join(repo_path, "ref/assembly_summary.txt")}')
    parser.add_argument('--mash_db', default = os.path.join(repo_path, "ref/Refseq_completeBacteria_chromosomes_s1000_k21.msh"), type = os.path.abspath, help = f'Path to mash db; default: {os.path.join(repo_path, "ref/Refseq_completeBacteria_chromosomes_s1000_k21.msh")}')
    parser.add_argument('--mash_info', default = os.path.join(repo_path, "ref/Refseq_completeBacteria_chromosomes_s1000_k21.info"), type = os.path.abspath, help = f'Path to mash db; default: {os.path.join(repo_path, "ref/Refseq_completeBacteria_chromosomes_s1000_k21.info")}')
    parser.add_argument('--mash_minoverlap', default = 500, type = int, help = 'Minimum number of shared hashes to keep; default: 500')
    parser.add_argument('--mash_kmersize', default = 21, type = int, help = 'kmer size for mash, must match size of database; default: 21')
    parser.add_argument('--mash_sketchsize', default = 1000, type = int, help = 'sketch size for mash, must match size of database; default: 1000')
    # snippy parameters
    parser.add_argument('--bedfile', default = "", help = 'Path to bedfile with regions to be excluded from SNP core analysis')
    parser.add_argument('--mapqual', default = 60, type = int, help = 'Minimum read mapping quality to consider; default: 60')
    parser.add_argument('--basequal', default = 13, type = int, help = 'Minimum base quality to consider; default: 13')
    parser.add_argument('--mincov', default = 10, type = int, help = 'Minimum site depth to for calling alleles; default: 10')
    parser.add_argument('--minfrac', default = 0, type = int, help = 'Minumum proportion for variant evidence (0=AUTO); default: 0')
    parser.add_argument('--minqual', default = 100, type = int, help = 'Minumum QUALITY in VCF column 6; default: 100')
    parser.add_argument('--maxsoft', default = 10, type = int, help = 'Maximum soft clipping to allow; default: 10')
    # iqtree parameters
    parser.add_argument('--iqtree_model', default = "GTR+ASC", help = 'Choose from "GTR+ASC","GTR+G4","MFP" for model finding; default: "GTR+ASC"')
    parser.add_argument('--iqtree_bb', default = 1000, type = int, help = 'Ultrafast bootstrap; default: 1000')
    parser.add_argument('--iqtree_alrt', default = 1000, type = int, help = 'SH-like approximate likelihood ratio test; default: 1000')
    parser.add_argument('--iqtree_nm', default = 10000, type = int, help = 'Maximum number of iterations; default: 10000')
    # report clustering parameters
    parser.add_argument('--clustering_cutoff', default = 10, type = int, help = 'Threshold for SNP clustering')
    parser.add_argument('--outlier_threshold', default = 1000, type = int, help = 'Number of SNPs to reference to be considered as outlier')
    # gubbins parameters
    parser.add_argument('--gubbins_tree_builder', default = "raxml", help = '{raxml,fasttree,hybrid} Application to use for tree building; default: "raxml"')
    parser.add_argument('--gubbins_iterations', default = 5, help = 'Maximum number of iterations; default: 5')
    parser.add_argument('--gubbins_min_snps', default = 3, help = 'Minimum SNPs to identify a recombination block; default: 3')
    parser.add_argument('--gubbins_filter_percentage', default = 25, help = 'Filter out taxa with more than this percentage of gaps; default: 25')
    parser.add_argument('--gubbins_converge_method', default = "weighted_robinson_foulds", help = '{weighted_robinson_foulds,robinson_foulds,recombination} Criteria to use to know when to halt iterations; default: "weighted_robinson_foulds"')
    parser.add_argument('--gubbins_min_window_size', default = 100, help = 'Minimum window size; default: 100')
    parser.add_argument('--gubbins_max_window_size', default = 10000, help = 'Maximum window size; default: 10000')
    parser.add_argument('--gubbins_raxml_model', default = "GTRCAT", help = '{GTRGAMMA,GTRCAT}, RAxML model; default: "GTRCAT"')
    # density filter
    parser.add_argument('--density_filter', default = False, action = 'store_true', help = 'Apply density_filter')
    parser.add_argument('--densityfilter_snp_threshold', default = 3, type = int, help = 'Minimum number of SNPs in window considered as high density region; default: 3')
    parser.add_argument('--densityfilter_window_size', default = 1000, type = int, help = 'Window size for density filter; default: 1000')
    parser.add_argument('--densityfilter_type', default = "window", help = 'Choose for "window" or "consecutive". The latter overwrites the window_size by the value of snp_threshold; default: "window"')
    # all rule deciders
    parser.add_argument('--report', default = False, action = 'store_true', help = 'Create html report')
    parser.add_argument('--iqtree', default = False, action = 'store_true', help = 'Create Maximum likelihood tree with iqtree')
    parser.add_argument('--nj', default = False, action = 'store_true', help = 'Create neighbor-joining tree with fastnj')
    parser.add_argument('--raxml', default = False, action = 'store_true', help = 'Create Maximum likelihood tree with raxml')
    parser.add_argument('--gubbins', default = False, action = 'store_true', help = 'Filter SNPs due to recombination using gubbins')
    parser.add_argument('--nosnp', default = False, action = 'store_true', help = 'Only reffinder')
    parser.add_argument('--fix-zero-snps', default = False, action = 'store_true', help = 'Fix snippy-core crash if zero SNPs in dataset')
    parser.add_argument('--all_pairwise', default = False, action = 'store_true', help = 'Also compute pairwise variants not only core snps. Requires that the reference is identical with a sample. The corresponding reference name must be specified with --reference_name')
    parser.add_argument('--reference_name', default = "", help = 'Sample name in sample sheet that corresponds to reference. Only required if option all_pairwise is set')
    # snippySnake arguments
    parser.add_argument('--rule_directives', default = os.path.join(repo_path, "profiles", "smk_directives.yaml"), type = os.path.abspath,
                        help = f'Process DAG with rule grouping and/or rule prioritization via Snakemake rule directives YAML; default: {os.path.join(repo_path, "profiles", "smk_directives.yaml")} equals no directives')
    # conda arguments
    parser.add_argument('--use_conda', default = False, action = 'store_true', help = 'Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env')
    parser.add_argument('--conda_frontend', default = False, action = 'store_true', help = 'Do not use mamba but conda as frontend to create individual conda environments')
    parser.add_argument('--condaprefix', default = os.path.join(repo_path, "conda_env"), help = f'Path of default conda environment, enables recycling of built environments; default: {os.path.join(repo_path, "conda_env")}')
    # cpu arguments
    parser.add_argument('-t', '--threads', default = 0, type = int, help = f'Number of Threads/Cores to use. This overrides the "<{pipeline}>/profiles" settings')
    parser.add_argument('-p', '--threads_sample', default = 5, type = int, help = 'Number of Threads to use per sample in multi-threaded rules; default: 5', metavar = 'THREADS')
    parser.add_argument('-f', '--force', default = False, type = str, help = 'Snakemake force. Force recalculation of output (rule or file) specified here', metavar = 'RULE')
    parser.add_argument('--forceall', default = False, type = str, help = 'Snakemake force. Force recalculation of all steps', metavar = 'RULE')
    parser.add_argument('--profile', default = "none", type = str, help = f'(A) Full path or (B) directory name under "<{pipeline}>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM; default: "bfr.hpc"')
    parser.add_argument('-n', '--dryrun', default = False, action = 'store_true', help = 'Snakemake dryrun. Only calculate graph without executing anything')
    parser.add_argument('--unlock', default = False, action = 'store_true', help = 'Unlock a Snakemake execution folder if it had been interrupted')
    parser.add_argument('--logdir', default = "/", type = os.path.abspath, help = 'Path to directory where .snakemake output is to be saved')
    parser.add_argument('-V', '--version', default = False, action = 'store_true', help = 'Print program version')
    # endregion # parse arguments
    return parser


def evaluate_arguments(args: argparse.Namespace) -> argparse.Namespace:
    # version
    args.version_fixed = open(os.path.join(args.repo_path, 'VERSION'), 'r').readline().strip()
    try:
        args.version_git = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL, cwd = args.repo_path).decode('ascii').strip()
    except:
        args.version_git = args.version_fixed

    if args.version:
        print(f"{args.pipeline} version\t{args.version_fixed}")
        if not args.version_fixed == args.version_git:
            print(f"commit      version\t{args.version_git.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {args.pipeline} version {args.version_fixed} ({args.version_git})")

    # check required arguments
    required_paths_to_check = {
        "-d/--working_directory": args.working_directory,
        "-l/--sample_list"      : args.sample_list,
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if path == '/'}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the following argument is required: {key}")
        sys.exit(1)

    # check dependency path
    dependency_paths_to_check = {
        "-l/--sample_list": args.sample_list,
        "-r/--reference"  : args.reference,
        "--refseq_db"     : args.refseq_db,
        "--mash_db"       : args.mash_db,
        "--mash_info"     : args.mash_info
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
        sys.exit(1)

    # create working directory if it does not exist
    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    # set snakemake logdir
    args.logdir = args.working_directory if args.logdir == "/" else args.logdir
    print(f"Snakemake logs are saved in {args.logdir}/.snakemake/log")

    # find snakemake profile: first check relative paths, then absolute paths to make args.profile absolute
    if os.path.exists(os.path.join(args.repo_path, "profiles", args.profile)):
        smk_profile = os.path.join(args.repo_path, "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.join(args.repo_path, "profiles", "bfr.hpc")):
        smk_profile = os.path.join(args.repo_path, "profiles", "bfr.hpc")
    else:
        print(f"ERROR: path to Snakemake config.yaml {args.profile} does not exist")
        sys.exit(1)
    args.profile = smk_profile

    # other checks
    ## mutually exclusive options
    if args.gubbins and args.density_filter:
        print("ERROR: Options --gubbins and --density_filter are mutually exclusive")
        sys.exit(1)

    if (args.iqtree and args.raxml) or (args.iqtree and args.nj) or (args.raxml and args.nj):
        print("ERROR: Options --iqtree, --raxml and --nj are mutually exclusive")
        sys.exit(1)

    ## handle less than three sample per run
    with open(args.sample_list, 'r') as fp:
        sample_count = len(fp.readlines()) - 1
        print('Sample count:', sample_count)

    if sample_count < 3:
        args.iqtree = False
        args.gubbins = False
        print("Gubbins and IQ-TREE will be disabled with less than 3 samples in the sample sheet")

    ## define reference and check format
    if os.path.isfile(args.reference):
        args.use_ref = True
        args.reference_type = "manual"
    else:
        args.use_ref = False
        args.reference_type = "auto"
        args.reference = os.path.join(args.working_directory, 'reffinder/reference/reference.gbff')

    args.reference_format = "compressed" if args.reference.endswith(('.gz', '.zip')) else "NA"
    args.reference_format = "fasta" if args.reference.endswith(('.fasta', '.fna', '.fas')) else args.reference_format
    args.reference_format = "genbank" if args.reference.endswith(('.gbff', '.genbank', 'gbk')) else args.reference_format

    if args.reference_format == "compressed":
        print("ERROR: Reference file must not be compressed")
        sys.exit(1)
    elif args.reference_format == "NA":
        print("Reference format not recognized")
    else:
        print(f"Reference in {args.reference_format} format")

    ## check that reference name is specified if all_pairwise
    if args.all_pairwise:
        if args.reference_name == "":
            print("ERROR: For option '--all_pairwise', option '--reference_name' must be specified. It must be one of the sample names in the sample sheet")
            sys.exit(1)
        # check that reference name is in sample list
        df = pd.read_table(args.sample_list)
        samples = df['sample'].tolist()
        if not args.reference_name in samples:
            print(f"ERROR: {args.reference_name} not part of sample sheet args.sample_list")
            sys.exit(1)

    # return modified args
    return args


def check_sample_list_hybrid(sample_list):
    """Check if sample list has a fastq header"""
    # Check whether sample list has header
    type1 = "sample\tfq1\tfq2\n"
    type2 = "sample\tassembly\n"

    with open(sample_list, 'r') as handle:
        header_line = handle.readline()

    if header_line != type1 and header_line != type2:
        print(f"ERROR: sample list {sample_list} does not have a proper header. Make sure that first line is either \n{type1}(tab-separated) or\n{type2}(tab-separated)")
        sys.exit(1)
    elif header_line == type1:
        print("Input is fastq")
    else:
        print("Input is assembly")


def create_config(args: argparse.Namespace):
    # region # create config dictionary
    config_dict = {
        'pipeline'   : args.pipeline,
        'version'    : args.version_fixed,
        'git_version': args.version_git,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'reference'  : args.reference,
        'smk_params' : {
            'log_directory'  : args.logdir,
            'profile'        : args.profile,
            'rule_directives': args.rule_directives,
            'threads'        : args.threads_sample
        },
        'parameters' : {  # TODO: change to params
            'use_reference'       : args.use_ref,
            'reference_type'      : args.reference_type,
            'reference_format'    : args.reference_format,
            'reference_name'      : args.reference_name,
            'do_report'           : args.report,
            'do_iqtree'           : args.iqtree,
            'do_nj'               : args.nj,
            'do_raxml'            : args.raxml,
            'do_gubbins'          : args.gubbins,
            'density_filter'      : args.density_filter,
            'branch_remove_cutoff': 0.5,
            'clustering_cutoff'   : args.clustering_cutoff,
            'outlier_threshold'   : args.outlier_threshold,
            'snippy'              : {
                'mapqual' : args.mapqual,
                'basequal': args.basequal,
                'mincov'  : args.mincov,
                'minfrac' : args.minfrac,
                'minqual' : args.minqual,
                'maxsoft' : args.maxsoft,
                'bedfile' : args.bedfile
            },
            'filtering'           : {
                'apply_density_filter': args.density_filter,
                'snp_threshold'       : args.densityfilter_snp_threshold,
                'window_size'         : args.densityfilter_window_size,
                'filter_type'         : args.densityfilter_type
            },
            'mash'                : {
                'refseq_db'      : args.refseq_db,
                'mash_db'        : args.mash_db,
                'mash_info'      : args.mash_info,
                'mash_minoverlap': args.mash_minoverlap,
                'kmersize'       : args.mash_kmersize,
                'sketchsize'     : args.mash_sketchsize
            },
            'gubbins'             : {
                'tree_builder'     : args.gubbins_tree_builder,
                'iterations'       : args.gubbins_iterations,
                'min_snps'         : args.gubbins_min_snps,
                'filter_percentage': args.gubbins_filter_percentage,
                'converge_method'  : args.gubbins_converge_method,
                'min_window_size'  : args.gubbins_min_window_size,
                'max_window_size'  : args.gubbins_max_window_size,
                'raxml_model'      : args.gubbins_raxml_model
            }
        }
    }
    # endregion # create config dictionary

    # region # create conditional config dictionary
    iqtree_dict = {
        'iqtree': {
            'iqtree_model': args.iqtree_model,
            'iqtree_bb'   : args.iqtree_bb,
            'iqtree_alrt' : args.iqtree_alrt,
            'iqtree_nm'   : args.iqtree_nm
        }
    }
    config_dict['parameters'].update(iqtree_dict) if args.iqtree else None
    # endregion # create conditional config dictionary

    # write config dictionary to yaml
    with open(args.config_file, 'w') as outfile:
        yaml_dump(data = config_dict, stream = outfile, default_flow_style = False, sort_keys = False)
    print(f"Config written to {args.config_file}")

    return config_dict


def check_config(config: dict) -> bool:
    # check required paths
    required_paths_to_check = {
        "config.workdir"                 : config.get('workdir', ''),
        "config.samples"                 : config.get('samples', ''),
        "config.reference"               : config.get('reference', ''),
        "config.smk_params.log_directory": config.get('smk_params', {}).get('log_directory', ''),
        "config.smk_params.profile"      : config.get('smk_params', {}).get('profile', '')
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if not os.path.exists(path)}
    if missing_required_paths:  # TODO:
        print(f"WARNING: You are using an old config structure from v3.1.3 or earlier. Please create a new config_snippysnake.yaml") if not config.get('smk_params', None) else None
        for key, path in missing_required_paths.items():
            print(f"ERROR: the config-defined path does not exist: {key} -> {path}")
        sys.exit(1)

    # check dependency paths - keep in sync with evaluate_arguments()
    dependency_paths_to_check = {
        "config.parameters.mash.refseq_db": config.get('parameters', {}).get('mash', {}).get('refseq_db', ''),
        "config.parameters.mash.mash_db"  : config.get('parameters', {}).get('mash', {}).get('mash_db', ''),
        "config.parameters.mash.mash_info": config.get('parameters', {}).get('mash', {}).get('mash_info', '')
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
        sys.exit(1)

    return True


def build_snakemake_call(args: argparse.Namespace) -> str:
    # force mode
    force_mode = "--forceall" if args.forceall else ""
    force_mode = "--force" if args.force else force_mode

    # all-rule force target, add new all rules here
    force_target = "collect_snippy_fix" if args.fix_zero_snps else ""
    force_target = "all_pairwise" if args.all_pairwise else force_target
    print("Fixing snippy-core for zero SNPs. After completion re-run with your previous settings") if force_target == "collect_snippy_fix" else None
    print("Computing also all pairwise variants") if force_target == "all_pairwise" else None

    # user-specified force target; force_targets are mutually exclusive
    force_target = args.forceall if args.forceall else force_target
    force_target = args.force if args.force else force_target
    force = f"{force_mode} {force_target}"

    # other workflow options
    threads = "--cores" if args.profile == "none" else ""  # use all available cores, if no profile is specified
    threads = f"--cores {args.threads}" if args.threads > 0 else threads  # override cores by commandline arg if provided
    dryrun = "--quiet --dryrun" if args.dryrun else ""  # TODO: --quiet rules is yet not available in Snakemake 7.5.0
    unlock = "--unlock" if args.unlock else ""

    # create command
    command = f"snakemake --profile {args.profile} --configfile {args.config_file} " \
              f"--snakefile {args.snakefile} {threads} {dryrun} {unlock} {force}"

    # append conda options if requested
    if args.use_conda:
        frontend = "conda" if args.conda_frontend else "mamba"
        command = command + f" --use-conda --conda-prefix {args.condaprefix} --conda-frontend {frontend}"

    return command


# %% Main

def main():
    # create commandline parser
    parser = argument_parser()

    # parse arguments and create namespace object
    args = parser.parse_args()

    # append settings to args namespace
    args.pipeline = "snippySnake"
    args.repo_path = os.path.dirname(os.path.realpath(__file__))
    args.config_file = os.path.join(args.working_directory, "config_snippysnake.yaml")

    if args.config == "/":
        # evaluate arguments
        args = evaluate_arguments(args = args)

        # write a new config file
        config = create_config(args = args)
    else:
        # load config preset
        print(f"Config file preset used from {args.config_file}")
        with open(args.config_file, 'r') as file:
            config = yaml_load(file)

        # update args with config or fall back to defaults
        args.sample_list = config.get('samples')
        args.logdir = config.get('smk_params', {}).get('log_directory', args.logdir)
        args.profile = config.get('smk_params', {}).get('profile', args.profile) if args.profile == "none" else args.profile  # if --profile argument is not provided, i.e. profile is default, then use config profile
        args.threads_sample = config.get('threads_sample', args.threads) if args.threads == 5 else args.threads  # if --threads_sample argument is not provided, i.e. profile is default, then use config threads_sample

        # check config
        check_config(config = config)

    # other checks
    check_sample_list_hybrid(sample_list = config.get('samples'))

    # run snakemake workflow
    smk_command = build_snakemake_call(args = args)
    print(smk_command)
    subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir


# %% Main Call

if __name__ == '__main__':
    main()
