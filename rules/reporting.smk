# rule draw_Tree:
#     input:
#         "results/snp_tree_adjusted.tre"
#     output:
#         png="results/snp_tree.png",
#         pdf="results/snp_tree.pdf",
#         svg="results/snp_tree.svg"
#     message: "Drawing a tree from newick file"
#     log:
#         "logs/draw_Tree.log"
#     params:
#         resolution="-width 600 -height 600",
#         figtree=config["paths"]["figtree"]
#     shell:
#         """
#         {params.figtree} -graphic PNG {params.resolution} {input} {output.png} 2> {log}
#         {params.figtree} -graphic PDF {params.resolution} {input} {output.pdf} 2> {log}
#         {params.figtree} -graphic SVG {params.resolution} {input} {output.svg} 2> {log}
#         """
#        "{params.figtree} -graphic PNG {params.resolution} {input} {output.png} 2> {log}"

# TODO: use ggtree/plotly for drawing tree

# def nwk_input(wildcards):
#     if (config["parameters"]["do_iqtree"]):
#         "results/snp_tree.treefile"
#         print("iqtree")
#     elif config["parameters"]["do_raxml"]:
#         "results/core.raxml"
#         print("raxml")
#     elif config["parameters"]["do_nj"]:
#         "results/snp_tree_nj.nwk"
#         print("nj")
#     else :
#      []

rule snp_report:
    input:
        snp_matrix = "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True or config["parameters"]["density_filter"] == True else "results/snp_distancematrix_unfiltered.tsv",
        # snp_matrix = "results/snp_distancematrix.tsv",
        core_vcf = "results/core.vcf",
        core_txt = "results/core.txt",
        # nwk  = "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] "results/core.raxml" elif config["parameters"]["do_raxml"] "results/snp_tree_nj.nwk" elif config["parameters"]["do_nj"] else [],
        nwk = "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] else [],
        coregenome_stats = "results/stats/coregenome.stats.tsv",
        coregenome_summary = "results/stats/coregenome.summary.tsv"
        # nwk  = if config["parameters"]["do_iqtree"]: "results/snp_tree.treefile" elif config["parameters"]["do_raxml"]: "results/core.raxml" elif config["parameters"]["do_nj"]: "results/snp_tree_nj.nwk"  else [],
        # nwk = nwk_input
    output:
        "results/report.html"
    params:
        workdir = config["workdir"],
        exportpath = "results/exported_trees",
        export = True,
        snp_threshold = config["parameters"]["clustering_cutoff"],# 20,
        outlier_threshold = config["parameters"]["outlier_threshold"],# 1000
        alignment_threshold = 0.25,# exclude samples where unaligned and lowcov is below this
        use_reference = config["parameters"]["use_reference"],
        do_iqtree = config["parameters"]["do_iqtree"],
        do_nj = config["parameters"]["do_nj"],
        do_raxml = config["parameters"]["do_raxml"],
        do_gubbins = config["parameters"]["do_gubbins"],
        do_densityfilter = config["parameters"]["density_filter"],
        gubbins_stats = "results/gubbins/gubbins.per_branch_statistics.csv" if config["parameters"]["do_gubbins"] == True else [],
        filtered_core_tab = "results/core.filtered.tab" if config["parameters"]["density_filter"] == True else [],
        reference = config["reference"],# if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.fna"},
    conda: "../envs/rmarkdown.yaml"
    message: "Creating Rmarkdown SNP report"
    log:
        log = "logs/snp_report.log"
    script:
        "../scripts/snp_report.Rmd"

# rule report_Rmarkdown:
#     input:
#         png = "results/snp_tree.png",
#         # pdf = "results/snp_tree.pdf",
#         txt = "results/core.txt",
#         matrix = "results/snp_distancematrix.tsv",
#         vcf = "results/core.vcf",
#         # aln = "results/core.aln",
#         workdir = config["workdir"],
#         nwk = "results/snp_tree.tre"
#     output:
#         "results/report.html"
#     conda:
#        "envs/snippy.yaml"
#     message: "Creating Rmarkdown report"
#     script:
#         "scripts/write_report.Rmd"
