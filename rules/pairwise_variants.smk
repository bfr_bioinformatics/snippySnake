# extra rules for pairwise snp

# rule find_centroid_reference: # outside snippy snake

# rule create_prokka_annotation:  # outside snippy snake?

# rule add_annotation_info to vcf: # goes to reporting?

# rule filter_high_density_regions: # goes to snp_calling.smk, also part of pairwise

# from single sample
# rule pairwise_variants_parsing:
# input:
# snp_results = "snps.tab" # single sample
# reference_sample = "" # where do we get it from
# output:
# params:
# script:

rule pairwise_variants_parsing_batch:
    input:
        snippy_dirs = expand('results/{sample}',sample = samples.index)
    output:
        variant_matrix = "results_pairwiseVariants/variant_matrix.tsv"
    params:
        reference = config["reference"],
        workflow_dir = {workflow.basedir},
        distance_filter = config["parameters"]["filtering"]["snp_threshold"],
        min_cluster_size = config["parameters"]["filtering"]["window_size"],
        reference_sample = config["parameters"]["reference_name"],
    # outdir:
    # log: # TODO add
    script:
        "../scripts/pairwise_variants_parsing_batch.R"

rule report_pairswise_variants:
    input:
        variant_matrix = "results_pairwiseVariants/variant_matrix.tsv"
    output:
        html = "results_pairwiseVariants/report_pairwise_variants.html"
    params:
        reference = config["reference"],
    script:
        "../scripts/report_pairwise_variants.Rmd"
