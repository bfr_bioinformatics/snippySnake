rule compute_raw_distanceMatrix:
    input:
        "results/core.aln"
    output:
        "results/snp_distancematrix_unfiltered.tsv"
    message: "Computing the unfiltered SNP distance matrix"
    conda: "../envs/snippy.yaml"
    log:
        "logs/compute_distanceMatrix_unfiltered.log"
    shell:
        "snp-dists {input} > {output} 2> {log}"

if config["parameters"]["density_filter"]:
    ruleorder: compute_densityfiltered_distanceMatrix > compute_gubbins_distanceMatrix
else:
    ruleorder: compute_gubbins_distanceMatrix > compute_densityfiltered_distanceMatrix

rule compute_densityfiltered_distanceMatrix:
    input:
        "results/core.filtered.aln"
    output:
        "results/snp_distancematrix.tsv"
    message: "Computing the filtered SNP distance matrix after density filter"
    conda:
        "../envs/snippy.yaml"
    log:
        "logs/compute_distanceMatrix_filtered.log"
    shell:
        "snp-dists {input} > {output} 2> {log}"

rule compute_gubbins_distanceMatrix:
    input:
        "results/core.clean.aln"
    output:
        "results/snp_distancematrix.tsv"
    message: "Computing the filtered SNP distance matrix after gubbins"
    conda: "../envs/snippy.yaml"
    log:
        "logs/compute_distanceMatrix_filtered.log"
    shell:
        "snp-dists {input} > {output} 2> {log}"

rule compute_Tree_iqtree:
    input:
        "results/core.clean.aln" if config["parameters"]["do_gubbins"] == True else "results/core.aln"
    output:
        "results/snp_tree.treefile"
    message: "Computing a tree from core alignment with iqtree"
    conda: "../envs/iqtree.yaml"
    log:
        "logs/compute_iqtree.log"
    threads:
        config["smk_params"]["threads"] * 6
    params:
        model = config["parameters"]["iqtree"]["iqtree_model"],
        bb = config["parameters"]["iqtree"]["iqtree_bb"],
        alrt = config["parameters"]["iqtree"]["iqtree_alrt"],
        nm = config["parameters"]["iqtree"]["iqtree_nm"]
    shell:
        "iqtree -s {input} -redo -ntmax {threads} -nt AUTO -st DNA -m {params.model} -bb {params.bb} -alrt {params.alrt} -nm {params.nm} -pre results/snp_tree 2>&1 | tee {log}"

# treebuilder/iqtree_fast.sh:iqtree -s "$aln" -redo -ntmax "$cpus" -nt AUTO -st DNA -fast -m GTR+G4 $opts
# treebuilder/iqtree.sh:iqtree -s "$aln" -redo -ntmax "$cpus" -nt AUTO -st DNA -m GTR+G4 -bb 1000 -alrt 1000 $opts
# treebuilder/iqtree_slow.sh:iqtree -s "$aln" -redo -ntmax "$cpus" -nt AUTO -st DNA -bb 1000 -alrt 1000 $opts

# scale branch length:
# inactive
# rule adjust_tree_file:
#     input:
#         tree = "results/snp_tree_ml.nwk",
# #       alignment = "results/core.aln",
#         vcf = "results/core.vcf"
#     output:
# #       final = "results/snp_tree.tre"
#         final = "results/snp_tree_adjusted.tre"
#     message: "Scaling branch length to average snp distance and remove short distances"
#     # conda:
#         #"envs/snippy.yaml"
#     log:
#        "logs/adjust_tree.log"
#     params:
#        threads = config["smk_params"]["threads"],
#        cutoff = config["parameters"]["branch_remove_cutoff"],
#        tmpfile = "results/snp_tree_adjusted.treefile.tmp",
#        treebenderpath = config["paths"]["treebender"]
#     shell:
#         """
#         alignment_length=`grep -v -c '^#' {input.vcf}`
#         {params.treebenderpath} --multiply_branch_lengths $alignment_length < {input.tree} > {params.tmpfile}
#         {params.treebenderpath} --null_short_branches {params.cutoff} < {params.tmpfile} > {output}
#         # rm {params.tmpfile} # optional
#         """

rule compute_Tree_raxml:
    input:
        "results/core.clean.aln" if config["parameters"]["do_gubbins"] == True else "results/core.aln"
    output:
        file = "results/core.raxml",
    message: "Computing a tree from core alignment with raxml"
    conda: "envs/raxml.yaml"
    threads: config["smk_params"]["threads"]
    log:
        "logs/compute_raxml.log"
    params:
        repeats = 20,
        model = "GTRGAMMA",
        dir = "results/raxml"
    shell:
        """
        raxmlHPC -PTHREADS {threads} -m {params.model} -p 12345 -s {input} -# {params.repeats} -n {output.file} -w {params.dir}
        """

# mkdir raxml2 # need to create before?
# TODO: test
# TODO: add to conda if desired

rule compute_Tree_fasttree:
    input:
        "results/core.clean.aln" if config["parameters"]["do_gubbins"] == True else "results/core.aln"
    output:
        "results/snp_fasttree.tre"
    message: "Computing a tree from core alignment"
    # conda: "envs/snippy.yaml"
    log:
        "logs/compute_Tree.log"
    shell:
        "fasttree -nt -log {log} {input} > {output}"

rule compute_fastdist:
    input:
        "results/core.aln"
    output:
        "results/fastdist.phylip"
    message: "Computing fast distance"
    conda: "../envs/fastphylo.yaml"
    log:
        "logs/compute_fastdist.log"
    shell:
        """
        fastdist --input-format="fasta" -o {output} --output-format="phylip" {input}
        """

rule compute_fastnj:
    input:
        "results/fastdist.phylip"
    output:
        "results/snp_tree_nj.nwk"
    message: "Computing fast nj"
    threads: config["smk_params"]["threads"]
    conda: "../envs/fastphylo.yaml"
    log:
        "logs/compute_fastnj.log"
    shell:
        """
        fnj -I phylip -O newick -o {output} -m BIONJ {input}
        """

# neighbor joining tree
# conda fastphylo
# fastdist -I="fasta" -i core.aln -o fastdist.phylip -O="phylip"
# fnj -I phylip -O newick -o fastphylo_nj.tre -m BIONJ fastdist.phylip
# or using grapetree
