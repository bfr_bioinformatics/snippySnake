rule count_seqs:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards,'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards,'fq2')
    # input:
    #     get_fastq
    # input:
    #     r1 = lambda wildcards: get_fastq(wildcards, samples, 'fq1'),
    #     r2 = lambda wildcards: get_fastq(wildcards, samples, 'fq2')
    output:
        "results/{sample}/stats.txt"
    message: "Running rule count_seqs on {wildcards.sample}"
    # params:
    #     "-a {} {}".format(config["adapter"], config["params"]["cutadapt-pe"])
    # log:
    #     "logs/{sample}-{unit}.log"
    shell:
        """
        echo {input.r1} >> {output}
        bioawk -c fastx 'END{{print NR}}' {input.r1} >> {output}
        echo {input.r2} >> {output}
        bioawk -c fastx 'END{{print NR}}' {input.r2} >> {output}
        """
        # "zcat {input} | bioawk -c fastx 'END{{print NR}}' > {output}"

rule summarize_counts:
    input:
        expand('results/{sample}/stats.txt',sample = samples.index)
        # "results/{sample}/stats.txt"
    output:
        "results/snippy.tsv"
    shell:
        "cat {input} >> {output}"
