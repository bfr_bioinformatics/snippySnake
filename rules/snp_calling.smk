# all rules related to snp calling directly, e.g. snippy calls


#ruleorder: run_snippy_contigs > run_snippy

# TODO detect from header of sample sheet
if 'assembly' in samples_raw.columns:
    ruleorder: run_snippy_contigs > run_snippy
    print("Detected assembly as input, using the run_snippy_contigs subworkflow")
else:
    ruleorder: run_snippy > run_snippy_contigs

# fix for 0 snps
ruleorder: collect_snippy > collect_snippy_fix



rule run_snippy:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards,'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards,'fq2'),
        reference = config["reference"] if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.gbff"},
    output:
        dir = directory("results/{sample}")  # new snakemake directive # or write filename
    message: "Running rule snippy on {wildcards.sample}"
    conda: "../envs/snippy.yaml"
    log:
        "logs/snippy_{sample}.log"
    params:
        # snippy = config["parameters"]["snippy"]["extra"],
        mapqual = config["parameters"]["snippy"]["mapqual"],
        basequal = config["parameters"]["snippy"]["basequal"],
        mincov = config["parameters"]["snippy"]["mincov"],
        minfrac = config["parameters"]["snippy"]["minfrac"],
        minqual = config["parameters"]["snippy"]["minqual"],
        maxsoft = config["parameters"]["snippy"]["maxsoft"]
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        if [[ ({input.r1} != {input.r2}) && -s {input.r2} ]]; then
            snippy --cpus {threads} --outdir {output.dir} --ref {input.reference} --R1 {input.r1} --R2 {input.r2} --mapqual {params.mapqual} --basequal {params.basequal} --mincov {params.mincov} --minfrac {params.minfrac} --minqual {params.minqual} --maxsoft {params.maxsoft} 2>&1 | tee {log}
        else
            snippy --cpus {threads} --outdir {output.dir} --ref {input.reference} --se {input.r1} --mapqual {params.mapqual} --basequal {params.basequal} --mincov {params.mincov} --minfrac {params.minfrac} --minqual {params.minqual} --maxsoft {params.maxsoft} 2>&1 | tee {log}
        fi
        """

# from assemblies
rule run_snippy_contigs:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards,'assembly'),
        reference = config["reference"] if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.gbff"},
    output:
        dir = directory("results/{sample}")  # new snakemake directive # or write filename
    message: "Running rule snippy on {wildcards.sample} with contigs"
    conda: "../envs/snippy.yaml"
    log:
        "logs/snippy_{sample}.log"
    params:
        # snippy = config["parameters"]["snippy"]["extra"],
        mapqual = config["parameters"]["snippy"]["mapqual"],
        basequal = config["parameters"]["snippy"]["basequal"],
        mincov = config["parameters"]["snippy"]["mincov"],
        minfrac = config["parameters"]["snippy"]["minfrac"],
        minqual = config["parameters"]["snippy"]["minqual"],
        maxsoft = config["parameters"]["snippy"]["maxsoft"]
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        echo "snippy --cpus {threads} --outdir {output.dir} --ref {input.reference} --ctgs {input.contigs} --mapqual {params.mapqual} --basequal {params.basequal} --mincov {params.mincov} --minfrac {params.minfrac} --minqual {params.minqual} --maxsoft {params.maxsoft} 2>&1 | tee {log}"
        snippy --cpus {threads} --outdir {output.dir} --ref {input.reference} --ctgs {input.contigs} --mapqual {params.mapqual} --basequal {params.basequal} --mincov {params.mincov} --minfrac {params.minfrac} --minqual {params.minqual} --maxsoft {params.maxsoft} 2>&1 | tee {log}
        """


# collect snippy results per sample and find core snps
rule collect_snippy:
    input:
        expand('results/{sample}',sample = samples.index)
    output:
        txt = "results/core.txt",
        vcf = "results/core.vcf",
        core_tab = "results/core.tab",
        aln = "results/core.aln",
        fullaln = "results/core.full.aln",
    message: "Aggregating core snps"
    conda: "../envs/snippy.yaml"
    log:
        "logs/collect_snippy.log"
    params:
        ref = config["reference"] if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.gbff"},
        bedfile = config["parameters"]["snippy"]["bedfile"]  #"" # bed files with regions to exclude
    shell:
        """
        if [[ {{params.bedfile}} != "" && -f {{params.bedfile}} ]];then
            echo "Masking regions with {params.bedfile}"
            snippy-core --mask {params.bedfile} --ref {params.ref} --prefix results/core {input} 2>&1 | tee {log}
        else
            echo "Not masking any regions since no bedfile was provided"
            snippy-core --ref {params.ref} --prefix results/core {input} 2>&1 | tee {log}
        fi
        """

rule collect_snippy_fix:
    input:
        expand('results/{sample}',sample = samples.index)
    output:
        txt = "results/core.txt",
        vcf = "results/core.vcf",
        aln = "results/core.aln",
        fullaln = "results/core.full.aln",
    message: "Aggregating core snps, fixing for 0 snps"
    conda:
        "../envs/snippy.yaml"
    log:
        "logs/collect_snippy.log"
    params:
        ref = config["reference"] if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.gbff"},
        bedfile = config["parameters"]["snippy"]["bedfile"]  #"" # bed files with regions to exclude
    shell:
        """
        echo "snippy-core --ref {params.ref} --prefix results/core {input} || grep '>' results/core.full.aln > results/core.aln"
        snippy-core --ref {params.ref} --prefix results/core {input} || grep '>' results/core.full.aln > results/core.aln
        """

# check that 0 snps: tail -n +2 core.tab | wc -l
# """
# if [[ {{params.bedfile}} != "" && -f {{params.bedfile}} ]];then
#   echo "Masking regions with {params.bedfile}"
#   snippy-core --mask {params.bedfile} --ref {params.ref} --prefix results/core {input} 2>&1 | tee {log}
# else
#   echo "Not masking any regions since no bedfile was provided"
#   snippy-core --ref {params.ref} --prefix results/core {input} 2>&1 | tee {log}
# fi
# """

rule filter_highdensity_regions:
    input:
        core_tab = "results/core.tab",
    output:
        filtered_alignment = "results/core.filtered.aln",
        annotated_core_tab = "results/core.filtered.tab"
    message: "Apply filter_highdensity_regions"
    params:
        snp_threshold = config["parameters"]["filtering"]["snp_threshold"],
        window_size = config["parameters"]["filtering"]["window_size"],
        workflow_dir = {workflow.basedir},
        filter_type = config["parameters"]["filtering"]["filter_type"]
    script:
        "../scripts/filter_highdensity_snps.R"

# clean core alignment for downstream (gubbins) analysis
rule clean_snippy:
    input:
        fullaln = "results/core.full.aln",
    output:
        fullaln = "results/core.clean.full.aln",
    message: "Cleaning core alignment"
    conda: "../envs/snippy.yaml"
    log:
        "logs/clean_snippy.log"
    # params:
    #     ref = config["reference"] if config["parameters"]["use_reference"] == True else {"reffinder/reference/reference.fna"},
    shell:
        "snippy-clean_full_aln {input} > {output} 2>&1 | tee {log}"

# filter recombination snps
rule run_gubbins:
    input:
        fullaln = "results/core.clean.full.aln",
    output:
        filtered_fasta = "results/gubbins/gubbins.filtered_polymorphic_sites.fasta",
        stat = "results/gubbins/gubbins.per_branch_statistics.csv"
    message: "Run gubbins"
    threads:
        config["smk_params"]["threads"] * 8
    conda: "../envs/gubbins.yaml"
    log:
        "logs/gubbins.log"
    params:
        gubbins_dir = "results/gubbins",
        prefix = "results/gubbins/gubbins",
        outgroup = "",
        starting_tree = "",
        tree_builder = config["parameters"]["gubbins"]["tree_builder"],# "raxml",
        iterations = config["parameters"]["gubbins"]["iterations"],# 5,
        min_snps = config["parameters"]["gubbins"]["min_snps"],# 3,
        filter_percentage = config["parameters"]["gubbins"]["filter_percentage"],# 25,
        converge_method = config["parameters"]["gubbins"]["converge_method"],# "weighted_robinson_foulds",
        min_window_size = config["parameters"]["gubbins"]["min_window_size"],# 100,
        max_window_size = config["parameters"]["gubbins"]["max_window_size"],# 10000,
        raxml_model = config["parameters"]["gubbins"]["raxml_model"],#"GTRCAT",
        # remove_identical_sequences = 0,
    shell:
        """
        echo "run_gubbins.py --threads {threads} --converge_method {params.converge_method} --tree_builder {params.tree_builder} --iterations {params.iterations} --min_snps {params.min_snps} --filter_percentage {params.filter_percentage} --min_window_size {params.min_window_size} --max_window_size {params.max_window_size} --raxml_model {params.raxml_model} -p {params.prefix} {input.fullaln}"
        mkdir -p {params.gubbins_dir} && run_gubbins.py --threads {threads} --converge_method {params.converge_method} --tree_builder {params.tree_builder} --iterations {params.iterations} --min_snps {params.min_snps} --filter_percentage {params.filter_percentage} --min_window_size {params.min_window_size} --max_window_size {params.max_window_size} --raxml_model {params.raxml_model} -p {params.prefix} {input.fullaln} 2>&1 | tee {log}
        """
#--remove_identical_sequences {params.remove_identical_sequences}

# call core snps after gubbins
rule call_gubbins_snpsites:
    input:
        filtered_fasta = "results/gubbins/gubbins.filtered_polymorphic_sites.fasta",
    output:
        corealn = "results/core.clean.aln",
    message: "Calling clean core alignment with rule call_gubbines_snpsites"
    conda: "../envs/snippy.yaml"
    log:
        "logs/gubbins_snps.log"
    shell:
        "snp-sites -c {input.filtered_fasta} > {output.corealn} 2>&1 | tee {log}"

rule compute_core_genome_size:  # part of reporting
    input: fullaln = "results/core.full.aln"
    output:
        stats = "results/stats/coregenome.stats.tsv",
        summary = "results/stats/coregenome.summary.tsv"
    message: "computing compute_core_genome_size"
    params:
        working_dir = config["workdir"]
    shell:
        "{workflow.basedir}/scripts/core_genome_analysis.R results"
