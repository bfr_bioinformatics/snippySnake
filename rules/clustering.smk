
# rule cluster_into_subtrees:
#     input:
#         tree = "results/snp_tree_adjusted.tre"  # TODO need adjusted single linkage tree here?
#     output:
#         # final = "results/cluster.tsv",
#         final = "results/cluster.txt"
#     message: "Clustering tree files by cutting all below threshold"
#     # conda: "envs/snippy.yaml"
#     log:
#         "logs/clustering.log"
#     params:
#         cutoff = config["parameters"]["clustering_cutoff"],
#         treebenderpath = config["paths"]["treebender"]
#     shell:
#         """
#         {params.treebenderpath} --cluster branch_length:{params.cutoff} < {input} > {output}
#         """

rule format_cluster_output:
    input:
        "results/cluster.txt"
    output:
        final = "results/cluster.tsv"
    message: "Re-formatting clustering output for use in grapetree"
    # conda: "envs/snippy.yaml"
    log:
        "logs/clustering_reformat.log"
    shell:
        """
        echo -e 'ID\tCluster' > {output}
        grep -v '^#' {input} | awk '{{print "C"NR,$0}}' | awk '{{ for(i = 2; i <= NF; i++) print $i,$1 }}' | tr ' ' '\t' >> {output}
        """

# %% scratch section --------------------

# TODO: rename clusters with olders member name (lexicographically sorted)

# tool for query of distance matrix
# getdistance () { grep "$2" $1  | tail -n 1 | cut -f $(cut -f 1 $1 | awk '{print NR,$0}' | grep "$3"  | cut -f 1 -d ' '); }
# getdistance snp_distancematrix.tsv 18-SA02229 18-SA01957-1

# getneighbors () { tail -n +2 $1 | cut -f 1,$(cut -f 1 $1 | awk '{print NR,$0}' | grep "$2"  | cut -f 1 -d ' ') | sort -k 2 -n; }
# getneighbors snp_distancematrix.tsv 18-SA01957-1
