# rules for finding the optimal reference via mash

rule mash_sketch:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards,'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards,'fq2')
    output:
        mash_sketch = "reffinder/mash/{sample}.msh"
    message: "Running rule mash on {wildcards.sample}"
    conda: "../envs/mash.yaml"
    log: "logs/mash_sketch_{sample}.log"
    threads: config["smk_params"]["threads"]
    params:
        mash_kmersize = config["parameters"]["mash"]["kmersize"],# 21,
        mash_sketchsize = config["parameters"]["mash"]["sketchsize"],# 1000,
        mash_minimumcopies = 2,
        mash_outname = "reffinder/mash/{sample}"
    shell:
        """
        if [[ ({input.r1} != {input.r2}) && -s {input.r2} ]]; then
            echo "zcat {input.r1} {input.r2} | mash sketch -r -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname}  -I {wildcards.sample} -C 'read pair' -" > {log}
            zcat {input.r1} {input.r2} | mash sketch -r -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname} -I {wildcards.sample} -C "read pair" - &>> {log}
        else
            echo "zcat {input.r1} | mash sketch -r -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname}  -I {wildcards.sample} -C 'single-ended reads' -" > {log}
            zcat {input.r1} | mash sketch -r -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname} -I {wildcards.sample} -C "single-ended reads" - &>> {log}
        fi
        """
    #mash sketch -p {threads} -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname} <(zcat {input.r1} {input.r2}) 2>&1 > {log}
    #mash sketch -p {threads} -k {params.mash_kmersize} -s {params.mash_sketchsize} -m {params.mash_minimumcopies} -o {params.mash_outname} <(zcat {input.r1} {input.r2}) 2>&1 > {log}

rule mash_dist:
    input:
        mash_sketch = "reffinder/mash/{sample}.msh"
    output:
        mash_dist = "reffinder/mash/{sample}.dist"
    message: "Computing mash distance for {wildcards.sample}"
    conda: "../envs/mash.yaml"
    log: "logs/mash_dist_{sample}.log"
    threads: config["smk_params"]["threads"]
    params:
        # mash_db = "/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/plasmids_removed/mash/Refseq_completeBacteria_chromosomes_s1000_k21.msh",
        mash_db = config["parameters"]["mash"]["mash_db"],
        mash_minoverlap = config["parameters"]["mash"]["mash_minoverlap"],# 500
    shell:
        """ 
        echo "mash dist {params.mash_db} {input.mash_sketch} | sed 's/\/1000/\t1000/g' | awk -F'\t' '5 > {params.mash_minoverlap}' > {output.mash_dist}" > {log}
        mash dist {params.mash_db} {input.mash_sketch} | sed 's/\/1000/\t1000/g' | awk -F'\t' '$5 > {params.mash_minoverlap}' > {output.mash_dist} 2>> {log}
        """
    # mash dist {params.mash_db} {input.mash_sketch} | sed 's/\/1000/\t1000/g' | awk -F'\t' -v minoverlap={params.mash_minoverlap} '$5 > minoverlap' > {output.mash_dist} 2>> {log}

rule collect_mash:
    input:
        mash_dist = expand("reffinder/mash/{sample}.dist",sample = samples.index)
    output:
        mash_dist = "reffinder/mash/joined.distance"
    message: "Joining mash distances"
    # conda: "../envs/mash.yaml"
    log: "logs/mash_joining.log"
    threads: config["smk_params"]["threads"]
    # params:    
    shell:
        """
        echo "cat {input} >> {output}"
        cat {input} >> {output}
        """

# compute aggeragated scores (summed score per target)
rule aggregate_mash_scores:
    input:
        mash_dist = "reffinder/mash/joined.distance"
    output:
        mash_aggregate = "reffinder/mash/aggregated_scores.tsv"
    message: "Aggregting mash scores"
    # conda: "../envs/mash.yaml"
    log: "logs/mash_aggregate.log"
    threads: config["smk_params"]["threads"]
    shell:
        """
        awk -F'\t' 'BEGIN{{OFS="\t"}};{{summedscore[$1]+=$5;l[$1]+=1}}; END{{for(value in summedscore) print value, summedscore[value], l[value],summedscore[value]/l[value]}}' {input} | sort -nr -k 4 > {output}
        """

rule mash_info:
    input:
        mash_aggregate = "reffinder/mash/aggregated_scores.tsv"
    output:
        mash_aggregate = "reffinder/mash/aggregated_scores_info.tsv"
    message: "Computing mash info for aggregated scores"
    # conda: "../envs/mash.yaml"
    log: "logs/mash_info.log"
    threads: config["smk_params"]["threads"]
    params:
        # mash_db = "/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi/plasmids_removed/mash/Refseq_completeBacteria_chromosomes_s1000_k21.msh",
        mash_info = config["parameters"]["mash"]["mash_info"]
    shell:
        """
        # complement with mash info: all hits
        echo -e "ref\ttotal\tcount\tavg\tinfo" > {output}
        while read ref total count avg; do
            info=`grep $ref {params.mash_info} | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{{2,}}/\t/g' | cut -f 4`
            echo -e "$ref\t$total\t$count\t$avg\t$info" >> {output}
        done < {input.mash_aggregate}
        """

rule mash_top_hit:
    input:
        mash_aggregate = "reffinder/mash/aggregated_scores.tsv"
        # mash_aggregate = "reffinder/mash/aggregated_scores_info.tsv"
    output:
        mash_best = "reffinder/mash/besthit.tsv"
    message: "Computing mash top hit"
    # conda: "../envs/mash.yaml"
    log: "logs/mash_tophit.log"
    threads: config["smk_params"]["threads"]
    params:
        assembly_stats = config["parameters"]["mash"]["refseq_db"]  # "/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/assembly_summary_refseq.txt"
    shell:
        """
        BestGCF=`head -n 1 {input.mash_aggregate} | cut -f 1 | awk -F'/' '{{print $NF}}' | cut -f 1-2 -d '_'`
        BestGCF=`echo $BestGCF | cut -f 1 -d '.'`
        BestHit=`head -n 1 {input.mash_aggregate} | cut -f 1`
        BestURL=`grep --color=never $BestGCF {params.assembly_stats} | cut -f 20`
        Best_Accession=`basename $BestURL`
        BestURL2fasta="$BestURL/${{Best_Accession}}_genomic.fna.gz"
        BestURL2gbff="$BestURL/${{Best_Accession}}_genomic.gbff.gz"
        echo -e "$BestGCF\t$BestURL\t$BestURL2fasta\t$BestURL2gbff\t$BestHit" > {output.mash_best}
        """

rule download_ref:
    input:
        mash_best = "reffinder/mash/besthit.tsv"
        # mash_aggregate = "reffinder/mash/aggregated_scores_info.tsv"
    output:
        # reference_gbff = "reffinder/reference/reference.gbff"
        reference_gbff = "reffinder/reference/reference.gbff.gz",
        reference_fasta = "reffinder/reference/reference.fna.gz"
    message: "Downloading best reference"
    # conda: "../envs/ncbi-download.yaml"
    log: "logs/download_ref.log"
    threads: config["smk_params"]["threads"]
    params:
        refdir = "reffinder/reference"
    shell:
        """
        BestURL2gbff=`cut -f 4 {input}`
        BestURL2fasta=`cut -f 3 {input}`
        mkdir -p {params.refdir}
        wget -O {output.reference_gbff} $BestURL2gbff --tries 10 -o {log}
        wget -O {output.reference_fasta} $BestURL2fasta --tries 10 -o {log}
        """

# using ncbi-acc tools
# rule download_ref_alt:
#     input:
#         mash_aggregate = "reffinder/mash/aggregated_scores_info.tsv"
#     output:
#         reference_gbff = "reffinder/reference/reference.gbff"
#     message: "Downloading best reference"
#     conda: "../envs/ncbi-download.yaml"
#     log: "logs/download_ref_alt.log"
#     threads: config["smk_params"]["threads"]
#     params:
#         refdir="reffinder/reference"
#     shell:
#        """
#        myacc=`head -n 2 ../mash/aggregated_scores_info.tsv | tail -n 1 | cut -f 5 | cut -f 1 -d ' '`
#        ncbi-acc-download -o {output.reference_gbff} $myacc
#        """

rule unzip_ref:
    input:
        reference_gbff = "reffinder/reference/reference.gbff.gz",
        reference_fasta = "reffinder/reference/reference.fna.gz"
    output:
        reference_gbff = "reffinder/reference/reference.gbff",
        reference_fasta = "reffinder/reference/reference.fna"
    message: "Unzipping ref sequences"
    # conda: "../envs/mash.yaml"
    log: "logs/unzip_references.log"
    threads: config["smk_params"]["threads"]
    shell:
        """
        gunzip -k {input.reference_gbff}
        gunzip -k {input.reference_fasta}
        """

rule reference_info:
    input:
        reference_fasta = "reffinder/reference/reference.fna"
    output:
        reference_info = "reffinder/reference/reference.info"
    message: "Reference info"
    # conda: "../envs/mash.yaml"
    log: "logs/reference_info.log"
    threads: config["smk_params"]["threads"]
    shell:
        """
        grep '>' {input} | sed 's/^>//' > {output}        
        """
