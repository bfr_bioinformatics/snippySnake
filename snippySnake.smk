# snippySnake - pipeline for variant calling for a set of files

# TODO: add intersnp distance filter as option?
# TODO: define reference as variable
# reference = "TODO"
# ref_fasta = config["ref"]["fasta"]
# print("reference: " + ref_fasta)
# TODO: check ref exists and is in correct format
# more
#        "results/snp_tree.tre", # result from fasttree

# %% Imports --------------------------------------------------------

import os
from datetime import datetime
import pandas as pd
import yaml

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Import rule directives
with open(config["smk_params"]["rule_directives"],"r") as stream:
    try:
        rule_directives = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        rule_directives = {'rule': ''}

# Collect sample names from sample list
samples_raw = pd.read_csv(config["samples"], sep = "\t", dtype = str).set_index('sample', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out
samples_column = "fq1"


# %% Functions ------------------------------------------------------

def _get_fastq(wildcards, column_name = samples_column):
    return samples.loc[wildcards.sample, [column_name]].dropna()[0]


# %% Check for software ---------------

# import shutil

# print("Checking if software treebender is available:",config["paths"]["treebender"])
# check_treebender = shutil.which(config["paths"]["treebender"])
# if not check_treebender:
#     sys.exit('Error: could not find treebinder')
# else:
#     print("treebinder: OK")

# print("Checking if software figtree is available:",config["paths"]["figtree"])
# check_figtree = shutil.which(config["paths"]["figtree"])
# if not check_figtree:
#     sys.exit('Error: could not find figtree')
# else:
#     print("figtree: OK")


# %% All and Local Rules  -------------------------------------------

rule all:
    input:
        # reference
        "reffinder/reference/reference.info" if config["parameters"]["reference_type"] == "auto" else [],
        # snippy
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/stats/coregenome.stats.tsv",
        # filtering settings
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        # tree settings
        "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] == True else [], # result of iqtree
        "results/core.raxml" if config["parameters"]["do_raxml"] == True else [], # result of raxml
        "results/snp_tree_nj.nwk" if config["parameters"]["do_nj"] == True else [], # result of nj
        # report settings
        "results/report.html" if config["parameters"]["do_report"] else [], # report

rule all_pairwise:
    input:
        # reference
        "reffinder/reference/reference.info" if config["parameters"]["reference_type"] == "auto" else [],
        # snippy
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/stats/coregenome.stats.tsv",
        # filtering settings
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        # tree settings
        "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] == True else [], # result of iqtree
        "results/core.raxml" if config["parameters"]["do_raxml"] == True else [], # result of raxml
        "results/snp_tree_nj.nwk" if config["parameters"]["do_nj"] == True else [], # result of nj
        # report settings
        "results/report.html" if config["parameters"]["do_report"] else [], # report
        # extra pairwise informatino
        "results_pairwiseVariants/variant_matrix.tsv",
        "results_pairwiseVariants/report_pairwise_variants.html"

rule all_old:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/stats/coregenome.stats.tsv",
        "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] == True else [], # result of iqtree
        # "results/snp_tree_adjusted.tre", # convert branch length to snp distances, inactive
        # "results/cluster.tsv", # cluster results inactive
        # "results/snp_tree.png", # plot with figtree (does not work for admin)
        # "results/report.html",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [],  # distance matrix from core alignment

rule all_noreport:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/stats/coregenome.stats.tsv",
        "results/snp_tree.treefile" if config["parameters"]["do_iqtree"] == True else [], # result of iqtree

rule all_iqtree:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/snp_tree.treefile", # result of iqtree
        "results/stats/coregenome.stats.tsv"

rule all_gubbins:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv", # distance matrix from core alignment
        "results/gubbins/gubbins.filtered_polymorphic_sites.fasta", # result from gubbins
        "results/stats/coregenome.stats.tsv"

rule all_raxml:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/core.raxml", # result of raxml
        "results/stats/coregenome.stats.tsv"

rule all_nj:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/fastdist.phylip", # 
        "results/snp_tree_nj.nwk",
        "results/stats/coregenome.stats.tsv"

rule all_report:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/report.html", # report
        "results/stats/coregenome.stats.tsv"

rule all_simple:
    input:
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment

rule all_reference_only:
    input:
        "reffinder/reference/reference.info"

rule all_snippy_from_reffinder:
    input:
        "reffinder/reference/reference.info",
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment

rule all_snippy_from_reffinder_iqtree:
    input:
        "reffinder/reference/reference.info",
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/snp_tree.treefile", # result of iqtree

rule all_snippy_from_reffinder_nj:
    input:
        "reffinder/reference/reference.info",
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/fastdist.phylip", # 
        "results/snp_tree_nj.nwk"

rule all_snippy_from_reffinder_report:
    input:
        "reffinder/reference/reference.info",
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/report.html"  # report

rule all_snippy_from_reffinder_iqtree_report:
    input:
        "reffinder/reference/reference.info",
        expand("results/{sample}",sample = samples.index), # call snps
        "results/core.aln", # core snps 
        "results/snp_distancematrix_unfiltered.tsv",
        "results/snp_distancematrix.tsv" if config["parameters"]["do_gubbins"] == True else [], # distance matrix from core alignment
        "results/snp_tree.treefile", # result of iqtree
        "results/report.html"  # report

rule all_only_pairwise:
    input:
        "results_pairwiseVariants/variant_matrix.tsv",
        "results_pairwiseVariants/report_pairwise_variants.html"


# %% Include rules --------------------------------------------------

include: "rules/reffinder.smk"  # find best reference
# TODO make switch
#input_type = "assembly"
#print (f"input_type: {input_type}")
include: "rules/snp_calling.smk"  # snippy snp calling and core snps
#if input_type == "reads":
    #include: "rules/snp_calling.smk"  # snippy snp calling and core snps
#elif input_type == "assembly":
    #include: "rules/snp_calling_contigs.smk" # snippy snp calling and core snps
#else:
    #print("Input type not recognized. Using reads")        
    #include: "rules/snp_calling.smk"  # snippy snp calling and core snps

include: "rules/phylogeny.smk"  # build phylogenetic tree, create distance matrix, change branch length
include: "rules/clustering.smk"  # cluster phylogenetic tree by snp distance
include: "rules/reporting.smk"  # plot and report
include: "rules/pairwise_variants.smk" # extra rules for pairwise variants


# %% logging information --------------------------------------------

def pipeline_status_message(status: str) -> tuple:
    RESET = "\033[0m"
    RED = "\033[31m"
    GREEN = "\033[32m"

    timestamp = datetime.now()
    script_id = os.environ.get('script_id', 'unset')
    status_file = os.path.join(config['workdir'], f"pipeline_status_{config['pipeline'].lower()}.txt")
    status_chunk = f"{timestamp.strftime('%F %H:%M')}\t{snakemake.logging.logger.logfile}\t{os.environ.get('CONDA_PREFIX', 'CONDA_PREFIX_is_unset')}\t{os.environ.get('USER', 'unknown')}:{os.uname().nodename}\tShellScriptId:[{script_id}]\n"

    if status == "running":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"[START] {os.path.abspath(__file__)}"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "success":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{GREEN}[SUCCESS]{RESET} Pipeline status: Workflow finished successfully"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "error":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{RED}[ERROR]{RESET} Pipeline status: An error occurred"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"

    return status_file, status, message


onstart:
    status_file, status, message = pipeline_status_message(status = 'running')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onsuccess:
    status_file, status, message = pipeline_status_message(status = 'success')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onerror:
    status_file, status, message = pipeline_status_message(status = 'error')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)
